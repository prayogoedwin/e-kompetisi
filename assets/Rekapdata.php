<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapdata extends CI_Controller {

	public function __construct(){
			parent::__construct();

            $this->load->model('Rekap_model');
            // Load Pagination library
	}



    public function blue() {
        if ($this->session->userdata('petani-id') !== null) {
                $data['judul'] = ("Rekapitulasi Data Petani Memiliki Lahan Tidak Masuk Dalam BDT Kemiskinan");
                $data['kota'] = $this->Rekap_model->kabkotablue();
                $data['tot'] = $this->Rekap_model->all_kabkotablue();
                $data['color'] = ("#5bc0de");
                $data['btn'] = ("info");
                $data['bdt'] = 1;
                $data['zona'] = ("blue");
                $this->load->view('admin/template/head');
                $this->load->view('admin/template/header');
                $this->load->view('admin/rekap_kab', $data);
                $this->load->view('admin/template/js');
                }else{
                header("Location: ".base_url());
             }
        }

	public function red() {
        if ($this->session->userdata('petani-id') !== null) {
                $data['judul'] = ("Rekapitulasi Data Petani Pemilik Lahan < 0.25 Masuk Dalam BDT Kemiskinan");
                $data['kota'] = $this->Rekap_model->kabkotared();
                $data['tot'] = $this->Rekap_model->all_kabkotared();
                $data['color'] = ("#d9534f");
                $data['btn'] = ("danger");
                $data['bdt'] = 1;
                $data['zona'] = ("red");
                $this->load->view('admin/template/head');
                $this->load->view('admin/template/header');
                $this->load->view('admin/rekap_kab', $data);
                $this->load->view('admin/template/js');
                }else{
                header("Location: ".base_url());
             }
        }

    public function yellow() {
        if ($this->session->userdata('petani-id') !== null) {
                $data['judul'] = ("Rekapitulasi Data Petani Lahan > 0.25 s/d 1 Masuk Dalam BDT Kemiskinan ");
                $data['kota'] = $this->Rekap_model->kabkotayellow();
                $data['tot'] = $this->Rekap_model->all_kabkotayellow();
                $data['color'] = ("#f0ad4e");
                $data['btn'] = ("warning");
                $data['bdt'] = 1;
                $data['zona'] = ("yellow");
                $this->load->view('admin/template/head');
                $this->load->view('admin/template/header');
                $this->load->view('admin/rekap_kab', $data);
                $this->load->view('admin/template/js');
                }else{
                header("Location: ".base_url());
             }
        }

    public function green() {
        if ($this->session->userdata('petani-id') !== null) {
                $data['judul'] = ("Rekapitulasi Data Petani Lahan > 1 s/d 2 Masuk Dalam BDT Kemiskinan");
                $data['kota'] = $this->Rekap_model->kabkotagreen();
                $data['tot'] = $this->Rekap_model->all_kabkotagreen();
                $data['color'] = ("#5cb85c");
                $data['btn'] = ("success");
                $data['bdt'] = 1;
                $data['zona'] = ("green");
                $this->load->view('admin/template/head');
                $this->load->view('admin/template/header');
                $this->load->view('admin/rekap_kab', $data);
                $this->load->view('admin/template/js');
                }else{
                header("Location: ".base_url());
             }
        }

    public function petanibdt() {
        if ($this->session->userdata('petani-id') !== null) {
                $data['judul'] = ("Data Petani Dalam BDT Kemiskinanan Dan Tidak Memiliki Lahan");
                $data['kota'] = $this->Rekap_model->kabkotabdt();
                $data['tot'] = $this->Rekap_model->all_kabkotabdt();
                $data['color'] = ("#f0ad4e");
                $data['btn'] = ("warning");
                $data['bdt'] = 0;
                $data['zona'] = ("orange");
                $this->load->view('admin/template/head');
                $this->load->view('admin/template/header');
                $this->load->view('admin/rekap_kab', $data);
                $this->load->view('admin/template/js');
                }else{
                header("Location: ".base_url());
             }
        }
}
?>
