# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.37-MariaDB)
# Database: ekompetisi
# Generation Time: 2019-12-19 02:53:35 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table e_akses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_akses`;

CREATE TABLE `e_akses` (
  `id_akses` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_akses` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_akses`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_akses` WRITE;
/*!40000 ALTER TABLE `e_akses` DISABLE KEYS */;

INSERT INTO `e_akses` (`id_akses`, `nama_akses`)
VALUES
	(1,'super admin'),
	(2,'kontingen');

/*!40000 ALTER TABLE `e_akses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_atlit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_atlit`;

CREATE TABLE `e_atlit` (
  `id_atlit` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) DEFAULT NULL,
  `nisn` varchar(255) DEFAULT NULL,
  `nama_atlit` varchar(255) DEFAULT NULL,
  `jk` varchar(255) DEFAULT NULL,
  `alamat` text,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `tinggi_badan` varchar(255) DEFAULT NULL,
  `berat_badan` varchar(255) DEFAULT NULL,
  `no_hp` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `sekolah` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `id_kontingen` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_atlit`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `e_atlit` WRITE;
/*!40000 ALTER TABLE `e_atlit` DISABLE KEYS */;

INSERT INTO `e_atlit` (`id_atlit`, `nik`, `nisn`, `nama_atlit`, `jk`, `alamat`, `tempat_lahir`, `tanggal_lahir`, `tinggi_badan`, `berat_badan`, `no_hp`, `email`, `sekolah`, `foto`, `id_kontingen`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(7,'1234124324','123','AAA','Pa','ZZZ','Demak','1991-10-10','170','55',NULL,NULL,'SD 1','assets/images/atlit/d41d8cd98f00b204e9800998ecf8427e.png',1,'2019-12-01 21:46:24',NULL,NULL),
	(8,'1234124324','123','AAA','Pa','demak kelurahan anjar','Demak','1991-10-10','170','60',NULL,NULL,'SD 1','assets/images/atlit/d41d8cd98f00b204e9800998ecf8427e.png',4,'2019-12-01 22:09:15',NULL,NULL),
	(9,'12313','12313','PUTRI 1','Pi','-','Demak','2019-12-02','170','55',NULL,NULL,'SD 1','assets/images/atlit/d41d8cd98f00b204e9800998ecf8427e.png',4,'2019-12-02 13:57:15',NULL,NULL),
	(10,'32454','65778978','AHmad','Pa','asd','asd','2019-12-21','23','123',NULL,NULL,'SD 1','assets/images/atlit/d41d8cd98f00b204e9800998ecf8427e.png',4,'2019-12-02 13:59:07',NULL,NULL),
	(11,'76','45','PUTRI 2','Pi','asd','Demak','2019-12-20','23','3321',NULL,NULL,'SD 1','assets/images/atlit/d41d8cd98f00b204e9800998ecf8427e.png',4,'2019-12-02 13:59:33',NULL,NULL),
	(12,'w234324','234234234','Putri 3','Pi','demak kelurahan anjar','Demak','2000-01-03','170','60',NULL,NULL,'SD K','assets/images/atlit/d41d8cd98f00b204e9800998ecf8427e.png',4,'2019-12-06 20:55:29',NULL,NULL),
	(13,'45678678','34234234','PUTRI 4','Pi','qswe','Demak','2019-12-31','170','60',NULL,NULL,'SD Y','assets/images/atlit/d41d8cd98f00b204e9800998ecf8427e.png',4,'2019-12-06 20:56:32',NULL,NULL),
	(14,'32453645234','65778978432','Karyi','Pa','asd','asd','2019-12-27','123','123',NULL,NULL,'SD YX','assets/images/atlit/d41d8cd98f00b204e9800998ecf8427e.png',5,'2019-12-10 17:22:36',NULL,NULL),
	(15,'7567567567','345345345','AHmad DYS','Pa','demak kelurahan anjar','Demak','2019-12-27','32','23',NULL,NULL,'SD YX','assets/images/atlit/d41d8cd98f00b204e9800998ecf8427e.png',5,'2019-12-10 17:23:35',NULL,NULL),
	(16,'543768678','5675678567','PUTRI 401','Pi','dsaf','Demak','2019-12-27','56','324',NULL,NULL,'SD Yds','assets/images/atlit/d41d8cd98f00b204e9800998ecf8427e.png',5,'2019-12-10 17:24:29',NULL,NULL);

/*!40000 ALTER TABLE `e_atlit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_banner
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_banner`;

CREATE TABLE `e_banner` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) DEFAULT NULL,
  `button` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `status` char(3) DEFAULT 'ON',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_banner` WRITE;
/*!40000 ALTER TABLE `e_banner` DISABLE KEYS */;

INSERT INTO `e_banner` (`id`, `judul`, `button`, `link`, `foto`, `status`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(7,'Halaman Depan','Klik Sekarang','https://facebook.com','assets/images/banner/d41d8cd98f00b204e9800998ecf8427e.png','ON',NULL,'2019-12-19 01:25:12',NULL,'2019-12-19 09:33:42'),
	(8,'Halaman Depan','Klik Sekarang','http://localhost/e-kompetisi/','assets/images/banner/d41d8cd98f00b204e9800998ecf8427e.png','ON',NULL,'2019-12-19 09:36:28',NULL,'2019-12-19 09:37:15'),
	(9,'Halaman Depan','Klik Sekarang','http://localhost/e-kompetisi/','assets/images/banner/d41d8cd98f00b204e9800998ecf8427e.png','ON',NULL,'2019-12-19 09:37:29',NULL,NULL);

/*!40000 ALTER TABLE `e_banner` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_cabor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_cabor`;

CREATE TABLE `e_cabor` (
  `id_cabor` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_cabor` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_cabor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_cabor` WRITE;
/*!40000 ALTER TABLE `e_cabor` DISABLE KEYS */;

INSERT INTO `e_cabor` (`id_cabor`, `nama_cabor`, `icon`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(5,'asd','assets/images/cabor/d41d8cd98f00b204e9800998ecf8427epng',NULL,'2019-11-29 21:40:07','2019-11-29 22:40:36','2019-11-29 22:40:36'),
	(6,'asd','assets/images/cabor/d41d8cd98f00b204e9800998ecf8427epng',NULL,'2019-11-29 21:40:07','2019-11-29 23:05:52','2019-11-29 23:05:52'),
	(7,'Coba','assets/images/cabor/d41d8cd98f00b204e9800998ecf8427epng',NULL,'2019-11-29 22:28:36','2019-11-29 22:43:24','2019-11-29 22:43:24'),
	(8,'Atletik','assets/images/cabor/d41d8cd98f00b204e9800998ecf8427epng',NULL,'2019-11-29 23:05:48','2019-11-29 23:06:43','2019-11-29 23:06:43'),
	(9,'Atletik','assets/images/cabor/d41d8cd98f00b204e9800998ecf8427epng',NULL,'2019-11-29 23:19:59','2019-11-30 00:15:18','2019-11-30 00:15:18'),
	(10,'Atletik','assets/images/cabor/d41d8cd98f00b204e9800998ecf8427epng',NULL,'2019-11-30 00:15:58',NULL,NULL),
	(11,'Catur','assets/images/cabor/d41d8cd98f00b204e9800998ecf8427epng',NULL,'2019-11-30 02:45:05',NULL,NULL);

/*!40000 ALTER TABLE `e_cabor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_foto
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_foto`;

CREATE TABLE `e_foto` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) DEFAULT NULL,
  `kategori` int(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `status` char(3) DEFAULT 'ON',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_foto` WRITE;
/*!40000 ALTER TABLE `e_foto` DISABLE KEYS */;

INSERT INTO `e_foto` (`id`, `judul`, `kategori`, `foto`, `status`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,NULL,NULL,'assets/images/foto/d41d8cd98f00b204e9800998ecf8427e.png','ON',NULL,'2019-12-19 03:19:56',NULL,'2019-12-19 03:20:21'),
	(2,'Jadwal Pertandingan',NULL,'assets/images/foto/d41d8cd98f00b204e9800998ecf8427e.png','ON',NULL,'2019-12-19 03:20:38',NULL,NULL),
	(3,'Jadwal Pertandingan',NULL,'assets/images/foto/d41d8cd98f00b204e9800998ecf8427e.png','ON',NULL,'2019-12-19 03:20:38',NULL,NULL),
	(4,'Jadwal Pertandingan',NULL,'assets/images/foto/d41d8cd98f00b204e9800998ecf8427e.png','ON',NULL,'2019-12-19 03:20:38',NULL,NULL),
	(5,'Jadwal Pertandingan',NULL,'assets/images/foto/d41d8cd98f00b204e9800998ecf8427e.png','ON',NULL,'2019-12-19 03:20:38',NULL,NULL);

/*!40000 ALTER TABLE `e_foto` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_informasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_informasi`;

CREATE TABLE `e_informasi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `judul` varchar(225) DEFAULT NULL,
  `isi` text,
  `foto` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `status` char(3) DEFAULT 'ON',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_informasi` WRITE;
/*!40000 ALTER TABLE `e_informasi` DISABLE KEYS */;

INSERT INTO `e_informasi` (`id`, `judul`, `isi`, `foto`, `file`, `status`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(4,'Jadwal Pertandingan','<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width:500px\">\r\n	<tbody>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n','assets/images/informasi/d41d8cd98f00b204e9800998ecf8427e.png','assets/images/informasi/d41d8cd98f00b204e9800998ecf8427e.png','ON',NULL,'2019-12-19 02:02:02',NULL,'2019-12-19 02:03:09'),
	(5,'Jadwal Pertandingan','<p>asd</p>\r\n','assets/images/informasi/d41d8cd98f00b204e9800998ecf8427e.png','assets/images/informasi/d41d8cd98f00b204e9800998ecf8427e.pdf','ON',NULL,'2019-12-19 02:03:29',NULL,'2019-12-19 02:26:21'),
	(6,'Jadwal Pertandingan X','<p>asdasd</p>\r\n','assets/images/informasi/d41d8cd98f00b204e9800998ecf8427e.png','assets/images/informasi/d41d8cd98f00b204e9800998ecf8427e.pdf','ON',NULL,'2019-12-19 02:17:48',NULL,'2019-12-19 02:26:17'),
	(7,'Jadwal Pertandingan','<p>asd</p>\r\n','Screen_Shot_2019-12-18_at_19_15_034.png','11.pdf','ON',NULL,'2019-12-19 02:24:38',NULL,NULL),
	(8,'Tugas Dan Fungsi','<p>asd</p>\r\n','043277b3026679bce21e7f15b28ad8a9.png','4a59e334ac42307d26b743178715e415.pdf','ON',NULL,'2019-12-19 02:26:12',NULL,NULL),
	(9,'Tugas Dan Fungsi','<p>asd</p>\r\n','043277b3026679bce21e7f15b28ad8a9.png','4a59e334ac42307d26b743178715e415.pdf','ON',NULL,'2019-12-19 02:26:12',NULL,NULL);

/*!40000 ALTER TABLE `e_informasi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kelas`;

CREATE TABLE `e_kelas` (
  `id_kelas` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_cabor` int(11) DEFAULT NULL,
  `jenis_kelas` char(2) DEFAULT NULL COMMENT '1=putra, 2=putri,3=campuraN',
  `nama_kelas` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kelas` WRITE;
/*!40000 ALTER TABLE `e_kelas` DISABLE KEYS */;

INSERT INTO `e_kelas` (`id_kelas`, `id_cabor`, `jenis_kelas`, `nama_kelas`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(5,10,'Pi','Lari 100 Meter',NULL,'2019-11-30 00:24:12','2019-12-10 13:20:03',NULL),
	(6,10,'Pa','Lari 500 Meter',NULL,'2019-11-30 03:15:19',NULL,NULL),
	(7,11,'Pa','Kelas 1',NULL,'2019-12-10 14:13:18',NULL,NULL),
	(8,11,'Pi','Kelas 2',NULL,'2019-12-10 14:13:25','2019-12-10 14:18:53',NULL);

/*!40000 ALTER TABLE `e_kelas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kompetisi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kompetisi`;

CREATE TABLE `e_kompetisi` (
  `id_kompetisi` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_kompetisi` varchar(255) DEFAULT NULL,
  `url` varchar(11) DEFAULT NULL,
  `deskripsi` text,
  `status` char(3) DEFAULT NULL COMMENT 'ON, OFF',
  `tahap` int(11) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kompetisi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kompetisi` WRITE;
/*!40000 ALTER TABLE `e_kompetisi` DISABLE KEYS */;

INSERT INTO `e_kompetisi` (`id_kompetisi`, `nama_kompetisi`, `url`, `deskripsi`, `status`, `tahap`, `icon`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'POSPEDA 2020',NULL,NULL,'ON',4,'assets/images/kompetisi/d41d8cd98f00b204e9800998ecf8427epng',NULL,'2019-12-01 23:15:36','2019-12-19 04:14:08',NULL),
	(2,'POSPEDA 2021',NULL,NULL,'ON',1,'assets/images/kompetisi/d41d8cd98f00b204e9800998ecf8427epng',NULL,'2019-12-19 03:54:22','2019-12-19 03:56:52',NULL);

/*!40000 ALTER TABLE `e_kompetisi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kompetisi_by_name_atlit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kompetisi_by_name_atlit`;

CREATE TABLE `e_kompetisi_by_name_atlit` (
  `id_kompetisi_by_name_atlit` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kompetisi` int(255) DEFAULT NULL,
  `id_kontingen` int(11) DEFAULT NULL,
  `id_cabor` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_atlit` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `sekolah` varchar(100) DEFAULT NULL,
  `kelas` varchar(100) DEFAULT NULL,
  `sksa` int(11) DEFAULT '1',
  `sksc` int(11) DEFAULT '1',
  `sttba` int(11) DEFAULT '1',
  `sttbc` int(11) DEFAULT '1',
  `raporta` int(11) DEFAULT '1',
  `raportc` int(11) DEFAULT '1',
  `aktaa` int(11) DEFAULT '1',
  `aktac` int(11) DEFAULT '1',
  `skda` int(11) DEFAULT '1',
  `skdc` int(11) DEFAULT '1',
  `nisna` int(11) DEFAULT '1',
  `nisnc` int(11) DEFAULT '1',
  `skskba` int(11) DEFAULT '1',
  `skskbc` int(11) DEFAULT '1',
  `khba` int(11) DEFAULT '1',
  `khbc` int(11) DEFAULT '1',
  `ket` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id_kompetisi_by_name_atlit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kompetisi_by_name_atlit` WRITE;
/*!40000 ALTER TABLE `e_kompetisi_by_name_atlit` DISABLE KEYS */;

INSERT INTO `e_kompetisi_by_name_atlit` (`id_kompetisi_by_name_atlit`, `id_kompetisi`, `id_kontingen`, `id_cabor`, `id_kelas`, `id_atlit`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `sekolah`, `kelas`, `sksa`, `sksc`, `sttba`, `sttbc`, `raporta`, `raportc`, `aktaa`, `aktac`, `skda`, `skdc`, `nisna`, `nisnc`, `skskba`, `skskbc`, `khba`, `khbc`, `ket`)
VALUES
	(1,1,4,10,5,9,NULL,'2019-12-17 01:12:57','2019-12-17 01:13:58','2019-12-17 01:13:58',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,1,4,10,5,13,NULL,'2019-12-17 01:14:03','2019-12-19 04:04:52','2019-12-19 04:04:52',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,1,4,10,6,8,NULL,'2019-12-17 01:14:21','2019-12-18 22:24:30',NULL,'qwe','5',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,0,0,'1'),
	(4,1,4,11,8,12,NULL,'2019-12-17 01:14:25','2019-12-18 22:34:21',NULL,'qwerr','1',1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,0,0,'1'),
	(5,1,4,10,5,11,NULL,'2019-12-17 02:28:51','2019-12-19 04:06:57',NULL,'','1',1,0,0,0,0,0,0,0,0,0,0,0,NULL,NULL,0,0,'0'),
	(6,1,4,10,6,10,NULL,'2019-12-17 02:36:06','2019-12-18 22:24:30',NULL,'qwe','5',0,0,0,0,0,1,0,0,0,0,0,0,NULL,NULL,0,0,'1'),
	(7,1,5,10,5,16,NULL,'2019-12-17 12:04:30','2019-12-18 22:16:22',NULL,'as','1',0,0,NULL,0,NULL,NULL,1,0,0,0,0,0,NULL,NULL,0,0,'1'),
	(8,1,5,10,6,14,NULL,'2019-12-17 12:04:43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(9,1,5,11,7,14,NULL,'2019-12-17 12:04:55',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(10,1,5,11,7,15,NULL,'2019-12-17 12:05:01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(11,1,5,11,8,16,NULL,'2019-12-17 12:05:13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(12,1,4,10,5,9,NULL,'2019-12-19 04:05:17','2019-12-19 04:06:57',NULL,'SD 1','3',0,0,1,1,1,1,1,1,1,1,1,1,NULL,NULL,1,0,'0');

/*!40000 ALTER TABLE `e_kompetisi_by_name_atlit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kompetisi_by_name_pelatih
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kompetisi_by_name_pelatih`;

CREATE TABLE `e_kompetisi_by_name_pelatih` (
  `id_kompetisi_by_name_pelatih` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kompetisi` int(255) DEFAULT NULL,
  `id_kontingen` int(11) DEFAULT NULL,
  `id_cabor` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_pelatih` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kompetisi_by_name_pelatih`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kompetisi_by_name_pelatih` WRITE;
/*!40000 ALTER TABLE `e_kompetisi_by_name_pelatih` DISABLE KEYS */;

INSERT INTO `e_kompetisi_by_name_pelatih` (`id_kompetisi_by_name_pelatih`, `id_kompetisi`, `id_kontingen`, `id_cabor`, `id_kelas`, `id_pelatih`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,4,10,5,2,NULL,'2019-12-17 11:18:55','2019-12-17 11:24:51','2019-12-17 11:24:51'),
	(2,1,4,10,6,1,NULL,'2019-12-17 11:21:33',NULL,NULL),
	(3,1,4,11,8,2,NULL,'2019-12-17 11:21:41',NULL,NULL),
	(4,1,4,10,5,2,NULL,'2019-12-17 11:44:28',NULL,NULL),
	(5,1,5,10,5,4,NULL,'2019-12-17 12:04:37','2019-12-17 12:32:11','2019-12-17 12:32:11'),
	(6,1,5,10,6,3,NULL,'2019-12-17 12:04:50',NULL,NULL),
	(7,1,5,11,7,3,NULL,'2019-12-17 12:05:07',NULL,NULL),
	(8,1,5,11,8,4,NULL,'2019-12-17 12:05:19',NULL,NULL),
	(9,1,5,10,5,4,NULL,'2019-12-17 20:43:44','2019-12-18 15:21:46','2019-12-18 15:21:46'),
	(10,1,5,10,5,4,NULL,'2019-12-18 15:21:52',NULL,NULL);

/*!40000 ALTER TABLE `e_kompetisi_by_name_pelatih` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kompetisi_by_number
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kompetisi_by_number`;

CREATE TABLE `e_kompetisi_by_number` (
  `id_kompetisi_by_number` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kompetisi` int(255) DEFAULT NULL,
  `id_kontingen` int(11) DEFAULT NULL,
  `id_cabor` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kompetisi_by_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kompetisi_by_number` WRITE;
/*!40000 ALTER TABLE `e_kompetisi_by_number` DISABLE KEYS */;

INSERT INTO `e_kompetisi_by_number` (`id_kompetisi_by_number`, `id_kompetisi`, `id_kontingen`, `id_cabor`, `id_kelas`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,5,10,5,NULL,'2019-12-15 19:10:52',NULL,NULL),
	(2,1,5,10,6,NULL,'2019-12-15 19:10:57',NULL,NULL),
	(3,1,5,11,7,NULL,'2019-12-15 19:11:02',NULL,NULL),
	(4,1,5,11,8,NULL,'2019-12-15 19:11:07',NULL,NULL),
	(5,1,4,10,5,NULL,'2019-12-15 19:11:32',NULL,NULL),
	(6,1,4,10,6,NULL,'2019-12-15 19:11:37',NULL,NULL),
	(7,1,4,11,8,NULL,'2019-12-15 19:11:43',NULL,NULL),
	(8,1,5,10,5,NULL,'2019-12-15 23:03:29','2019-12-15 23:03:38','2019-12-15 23:03:38');

/*!40000 ALTER TABLE `e_kompetisi_by_number` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kompetisi_cabor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kompetisi_cabor`;

CREATE TABLE `e_kompetisi_cabor` (
  `id_kompetisi_cabor` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kompetisi` int(255) DEFAULT NULL,
  `id_cabor` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kompetisi_cabor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kompetisi_cabor` WRITE;
/*!40000 ALTER TABLE `e_kompetisi_cabor` DISABLE KEYS */;

INSERT INTO `e_kompetisi_cabor` (`id_kompetisi_cabor`, `id_kompetisi`, `id_cabor`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,10,NULL,'2019-11-30 02:45:50',NULL,NULL),
	(2,1,11,NULL,'2019-11-30 02:45:50',NULL,NULL),
	(3,2,10,NULL,'2019-12-19 03:56:37',NULL,NULL),
	(4,2,11,NULL,'2019-12-19 03:56:37','2019-12-19 03:57:13','2019-12-19 03:57:13');

/*!40000 ALTER TABLE `e_kompetisi_cabor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kompetisi_keikutsertaan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kompetisi_keikutsertaan`;

CREATE TABLE `e_kompetisi_keikutsertaan` (
  `id_kompetisi_keikutsertaan` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kompetisi` int(255) DEFAULT NULL,
  `id_kontingen` int(11) DEFAULT NULL,
  `id_cabor` int(11) DEFAULT NULL,
  `kelas_pa` int(11) DEFAULT NULL,
  `kelas_pi` int(11) DEFAULT NULL,
  `atlit_pa` int(11) DEFAULT '0',
  `atlit_pi` int(11) DEFAULT '0',
  `pelatih_pa` int(11) DEFAULT '0',
  `pelatih_pi` int(11) NOT NULL DEFAULT '0',
  `generate_by_kontingen` int(11) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kompetisi_keikutsertaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kompetisi_keikutsertaan` WRITE;
/*!40000 ALTER TABLE `e_kompetisi_keikutsertaan` DISABLE KEYS */;

INSERT INTO `e_kompetisi_keikutsertaan` (`id_kompetisi_keikutsertaan`, `id_kompetisi`, `id_kontingen`, `id_cabor`, `kelas_pa`, `kelas_pi`, `atlit_pa`, `atlit_pi`, `pelatih_pa`, `pelatih_pi`, `generate_by_kontingen`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,4,10,NULL,NULL,2,2,1,1,1,NULL,'2019-12-15 19:09:34','2019-12-15 19:33:17',NULL),
	(2,1,4,11,NULL,NULL,1,1,1,1,1,NULL,'2019-12-15 19:09:42','2019-12-15 19:33:26',NULL),
	(3,1,5,10,NULL,NULL,1,1,1,1,1,NULL,'2019-12-15 19:10:19','2019-12-15 22:53:36',NULL),
	(4,1,5,11,NULL,NULL,2,1,1,1,1,NULL,'2019-12-15 19:10:26','2019-12-15 22:54:15',NULL),
	(5,2,4,10,NULL,NULL,5,5,1,1,0,NULL,'2019-12-19 03:57:23',NULL,NULL);

/*!40000 ALTER TABLE `e_kompetisi_keikutsertaan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kompetisi_keikutsertaan_atlit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kompetisi_keikutsertaan_atlit`;

CREATE TABLE `e_kompetisi_keikutsertaan_atlit` (
  `id_kompetisi_keikutsertaan_detail` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kompetisi_keikutsertaan` int(11) DEFAULT NULL,
  `id_kompetisi` int(255) DEFAULT NULL,
  `id_kontingen` int(11) DEFAULT NULL,
  `id_cabor` int(11) DEFAULT NULL,
  `id_atlit` int(11) DEFAULT NULL,
  `jk` char(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kompetisi_keikutsertaan_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kompetisi_keikutsertaan_atlit` WRITE;
/*!40000 ALTER TABLE `e_kompetisi_keikutsertaan_atlit` DISABLE KEYS */;

INSERT INTO `e_kompetisi_keikutsertaan_atlit` (`id_kompetisi_keikutsertaan_detail`, `id_kompetisi_keikutsertaan`, `id_kompetisi`, `id_kontingen`, `id_cabor`, `id_atlit`, `jk`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,1,4,10,8,'Pa',NULL,'2019-12-15 19:33:17',NULL,NULL),
	(2,1,1,4,10,10,'Pa',NULL,'2019-12-15 19:33:17',NULL,NULL),
	(3,1,1,4,10,9,'Pi',NULL,'2019-12-15 19:33:17',NULL,NULL),
	(4,1,1,4,10,11,'Pi',NULL,'2019-12-15 19:33:17',NULL,NULL),
	(5,2,1,4,11,8,'Pa',NULL,'2019-12-15 19:33:26',NULL,NULL),
	(6,2,1,4,11,12,'Pi',NULL,'2019-12-15 19:33:26',NULL,NULL),
	(8,3,1,5,10,16,'Pi',NULL,'2019-12-15 22:53:36',NULL,NULL),
	(9,4,1,5,11,14,'Pa',NULL,'2019-12-15 22:54:15',NULL,NULL),
	(10,4,1,5,11,15,'Pa',NULL,'2019-12-15 22:54:15',NULL,NULL);

/*!40000 ALTER TABLE `e_kompetisi_keikutsertaan_atlit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kompetisi_keikutsertaan_pelatih
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kompetisi_keikutsertaan_pelatih`;

CREATE TABLE `e_kompetisi_keikutsertaan_pelatih` (
  `id_kompetisi_keikutsertaan_detail` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kompetisi_keikutsertaan` int(11) DEFAULT NULL,
  `id_kompetisi` int(255) DEFAULT NULL,
  `id_kontingen` int(11) DEFAULT NULL,
  `id_cabor` int(11) DEFAULT NULL,
  `id_pelatih` int(11) DEFAULT NULL,
  `jk` char(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kompetisi_keikutsertaan_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kompetisi_keikutsertaan_pelatih` WRITE;
/*!40000 ALTER TABLE `e_kompetisi_keikutsertaan_pelatih` DISABLE KEYS */;

INSERT INTO `e_kompetisi_keikutsertaan_pelatih` (`id_kompetisi_keikutsertaan_detail`, `id_kompetisi_keikutsertaan`, `id_kompetisi`, `id_kontingen`, `id_cabor`, `id_pelatih`, `jk`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,1,4,10,1,'Pa',NULL,'2019-12-15 19:33:17',NULL,NULL),
	(2,1,1,4,10,2,'Pi',NULL,'2019-12-15 19:33:17',NULL,NULL),
	(3,2,1,4,11,1,'Pa',NULL,'2019-12-15 19:33:26',NULL,NULL),
	(4,2,1,4,11,2,'Pi',NULL,'2019-12-15 19:33:26',NULL,NULL),
	(5,3,1,5,10,3,'Pa',NULL,'2019-12-15 22:53:36',NULL,NULL),
	(6,3,1,5,10,4,'Pi',NULL,'2019-12-15 22:53:36',NULL,NULL),
	(7,4,1,5,11,3,'Pa',NULL,'2019-12-15 22:54:15',NULL,NULL),
	(8,4,1,5,11,4,'Pi',NULL,'2019-12-15 22:54:15',NULL,NULL);

/*!40000 ALTER TABLE `e_kompetisi_keikutsertaan_pelatih` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kompetisi_kelas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kompetisi_kelas`;

CREATE TABLE `e_kompetisi_kelas` (
  `id_kompetisi_kelas` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kompetisi_cabor` int(11) DEFAULT NULL,
  `id_kompetisi` int(11) DEFAULT NULL,
  `id_cabor` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kompetisi_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kompetisi_kelas` WRITE;
/*!40000 ALTER TABLE `e_kompetisi_kelas` DISABLE KEYS */;

INSERT INTO `e_kompetisi_kelas` (`id_kompetisi_kelas`, `id_kompetisi_cabor`, `id_kompetisi`, `id_cabor`, `id_kelas`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(11,1,1,10,5,NULL,'2019-11-30 03:25:01','2019-12-10 16:03:40','2019-12-10 16:03:40'),
	(12,1,1,10,6,NULL,'2019-11-30 03:25:01','2019-12-10 16:03:45','2019-12-10 16:03:45'),
	(13,NULL,NULL,NULL,7,NULL,'2019-12-10 15:59:11',NULL,NULL),
	(14,NULL,NULL,NULL,8,NULL,'2019-12-10 15:59:11',NULL,NULL),
	(15,2,1,11,7,NULL,'2019-12-10 16:00:38','2019-12-10 16:04:03','2019-12-10 16:04:03'),
	(16,2,1,11,8,NULL,'2019-12-10 16:00:38','2019-12-10 16:03:36','2019-12-10 16:03:36'),
	(17,1,1,10,5,NULL,'2019-12-10 16:03:51','2019-12-10 17:14:41','2019-12-10 17:14:41'),
	(18,1,1,10,6,NULL,'2019-12-10 16:03:51','2019-12-10 17:14:38','2019-12-10 17:14:38'),
	(19,2,1,11,7,NULL,'2019-12-10 17:12:43','2019-12-10 17:13:36','2019-12-10 17:13:36'),
	(20,2,1,11,8,NULL,'2019-12-10 17:12:43','2019-12-10 17:12:48','2019-12-10 17:12:48'),
	(21,2,1,11,7,NULL,'2019-12-10 17:13:43',NULL,NULL),
	(22,2,1,11,8,NULL,'2019-12-10 17:13:43',NULL,NULL),
	(23,1,1,10,5,NULL,'2019-12-10 17:14:46',NULL,NULL),
	(24,1,1,10,6,NULL,'2019-12-10 17:14:46','2019-12-10 17:14:56','2019-12-10 17:14:56'),
	(25,1,1,10,6,NULL,'2019-12-10 17:15:00',NULL,NULL);

/*!40000 ALTER TABLE `e_kompetisi_kelas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kompetisi_kontingen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kompetisi_kontingen`;

CREATE TABLE `e_kompetisi_kontingen` (
  `id_kompetisi_kontingen` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kompetisi` int(255) DEFAULT NULL,
  `id_kontingen` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kompetisi_kontingen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kompetisi_kontingen` WRITE;
/*!40000 ALTER TABLE `e_kompetisi_kontingen` DISABLE KEYS */;

INSERT INTO `e_kompetisi_kontingen` (`id_kompetisi_kontingen`, `id_kompetisi`, `id_kontingen`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,4,NULL,'2019-12-01 23:17:55',NULL,NULL),
	(2,1,5,NULL,'2019-12-01 23:17:55',NULL,NULL),
	(3,1,6,NULL,'2019-12-01 23:17:55',NULL,NULL),
	(4,1,7,NULL,'2019-12-01 23:17:55',NULL,NULL),
	(5,1,8,NULL,'2019-12-01 23:17:55',NULL,NULL),
	(6,1,12,NULL,'2019-12-01 23:19:38',NULL,NULL),
	(7,1,38,NULL,'2019-12-01 23:19:38',NULL,NULL),
	(8,2,4,NULL,'2019-12-19 03:56:18',NULL,NULL);

/*!40000 ALTER TABLE `e_kompetisi_kontingen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kompetisi_medali
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kompetisi_medali`;

CREATE TABLE `e_kompetisi_medali` (
  `id_kompetisi_medali` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_kompetisi` int(11) DEFAULT NULL,
  `id_kontingen` int(11) DEFAULT NULL,
  `id_cabor` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_atlit` int(11) DEFAULT NULL,
  `id_medali` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kompetisi_medali`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kompetisi_medali` WRITE;
/*!40000 ALTER TABLE `e_kompetisi_medali` DISABLE KEYS */;

INSERT INTO `e_kompetisi_medali` (`id_kompetisi_medali`, `id_kompetisi`, `id_kontingen`, `id_cabor`, `id_kelas`, `id_atlit`, `id_medali`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,1,4,11,7,8,1,NULL,'2019-12-19 09:20:19',NULL,NULL),
	(2,1,5,11,7,15,2,NULL,'2019-12-19 09:22:56',NULL,NULL),
	(3,1,4,10,5,11,2,NULL,'2019-12-19 09:32:46',NULL,NULL);

/*!40000 ALTER TABLE `e_kompetisi_medali` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kontingen
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kontingen`;

CREATE TABLE `e_kontingen` (
  `id_kontingen` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kode_kontingen` int(11) DEFAULT NULL,
  `nama_kontingen` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_kontingen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kontingen` WRITE;
/*!40000 ALTER TABLE `e_kontingen` DISABLE KEYS */;

INSERT INTO `e_kontingen` (`id_kontingen`, `kode_kontingen`, `nama_kontingen`, `icon`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `user`)
VALUES
	(1,3300,'PROVINSI JAWA TENGAH','assets/images/kontingen/Provinsi_Jawa_Tengah.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'PROVINSI_JAWA_TENGAH'),
	(4,3301,'KABUPATEN CILACAP','assets/images/kontingen/Kabupaten_Cilacap.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_CILACAP'),
	(5,3302,'KABUPATEN BANYUMAS','assets/images/kontingen/Kabupaten_Banyumas.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_BANYUMAS'),
	(6,3303,'KABUPATEN PURBALINGGA','assets/images/kontingen/Kabupaten_Purbalingga.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_PURBALINGGA'),
	(7,3304,'KABUPATEN BANJARNEGARA','assets/images/kontingen/Kabupaten_Banjarnegara.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_BANJARNEGARA'),
	(8,3305,'KABUPATEN KEBUMEN','assets/images/kontingen/Kabupaten_Kebumen.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_KEBUMEN'),
	(9,3306,'KABUPATEN PURWOREJO','assets/images/kontingen/Kabupaten_Purworejo.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_PURWOREJO'),
	(10,3307,'KABUPATEN WONOSOBO','assets/images/kontingen/Kabupaten_Wonosobo.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_WONOSOBO'),
	(11,3308,'KABUPATEN MAGELANG','assets/images/kontingen/Kabupaten_Magelang.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_MAGELANG'),
	(12,3309,'KABUPATEN BOYOLALI','assets/images/kontingen/Kabupaten_Boyolali.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_BOYOLALI'),
	(13,3310,'KABUPATEN KLATEN','assets/images/kontingen/Kabupaten_Klaten.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_KLATEN'),
	(14,3311,'KABUPATEN SUKOHARJO','assets/images/kontingen/Kabupaten_Sukoharjo.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_SUKOHARJO'),
	(15,3312,'KABUPATEN WONOGIRI','assets/images/kontingen/Kabupaten_Wonogiri.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_WONOGIRI'),
	(16,3313,'KABUPATEN KARANGANYAR','assets/images/kontingen/Kabupaten_Karanganyar.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_KARANGANYAR'),
	(17,3314,'KABUPATEN SRAGEN','assets/images/kontingen/Kabupaten_Sragen.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_SRAGEN'),
	(18,3315,'KABUPATEN GROBOGAN','assets/images/kontingen/Kabupaten_Grobogan.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_GROBOGAN'),
	(19,3316,'KABUPATEN BLORA','assets/images/kontingen/Kabupaten_Blora.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_BLORA'),
	(20,3317,'KABUPATEN REMBANG','assets/images/kontingen/Kabupaten_Rembang.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_REMBANG'),
	(21,3318,'KABUPATEN PATI','assets/images/kontingen/Kabupaten_Pati.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_PATI'),
	(22,3319,'KABUPATEN KUDUS','assets/images/kontingen/Kabupaten_Kudus.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_KUDUS'),
	(23,3320,'KABUPATEN JEPARA','assets/images/kontingen/Kabupaten_Jepara.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_JEPARA'),
	(24,3321,'KABUPATEN DEMAK','assets/images/kontingen/Kabupaten_Demak.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_DEMAK'),
	(25,3322,'KABUPATEN SEMARANG','assets/images/kontingen/Kabupaten_Semarang.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_SEMARANG'),
	(26,3323,'KABUPATEN TEMANGGUNG','assets/images/kontingen/Kabupaten_Temanggung.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_TEMANGGUNG'),
	(27,3324,'KABUPATEN KENDAL','assets/images/kontingen/Kabupaten_Kendal.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_KENDAL'),
	(28,3325,'KABUPATEN BATANG','assets/images/kontingen/Kabupaten_Batang.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_BATANG'),
	(29,3326,'KABUPATEN PEKALONGAN','assets/images/kontingen/Kabupaten_Pekalongan.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_PEKALONGAN'),
	(30,3327,'KABUPATEN PEMALANG','assets/images/kontingen/Kabupaten_Pemalang.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_PEMALANG'),
	(31,3328,'KABUPATEN TEGAL','assets/images/kontingen/Kabupaten_Tegal.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_TEGAL'),
	(32,3329,'KABUPATEN BREBES','assets/images/kontingen/Kabupaten_Brebes.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KABUPATEN_BREBES'),
	(33,3371,'KOTA MAGELANG','assets/images/kontingen/Kota_Magelang.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KOTA_MAGELANG'),
	(34,3372,'KOTA SURAKARTA','assets/images/kontingen/Kota_Surakarta.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KOTA_SURAKARTA'),
	(35,3373,'KOTA SALATIGA','assets/images/kontingen/Kota_Salatiga.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KOTA_SALATIGA'),
	(36,3374,'KOTA SEMARANG','assets/images/kontingen/Kota_Semarang.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KOTA_SEMARANG'),
	(37,3375,'KOTA PEKALONGAN','assets/images/kontingen/Kota_Pekalongan.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KOTA_PEKALONGAN'),
	(38,3376,'KOTA TEGAL','assets/images/kontingen/Kota_Tegal.png',NULL,NULL,'2019-12-01 21:56:05',NULL,'KOTA_TEGAL');

/*!40000 ALTER TABLE `e_kontingen` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_kontingen_atlit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_kontingen_atlit`;

CREATE TABLE `e_kontingen_atlit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `kontingen_id` int(11) DEFAULT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `tempat_lahir` varchar(50) NOT NULL DEFAULT '-',
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `hp` varchar(25) DEFAULT NULL,
  `foto` text,
  `jk` varchar(15) DEFAULT NULL,
  `berat` decimal(10,2) DEFAULT NULL,
  `tinggi` decimal(10,2) DEFAULT NULL,
  `nisn` varchar(35) NOT NULL,
  `nomor_ijazah` varchar(35) NOT NULL,
  `nomor_akta_lahir` varchar(35) NOT NULL,
  `verifikasi` varchar(1) NOT NULL DEFAULT '0',
  `sekolah` text,
  `kelas` varchar(5) DEFAULT NULL,
  `tn` varchar(1) DEFAULT '0',
  `tg` varchar(1) DEFAULT '0',
  `tr` varchar(1) DEFAULT '0',
  `td` varchar(1) DEFAULT '0',
  `kka` varchar(1) DEFAULT '0',
  `kkf` varchar(1) DEFAULT '0',
  `tra` varchar(1) DEFAULT '0',
  `trf` varchar(1) DEFAULT '0',
  `tga` varchar(1) DEFAULT '0',
  `tgf` varchar(1) DEFAULT '0',
  `sksa` varchar(1) DEFAULT '0',
  `sksf` varchar(1) DEFAULT '0',
  `rsa` varchar(1) DEFAULT '0',
  `rsf` varchar(1) DEFAULT '0',
  `isa` varchar(1) DEFAULT '0',
  `isf` varchar(1) DEFAULT '0',
  `aktaa` varchar(1) DEFAULT '0',
  `aktaf` varchar(1) DEFAULT '0',
  `skda` varchar(1) DEFAULT '0',
  `skdf` varchar(1) DEFAULT '0',
  `ket` varchar(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_kontingen_atlit` WRITE;
/*!40000 ALTER TABLE `e_kontingen_atlit` DISABLE KEYS */;

INSERT INTO `e_kontingen_atlit` (`id`, `kontingen_id`, `nama`, `tempat_lahir`, `tgl_lahir`, `email`, `hp`, `foto`, `jk`, `berat`, `tinggi`, `nisn`, `nomor_ijazah`, `nomor_akta_lahir`, `verifikasi`, `sekolah`, `kelas`, `tn`, `tg`, `tr`, `td`, `kka`, `kkf`, `tra`, `trf`, `tga`, `tgf`, `sksa`, `sksf`, `rsa`, `rsf`, `isa`, `isf`, `aktaa`, `aktaf`, `skda`, `skdf`, `ket`)
VALUES
	(3,18,'SULISTYO','PURWOREJO','2003-09-25','-','-','-','Laki - Laki',59.00,170.00,'\'0034600541','-','132/II/2005','0',NULL,NULL,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
	(4,18,'','','0000-00-00','','','-','Laki - Laki',0.00,0.00,'-','-','-','0',NULL,NULL,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
	(5,18,'','','0000-00-00','','','-','Laki - Laki',0.00,0.00,'-','-','-','0',NULL,NULL,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
	(6,19,'LATIFA NUR HASANAH','KEBUMEN','2003-10-25','-','081915050382','-','Perempuan',45.00,158.00,'0031136280','009/MTs.11.05.715/PP.01.1/05/2018','7784/TP/2007','0',NULL,NULL,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
	(7,22,'ERFIDA AZKIYATUN NAIMAH','KEBUMEN','2004-03-20','-','-','-','Perempuan',53.00,159.00,'0043458315','DN-03Dd/060120999','3305-LT-26092016-0012','0',NULL,NULL,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
	(8,21,'RIZQI MAULIDA MAFTUCHAH','KEBUMEN','2003-06-19','-','085747480526','-','Perempuan',41.00,149.00,'0030774275','DN-Dp/130534152','5199/2003','0',NULL,NULL,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
	(9,44,'ERIKA FADHILASARI','PATI','2002-05-12','bidporapati@gmail.com','081327595693','1565273097ERIKA.JPG','Perempuan',40.00,165.00,'0023099618','20364132','19207511927564','0',NULL,NULL,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
	(10,45,'ERIKA FADHILASARI','PATI','2002-05-12','bidporapati@gmail.com  ','081327595693','1565273130ERIKA.JPG','Perempuan',40.00,165.00,'0023099618','20364132  ',' 19207511927564 ','0',NULL,NULL,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
	(11,41,'ADIT AJUN PRASETYA ','PATI','2003-12-26','bidporapati@gmail.com','081391859841','1565273736ADIT.JPG','Laki - Laki',50.00,162.00,'0016754464','20338980','3318042612030002','0',NULL,NULL,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
	(12,42,'ADIT AJUN PRASETYA ','PATI','2003-12-26','bidporapati@gmail.com','081391859841 ','1565273762ADIT.JPG','Laki - Laki',50.00,162.00,'0016754464  ','20338980  ','3318042612030002 ','0',NULL,NULL,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');

/*!40000 ALTER TABLE `e_kontingen_atlit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_pelatih
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_pelatih`;

CREATE TABLE `e_pelatih` (
  `id_pelatih` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nik` varchar(255) DEFAULT NULL,
  `nama_pelatih` varchar(255) DEFAULT NULL,
  `jk` varchar(255) DEFAULT NULL,
  `alamat` text,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `tinggi_badan` varchar(255) DEFAULT NULL,
  `berat_badan` varchar(255) DEFAULT NULL,
  `no_hp` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `id_kontingen` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_pelatih`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `e_pelatih` WRITE;
/*!40000 ALTER TABLE `e_pelatih` DISABLE KEYS */;

INSERT INTO `e_pelatih` (`id_pelatih`, `nik`, `nama_pelatih`, `jk`, `alamat`, `tempat_lahir`, `tanggal_lahir`, `tinggi_badan`, `berat_badan`, `no_hp`, `email`, `foto`, `id_kontingen`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'1234124324','Sukro','Pa','smg','Demak','2019-12-04','170','80',NULL,NULL,'assets/images/pelatih/d41d8cd98f00b204e9800998ecf8427e.png',4,'2019-12-01 22:19:31',NULL,NULL),
	(2,'123','123','Pi','123','123','2019-12-27','123','123',NULL,NULL,'assets/images/pelatih/d41d8cd98f00b204e9800998ecf8427e.png',4,'2019-12-02 13:57:46','2019-12-07 00:56:26',NULL),
	(3,'645667','3455','Pa','123','Demak','2019-12-25','54','345',NULL,NULL,'assets/images/pelatih/d41d8cd98f00b204e9800998ecf8427e.png',5,'2019-12-10 17:24:56',NULL,NULL),
	(4,'345','Putri X','Pi','demak kelurahan anjar','Grobogan','2019-12-13','34','324',NULL,NULL,'assets/images/pelatih/d41d8cd98f00b204e9800998ecf8427e.png',5,'2019-12-10 17:25:19',NULL,NULL);

/*!40000 ALTER TABLE `e_pelatih` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_tahap
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_tahap`;

CREATE TABLE `e_tahap` (
  `id_tahap` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_tahap` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tahap`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_tahap` WRITE;
/*!40000 ALTER TABLE `e_tahap` DISABLE KEYS */;

INSERT INTO `e_tahap` (`id_tahap`, `nama_tahap`)
VALUES
	(1,'Tahap I - Keikutsertaaan'),
	(2,'Tahap II - By Number'),
	(3,'Tahap III - By Name'),
	(4,'Tahap IV - Keabsahan'),
	(5,'Tahap V - Masa Kompetisi - Pembagian Medali'),
	(6,'Tahap VI - Selesai');

/*!40000 ALTER TABLE `e_tahap` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table e_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `e_user`;

CREATE TABLE `e_user` (
  `id_user` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username_` varchar(50) DEFAULT NULL,
  `password_` varchar(255) DEFAULT NULL,
  `id_kontingen` int(11) DEFAULT NULL,
  `akses` int(11) DEFAULT NULL,
  `status` char(3) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `e_user` WRITE;
/*!40000 ALTER TABLE `e_user` DISABLE KEYS */;

INSERT INTO `e_user` (`id_user`, `username_`, `password_`, `id_kontingen`, `akses`, `status`, `last_login`, `created_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(2,'admin','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',1,99,'ON',NULL,NULL,'2019-11-30 16:04:32','2019-12-19 03:53:55',NULL),
	(68,'PROVINSI_JAWA_TENGAH','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',1,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23','2019-12-01 22:03:01'),
	(69,'KABUPATEN_CILACAP','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',4,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(70,'KABUPATEN_BANYUMAS','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',5,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(71,'KABUPATEN_PURBALINGGA','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',6,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(72,'KABUPATEN_BANJARNEGARA','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',7,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(73,'KABUPATEN_KEBUMEN','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',8,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(74,'KABUPATEN_PURWOREJO','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',9,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(75,'KABUPATEN_WONOSOBO','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',10,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(76,'KABUPATEN_MAGELANG','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',11,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(77,'KABUPATEN_BOYOLALI','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',12,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(78,'KABUPATEN_KLATEN','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',13,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(79,'KABUPATEN_SUKOHARJO','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',14,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(80,'KABUPATEN_WONOGIRI','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',15,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(81,'KABUPATEN_KARANGANYAR','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',16,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(82,'KABUPATEN_SRAGEN','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',17,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(83,'KABUPATEN_GROBOGAN','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',18,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(84,'KABUPATEN_BLORA','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',19,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(85,'KABUPATEN_REMBANG','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',20,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(86,'KABUPATEN_PATI','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',21,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(87,'KABUPATEN_KUDUS','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',22,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(88,'KABUPATEN_JEPARA','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',23,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(89,'KABUPATEN_DEMAK','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',24,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(90,'KABUPATEN_SEMARANG','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',25,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(91,'KABUPATEN_TEMANGGUNG','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',26,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(92,'KABUPATEN_KENDAL','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',27,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(93,'KABUPATEN_BATANG','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',28,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(94,'KABUPATEN_PEKALONGAN','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',29,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(95,'KABUPATEN_PEMALANG','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',30,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(96,'KABUPATEN_TEGAL','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',31,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(97,'KABUPATEN_BREBES','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',32,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(98,'KOTA_MAGELANG','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',33,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(99,'KOTA_SURAKARTA','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',34,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(100,'KOTA_SALATIGA','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',35,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(101,'KOTA_SEMARANG','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',36,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(102,'KOTA_PEKALONGAN','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',37,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL),
	(103,'KOTA_TEGAL','$2y$11$WH.eVDznfyd6M9A77nixqesGgwMeRpuBZioIAi/NE/8iErYIX3zxy',38,88,'ON',NULL,NULL,NULL,'2019-12-01 22:05:23',NULL);

/*!40000 ALTER TABLE `e_user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
