<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pelatih extends CI_Controller
{
    function __construct()
    {
		parent::__construct();
		$this->akses = $this->session->userdata('akses');
		$this->kontingen = $this->session->userdata('kontingen');
    }

    public function index()
	{
		if($this->akses == '99'){
			$data['alldata'] = $this->Crud_model->get_all('e_pelatih');
		}else{
			$data['alldata'] = $this->Crud_model->get_all_by('e_pelatih', 'id_kontingen', $this->kontingen);
		}
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatih/index',$data);
	}
	
	public function pelatih($id)
	{
		
			$data['alldata'] = $this->Crud_model->get_all_by('e_pelatih', 'id_kontingen', $id);
		
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatih/index',$data);
    }

    public function add()
	{
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/pelatih/add');
    }

    public function adds(){
		//image
		ini_set('max_execution_time', 300);
		$ext = pathinfo($_FILES['foto']['name']);
		$date = date('Y-m-d H:i:s');
		$filename = md5($this->input->post('foto').$date).'.'.$ext['extension'];
		$config['upload_path']   = './assets/images/pelatih/'; 
		$config['allowed_types'] = 'png|jpg|jpeg';
        $config['max_size'] = '500';
        $config['file_name'] = $filename;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
			if (!empty($_FILES['foto']['name'])) {
				$upload = $this->upload->data(); 
				$savenamefile = 'assets/images/pelatih/' . $filename;
			}
			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
			$config['cacheable']    = true; //boolean, the default is true
			$config['cachedir']     = './assets/'; //string, the default is application/cache/
			$config['errorlog']     = './assets/'; //string, the default is application/logs/
			$config['imagedir']     = './assets/qr/'; //direktori penyimpanan qr code
			$config['quality']      = true; //boolean, the default is true
			$config['size']         = '1024'; //interger, the default is 1024
			$config['black']        = array(224,255,255); // array, default is array(255,255,255)
			$config['white']        = array(70,130,180); // array, default is array(0,0,0)
			$this->ciqrcode->initialize($config);
			$image_name=md5($this->input->post('nik')).'-'.md5(date('Y-m-d H:i:s')).'.png'; //buat name dari qr code sesuai dengan nim
			$params['data'] = 'http://simdisporapar.jatengprov.go.id/databasepora/e-kompetisi/publik/cek_pelatih/'.$image_name; //data yang akan di jadikan QR CODE
			$params['level'] = 'H'; //H=High
			$params['size'] = 10;
			$params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
			$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
			$data = array(
                'nik' => $this->input->post('nik'),
				'nama_pelatih' => $this->input->post('nama_pelatih'),
				'jk' => $this->input->post('jk'),
				'alamat' => $this->input->post('alamat'),
				'tempat_lahir' => $this->input->post('tempat_lahir'), 
				'tanggal_lahir' => date('Y-m-d', strtotime($this->input->post('tanggal_lahir'))),
				'tinggi_badan' => $this->input->post('tinggi'),
				'berat_badan' => $this->input->post('berat'),
				'foto' => $savenamefile,
				'id_kontingen' => $this->kontingen,
				'created_at' => date('Y-m-d H:i:s'),
				'qr' => $image_name,
			);
			$insert = $this->Crud_model->insert('e_pelatih', $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('pelatih'));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('pelatih'));
			}
		} else { 
			if (($_FILES['foto']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('pelatih'));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload foto');
				redirect(site_url('pelatih'));
			}
		
		}
	}
	
	public function adds_foto(){
		$id_pelatih = $this->input->post('id_pelatih');
		$det = $this->Crud_model->get_by_id('e_pelatih', 'id_pelatih', $id_pelatih);
		if($det != false){
		//image
		ini_set('max_execution_time', 300);
		$ext = pathinfo($_FILES['foto']['name']);
		$date = date('Y-m-d H:i:s');
		$filename = md5($this->input->post('foto').$date).'.'.$ext['extension'];
		$config['upload_path']   = './assets/images/pelatih/'; 
		$config['allowed_types'] = 'png|jpg|jpeg';
        $config['max_size'] = '500';
        $config['file_name'] = $filename;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
			if (!empty($_FILES['foto']['name'])) {
				$upload = $this->upload->data(); 
				$savenamefile = 'assets/images/pelatih/' . $filename;
			}
			$data = array(
				'id_pelatih' => $id_pelatih,
				'foto' => $savenamefile
			);
			$insert = $this->Crud_model->update('e_pelatih', 'id_pelatih', $id_pelatih, $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('pelatih/detail/'.$id_pelatih));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('pelatih/detail/'.$id_pelatih));
			}
		} else { 
			if (($_FILES['foto']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('pelatih/detail/'.$id_pelatih));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload foto');
				redirect(site_url('pelatih/detail/'.$id_pelatih));
			}
		
		}
		}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'pelatih Tidak di Temukan');
				redirect(site_url('pelatih'));
		}
	}
    
    public function hapus($id){
        $del = $this->Crud_model->is_delete('e_pelatih', 'id_pelatih', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('pelatih'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('pelatih'));
        }
	}
	
	public function detail($id_pelatih)
	{
		$det = $this->Crud_model->get_by_id('e_pelatih', 'id_pelatih', $id_pelatih);
		if($det != false){
				$data['detail'] = $det;
				$this->load->view('backend/template/head');
				$this->load->view('backend/template/header');
				$this->load->view('backend/template/sidebar');
				$this->load->view('backend/pelatih/detail',$data);
		}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Pelatih Tidak di Temukan');
				redirect(site_url('pelatih'));
		}
	}
	
	public function updates(){
		$id_pelatih = $this->input->post('id_pelatih');
		$det = $this->Crud_model->get_by_id('e_pelatih', 'id_pelatih', $id_pelatih);
		if($det != false){
			$data = array(
				'nik' => $this->input->post('nik'),
				'nama_pelatih' => $this->input->post('nama_pelatih'),
				'jk' => $this->input->post('jk'),
				'alamat' => $this->input->post('alamat'),
				'tempat_lahir' => $this->input->post('tempat_lahir'), 
				'tanggal_lahir' => date('Y-m-d', strtotime($this->input->post('tanggal_lahir'))),
				'tinggi_badan' => $this->input->post('tinggi'),
				'berat_badan' => $this->input->post('berat'),
			);
			$insert = $this->Crud_model->update('e_pelatih', 'id_pelatih', $id_pelatih, $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Merubah');
				redirect(site_url('pelatih/detail/'.$id_pelatih));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Merubah');
				redirect(site_url('pelatih/detail/'.$id_pelatih));
			}
		}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'pelatih Tidak di Temukan');
				redirect(site_url('pelatih'));
		}
    }

}