<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kontingen extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
	{
        $data['alldata'] = $this->Crud_model->get_all('e_kontingen');
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kontingen/index',$data);
    }

    public function add()
	{
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kontingen/add');
    }

    public function adds(){
		//image
		ini_set('max_execution_time', 300);
		$ext = pathinfo($_FILES['icon']['name']);
		$filename = md5($this->input->post('icon')).'.'.$ext['extension'];
		$config['upload_path']   = './assets/images/kontingen/'; 
		$config['allowed_types'] = 'png';
        $config['max_size'] = '500';
        $config['file_name'] = $filename;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('icon') || empty($_FILES['icon']['name'])) {
			if (!empty($_FILES['icon']['name'])) {
				$upload = $this->upload->data(); 
				$savenamefile = 'assets/images/kontingen/' . $filename;
			}
			$data = array(
                'kode_kontingen' => $this->input->post('kode_kontingen'),
                'nama_kontingen' => $this->input->post('nama_kontingen'),
                'icon' => $savenamefile,
				'created_at' => date('Y-m-d H:i:s'),
			);
			$insert = $this->Crud_model->insert('e_kontingen', $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('kontingen'));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('kontingen'));
			}
		} else { 
			if (($_FILES['icon']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('kontingen'));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload icon');
				redirect(site_url('kontingen'));
			}
		
		}
    }
    
    public function hapus($id){
        $del = $this->Crud_model->is_delete('e_kontingen', 'id_kontingen', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('kontingen'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('kontingen'));
        }
    }

}