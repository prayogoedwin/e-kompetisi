<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class kelolauser extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
	{
        $data['alldata'] = $this->Crud_model->get_all('e_user');
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kelolauser/index',$data);
    }

    public function add()
	{
        $data['alldata'] = $this->Crud_model->get_all('e_kontingen');
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kelolauser/add',$data);
    }

    public function adds(){
        // $password = $this->input->post('username').'-@'.date('Y');
        $password = '12345';
        $options = [
			'cost' => 11
		];
        $hash_password = password_hash($password, PASSWORD_BCRYPT, $options);
			$data = array(
                'username_'     => $this->input->post('username'),
                'password_'     => $hash_password,
                'id_kontingen'  => $this->input->post('id_kontingen'),
                'akses'         => $this->input->post('akses'),
                'status'        => 'ON',
				'created_at'    => date('Y-m-d H:i:s'),
			);
			$insert = $this->Crud_model->insert('e_user', $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('kelolauser'));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('kelolauser'));
			}
    }
    
    public function hapus($id){
        $del = $this->Crud_model->is_delete('e_user', 'id_user', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('kelolauser'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('kelolauser'));
        }
    }

    public function rubah($id){
        $row = $this->Crud_model->get_by_id('e_user', 'id_user', $id);
        if($row->status == 'ON'){
            $data = array(
                    'status' => 'OFF'
            );
        }else{
            $data = array(
                'status' => 'ON'
            );
        }
        
        $up = $this->Crud_model->update('e_user', 'id_user', $id, $data);
        if($up != false){
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Rubah Status');
				redirect(site_url('kelolauser'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Rubah Status');
				redirect(site_url('kelolauser'));
        }
    }

    public function reset($id){
        // $row = $this->Crud_model->get_by_id('e_user', 'id_user', $id);
        // $password = $row->username.'-@'.date('Y');
        $password = '12345';
        $options = [
			'cost' => 11
        ];
        $hash_password = password_hash($password, PASSWORD_BCRYPT, $options);

        $data = array(
            'password_' => $hash_password,
        );
        $up = $this->Crud_model->update('e_user', 'id_user', $id, $data);
        if($up != false){
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Reset Password');
				redirect(site_url('kelolauser'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Reset Password');
				redirect(site_url('kelolauser'));
        }
    }

}