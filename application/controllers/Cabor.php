<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cabor extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
	{
        $data['alldata'] = $this->Crud_model->get_all('e_cabor');
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/cabor/index',$data);
    }

    public function add()
	{
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/cabor/add');
    }

    public function adds(){
		//image
		ini_set('max_execution_time', 300);
		$ext = pathinfo($_FILES['icon']['name']);
		$filename = md5($this->input->post('icon')).$ext['extension'];
		$config['upload_path']   = './assets/images/cabor/'; 
		$config['allowed_types'] = 'png';
        $config['max_size'] = '500';
        $config['file_name'] = $filename;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('icon') || empty($_FILES['icon']['name'])) {
			if (!empty($_FILES['icon']['name'])) {
				$upload = $this->upload->data(); 
				$savenamefile = 'assets/images/cabor/' . $filename;
			}
			$data = array(
                'nama_cabor' => $this->input->post('nama_cabor'),
				'icon' => $savenamefile,
				'created_at' => date('Y-m-d H:i:s'),
			);
			$insert = $this->Crud_model->insert('e_cabor', $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('cabor'));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('cabor'));
			}
		} else { 
			if (($_FILES['icon']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('cabor'));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload icon');
				redirect(site_url('cabor'));
			}
		
		}
    }
    
    public function hapus($id){
        $del = $this->Crud_model->is_delete('e_cabor', 'id_cabor', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('cabor'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('cabor'));
        }
    }


    // ini crud kelas
    public function kelas($id)
	{
        $data['alldata'] = $this->Crud_model->get_all_by('e_kelas','id_cabor', $id);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_cabor','id_cabor', $id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/cabor/kelas',$data);
    }

    public function add_kelas($id)
	{
        $data['rowdata'] = $this->Crud_model->get_by_id('e_cabor','id_cabor', $id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/cabor/add_kelas',$data);
    }

    public function adds_kelas(){
            $id = $this->input->post('id_cabor');
			$data = array(
                'jenis_kelas'=> $this->input->post('jenis_kelas'),
                'nama_kelas' => $this->input->post('nama_kelas'),
				'id_cabor'   => $id,
				'created_at' => date('Y-m-d H:i:s'),
			);
			$insert = $this->Crud_model->insert('e_kelas', $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('cabor/kelas/'.$id));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('cabor/kelas/'.$id));
			}
    }

    public function hapus_kelas($id){
        $row = $this->Crud_model->get_by_id('e_kelas', 'id_kelas', $id);
        $del = $this->Crud_model->is_delete('e_kelas', 'id_kelas', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('cabor/kelas/'.$row->id_cabor));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('cabor/kelas/'.$row->id_cabor));
        }
    }

}