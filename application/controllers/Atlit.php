<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Atlit extends CI_Controller
{
    function __construct()
    {
		parent::__construct();
		$this->akses = $this->session->userdata('akses');
		$this->kontingen = $this->session->userdata('kontingen');
    }

    public function index()
	{
		if($this->akses == '99'){
			$data['alldata'] = $this->Crud_model->get_all('e_atlit');
		}else{
			$data['alldata'] = $this->Crud_model->get_all_by('e_atlit', 'id_kontingen', $this->kontingen);
		}
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/atlit/index',$data);
	}
	
	public function atlit($id)
	{
		
		$data['alldata'] = $this->Crud_model->get_all_by('e_atlit', 'id_kontingen', $id);

        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/atlit/index',$data);
    }

    public function add()
	{
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/atlit/add');
    }

    public function adds(){
		//image
		ini_set('max_execution_time', 300);
		$ext = pathinfo($_FILES['foto']['name']);
		$filename = md5($this->input->post('foto').date('Y-m-d H:i:s')).'.'.$ext['extension'];
		$config['upload_path']   = './assets/images/atlit/'; 
		$config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '500';
        $config['file_name'] = $filename;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
			if (!empty($_FILES['foto']['name'])) {
				$upload = $this->upload->data(); 
				$savenamefile = 'assets/images/atlit/' . $filename;
			}
			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
			$config['cacheable']    = true; //boolean, the default is true
			$config['cachedir']     = './assets/'; //string, the default is application/cache/
			$config['errorlog']     = './assets/'; //string, the default is application/logs/
			$config['imagedir']     = './assets/qr/'; //direktori penyimpanan qr code
			$config['quality']      = true; //boolean, the default is true
			$config['size']         = '1024'; //interger, the default is 1024
			$config['black']        = array(224,255,255); // array, default is array(255,255,255)
			$config['white']        = array(70,130,180); // array, default is array(0,0,0)
			$this->ciqrcode->initialize($config);
			$image_name=md5($this->input->post('nik')).'-'.md5(date('Y-m-d H:i:s')).'.png'; //buat name dari qr code sesuai dengan nim
			// $params['data'] = 'http://localhost/e-kompetisi/publik/cek/'.$image_name; //data yang akan di jadikan QR CODE
			$params['data'] = 'http://simdisporapar.jatengprov.go.id/databasepora/e-kompetisi/publik/cek_atlit/'.$image_name; //data yang akan di jadikan QR CODE
			$params['level'] = 'H'; //H=High
			$params['size'] = 10;
			$params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
			$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
			$data = array(
                'nik' => $this->input->post('nik'),
				'nisn' => $this->input->post('nisn'),
				'sekolah' => $this->input->post('sekolah'),
				'nama_atlit' => $this->input->post('nama_atlit'),
				'jk' => $this->input->post('jk'),
				'alamat' => $this->input->post('alamat'),
				'tempat_lahir' => $this->input->post('tempat_lahir'), 
				'tanggal_lahir' => date('Y-m-d', strtotime($this->input->post('tanggal_lahir'))),
				'tinggi_badan' => $this->input->post('tinggi'),
				'berat_badan' => $this->input->post('berat'),
				'foto' => $savenamefile,
				'id_kontingen' => $this->kontingen,
				'created_at' => date('Y-m-d H:i:s'),
				'qr' => $image_name,
			);
			$insert = $this->Crud_model->insert('e_atlit', $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('atlit'));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('atlit'));
			}
		} else { 
			if (($_FILES['foto']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('atlit'));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload foto');
				redirect(site_url('atlit'));
			}
		
		}
    }
    
    public function hapus($id){
        $del = $this->Crud_model->is_delete('e_atlit', 'id_atlit', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('atlit'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('atlit'));
        }
	}
	
	public function detail($id_atlit)
	{
		$det = $this->Crud_model->get_by_id('e_atlit', 'id_atlit', $id_atlit);
		if($det != false){
				$data['detail'] = $det;
				$this->load->view('backend/template/head');
				$this->load->view('backend/template/header');
				$this->load->view('backend/template/sidebar');
				$this->load->view('backend/atlit/detail',$data);
		}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Atlit Tidak di Temukan');
				redirect(site_url('atlit'));
		}
	}
	
	public function adds_foto(){
		$id_atlit = $this->input->post('id_atlit');
		$det = $this->Crud_model->get_by_id('e_atlit', 'id_atlit', $id_atlit);
		if($det != false){
		//image
		ini_set('max_execution_time', 300);
		$ext = pathinfo($_FILES['foto']['name']);
		$date = date('Y-m-d H:i:s');
		$filename = md5($this->input->post('foto').$date).'.'.$ext['extension'];
		$config['upload_path']   = './assets/images/atlit/'; 
		$config['allowed_types'] = 'png|jpg|jpeg';
        $config['max_size'] = '500';
        $config['file_name'] = $filename;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
			if (!empty($_FILES['foto']['name'])) {
				$upload = $this->upload->data(); 
				$savenamefile = 'assets/images/atlit/' . $filename;
			}
			$data = array(
				'id_atlit' => $id_atlit,
				'foto' => $savenamefile
			);
			$insert = $this->Crud_model->update('e_atlit', 'id_atlit', $id_atlit, $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('atlit/detail/'.$id_atlit));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('atlit/detail/'.$id_atlit));
			}
		} else { 
			if (($_FILES['foto']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('atlit/detail/'.$id_atlit));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload foto');
				redirect(site_url('atlit/detail/'.$id_atlit));
			}
		
		}
		}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Atlit Tidak di Temukan');
				redirect(site_url('atlit'));
		}
	}
	
	public function adds_ijazah(){
		$id_atlit = $this->input->post('id_atlit');
		$det = $this->Crud_model->get_by_id('e_atlit', 'id_atlit', $id_atlit);
		if($det != false){
		//image
		ini_set('max_execution_time', 300);
		$ext = pathinfo($_FILES['foto']['name']);
		$date = date('Y-m-d H:i:s');
		$filename = md5($this->input->post('foto').$date).'.'.$ext['extension'];
		$config['upload_path']   = './assets/images/ijazah/'; 
		$config['allowed_types'] = 'pdf';
        $config['max_size'] = '1000';
        $config['file_name'] = $filename;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
			if (!empty($_FILES['foto']['name'])) {
				$upload = $this->upload->data(); 
				$savenamefile = 'assets/images/ijazah/' . $filename;
			}
			$data = array(
				'id_atlit' => $id_atlit,
				'ijazah' => $savenamefile
			);
			$insert = $this->Crud_model->update('e_atlit', 'id_atlit', $id_atlit, $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('atlit/detail/'.$id_atlit));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('atlit/detail/'.$id_atlit));
			}
		} else { 
			if (($_FILES['foto']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('atlit/detail/'.$id_atlit));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload foto');
				redirect(site_url('atlit/detail/'.$id_atlit));
			}
		
		}
		}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Atlit Tidak di Temukan');
				redirect(site_url('atlit'));
		}
	}

	public function adds_akte(){
		$id_atlit = $this->input->post('id_atlit');
		$det = $this->Crud_model->get_by_id('e_atlit', 'id_atlit', $id_atlit);
		if($det != false){
		//image
		ini_set('max_execution_time', 300);
		$ext = pathinfo($_FILES['foto']['name']);
		$date = date('Y-m-d H:i:s');
		$filename = md5($this->input->post('foto').$date).'.'.$ext['extension'];
		$config['upload_path']   = './assets/images/akte/'; 
		$config['allowed_types'] = 'pdf';
        $config['max_size'] = '1000';
        $config['file_name'] = $filename;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
			if (!empty($_FILES['foto']['name'])) {
				$upload = $this->upload->data(); 
				$savenamefile = 'assets/images/akte/' . $filename;
			}
			$data = array(
				'id_atlit' => $id_atlit,
				'akte' => $savenamefile
			);
			$insert = $this->Crud_model->update('e_atlit', 'id_atlit', $id_atlit, $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('atlit/detail/'.$id_atlit));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('atlit/detail/'.$id_atlit));
			}
		} else { 
			if (($_FILES['foto']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('atlit/detail/'.$id_atlit));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload foto');
				redirect(site_url('atlit/detail/'.$id_atlit));
			}
		
		}
		}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Atlit Tidak di Temukan');
				redirect(site_url('atlit'));
		}
	}

	public function adds_raport(){
		$id_atlit = $this->input->post('id_atlit');
		$det = $this->Crud_model->get_by_id('e_atlit', 'id_atlit', $id_atlit);
		if($det != false){
		//image
		ini_set('max_execution_time', 300);
		$ext = pathinfo($_FILES['foto']['name']);
		$date = date('Y-m-d H:i:s');
		$filename = md5($this->input->post('foto').$date).'.'.$ext['extension'];
		$config['upload_path']   = './assets/images/raport/'; 
		$config['allowed_types'] = 'pdf';
        $config['max_size'] = '1000';
        $config['file_name'] = $filename;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
			if (!empty($_FILES['foto']['name'])) {
				$upload = $this->upload->data(); 
				$savenamefile = 'assets/images/raport/' . $filename;
			}
			$data = array(
				'id_atlit' => $id_atlit,
				'raport' => $savenamefile
			);
			$insert = $this->Crud_model->update('e_atlit', 'id_atlit', $id_atlit, $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('atlit/detail/'.$id_atlit));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('atlit/detail/'.$id_atlit));
			}
		} else { 
			if (($_FILES['foto']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('atlit/detail/'.$id_atlit));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload foto');
				redirect(site_url('atlit/detail/'.$id_atlit));
			}
		
		}
		}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Atlit Tidak di Temukan');
				redirect(site_url('atlit'));
		}
	}
	
	public function updates(){
		$id_atlit = $this->input->post('id_atlit');
		$det = $this->Crud_model->get_by_id('e_atlit', 'id_atlit', $id_atlit);
		if($det != false){
			$data = array(
                'nik' => $this->input->post('nik'),
				'nisn' => $this->input->post('nisn'),
				'sekolah' => $this->input->post('sekolah'),
				'nama_atlit' => $this->input->post('nama_atlit'),
				'jk' => $this->input->post('jk'),
				'alamat' => $this->input->post('alamat'),
				'tempat_lahir' => $this->input->post('tempat_lahir'), 
				'tanggal_lahir' => date('Y-m-d', strtotime($this->input->post('tanggal_lahir'))),
				'tinggi_badan' => $this->input->post('tinggi'),
				'berat_badan' => $this->input->post('berat'),
			);
			$insert = $this->Crud_model->update('e_atlit', 'id_atlit', $id_atlit, $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Merubah');
				redirect(site_url('atlit/detail/'.$id_atlit));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Merubah');
				redirect(site_url('atlit/detail/'.$id_atlit));
			}
		}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Atlit Tidak di Temukan');
				redirect(site_url('atlit'));
		}
    }

}