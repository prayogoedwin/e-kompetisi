<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kompetisi_user_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_all($kontingen){
        $get =$this->db->query("SELECT DISTINCT(a.id_kompetisi), a.* FROM e_kompetisi a
        INNER JOIN e_kompetisi_kontingen b ON a.id_kompetisi = b.id_kompetisi
        WHERE a.deleted_at IS NULL
        AND a.status = 'ON'
        AND b.deleted_at IS NULL
        AND b.id_kontingen = '$kontingen'");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }

     function get_keikutsertaan($kompetisi, $kontingen){
        $get =$this->db->query("SELECT * FROM e_kompetisi_keikutsertaan a WHERE a.deleted_at IS NULL
        AND a.id_kompetisi = '$kompetisi' AND a.id_kontingen = '$kontingen';");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }     

     function get_keikutsertaan_cabor($kompetisi, $kontingen, $cabor){
      $get =$this->db->query("SELECT * FROM e_kompetisi_keikutsertaan a WHERE a.deleted_at IS NULL
      AND a.id_kompetisi = '$kompetisi' AND a.id_kontingen = '$kontingen'
      AND id_cabor = '$cabor';");
      if($get){
         return $get->row();
       }else{
          return FALSE;
       }
      }   

     function get_by_number($kompetisi, $kontingen){
        $get = $this->db->query("SELECT a.* FROM e_kompetisi_keikutsertaan a
        WHERE a.id_kompetisi = '$kompetisi'
        AND a.id_kontingen = '$kontingen' AND a.deleted_at IS NULL");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }  

     function get_kelas_by($kompetisi, $kontingen, $cabor){
      $get = $this->db->query("SELECT a.* FROM e_kompetisi_keikutsertaan a
      WHERE a.id_kompetisi = '$kompetisi'
      AND a.id_kontingen = '$kontingen' 
      AND a.id_cabor = '$cabor'
      AND a.deleted_at IS NULL");
      if($get){
         return $get->result();
       }else{
          return FALSE;
       }
   }  

   function get_by_name($kompetisi, $kontingen){
      $get = $this->db->query("SELECT a.* FROM e_kompetisi_by_number a
      WHERE a.id_kompetisi = '$kompetisi'
      AND a.id_kontingen = '$kontingen' AND a.deleted_at IS NULL
      ORDER by a.id_cabor");
      if($get){
         return $get->result();
       }else{
          return FALSE;
       }
   } 
   
   function hitung_atlit_by_keikutsertaan($id_kompetisi, $id_kontingen){
      return $this->db->query("SELECT COUNT(DISTINCT(id_atlit)) as atlit FROM e_kompetisi_by_name_atlit WHERE id_kompetisi = '$id_kompetisi' AND id_kontingen = '$id_kontingen' ")->row();
  }


   
        

}
?>