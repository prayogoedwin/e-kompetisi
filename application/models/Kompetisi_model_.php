<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kompetisi_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_all($kontingen){
        $get =$this->db->query("SELECT DISTINCT(a.id_kompetisi), a.* FROM e_kompetisi a
        INNER JOIN e_kompetisi_kontingen b ON a.id_kompetisi = b.id_kompetisi
        WHERE a.deleted_at IS NULL
        AND a.status = 'ON'
        AND b.deleted_at IS NULL
        AND b.id_kontingen = '$kontingen'");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }

     function get_keikutsertaan($kompetisi, $kontingen){
        $get =$this->db->query("SELECT * FROM e_kompetisi_keikutsertaan a WHERE a.deleted_at IS NULL
        AND a.id_kompetisi = '$kompetisi' AND a.id_kontingen = '$kontingen';");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }     

     function get_by_number($kompetisi){
        $get = $this->db->query("SELECT a.* FROM e_kompetisi_keikutsertaan a
        WHERE a.id_kompetisi = '$kompetisi'
        AND a.deleted_at IS NULL
        order by a.id_kontingen DESC");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }  

     function get_kelas_by($kompetisi, $kontingen, $cabor){
      $get = $this->db->query("SELECT a.* FROM e_kompetisi_keikutsertaan a
      WHERE a.id_kompetisi = '$kompetisi'
      AND a.id_kontingen = '$kontingen' 
      AND a.id_cabor = '$cabor'
      AND a.deleted_at IS NULL");
      if($get){
         return $get->result();
       }else{
          return FALSE;
       }
   } 
   
   function get_by_name($kompetisi){
      $get = $this->db->query("SELECT distinct(a.id_kontingen), a.id_kompetisi FROM e_kompetisi_keikutsertaan a
      WHERE a.id_kompetisi = '$kompetisi'
      AND a.deleted_at IS NULL
      order by a.id_kontingen DESC");
      if($get){
         return $get->result();
       }else{
          return FALSE;
       }
   }  

   function get_validasi_by($kompetisi, $kontingen, $cabor, $jk){
      $get = $this->db->query("SELECT a.*, b.nama_atlit FROM e_kompetisi_by_name_atlit a
      INNER JOIN e_atlit b ON a.id_atlit = b.id_atlit
      WHERE a.id_kompetisi = '$kompetisi'
      AND a.id_kontingen = '$kontingen' 
      AND a.id_cabor = '$cabor'
      AND b.jk = '$jk'
      AND a.deleted_at IS NULL");
      if($get){
         return $get->result();
       }else{
          return array();
       }
   }

   function get_atlit_by($kompetisi, $kontingen){
      $get = $this->db->query("SELECT a.id_atlit, a.id_kompetisi, b.*, b.nama_atlit as nama, a.id_cabor as cabor, a.id_kelas as kelas from e_kompetisi_by_name_atlit a
      INNER JOIN e_atlit b ON a.id_atlit = b.id_atlit
      WHERE a.id_kompetisi = $kompetisi
      AND a.id_kontingen LIKE '$kontingen'
      AND a.deleted_at IS NULL");
      if($get){
         return $get->result();
       }else{
          return array();
       }
   }

   function get_pelatih_by($kompetisi, $kontingen){
      $get = $this->db->query("SELECT a.id_pelatih, a.id_kompetisi, b.*, b.nama_pelatih as nama from e_kompetisi_by_name_pelatih a
      INNER JOIN e_pelatih b ON a.id_pelatih = b.id_pelatih
      WHERE a.id_kompetisi = $kompetisi
      AND a.id_kontingen LIKE '$kontingen'
      AND a.deleted_at IS NULL");
      if($get){
         return $get->result();
       }else{
          return array();
       }
   }


   
        

}
?>