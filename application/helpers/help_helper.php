<?php

function is_login(){
    $ci = get_instance();
    if($ci->session->userdata('id') == null){
        redirect('welcome/login');
    }else{
        return true;
    }
}

function nama_kota($kota){
    $ci = get_instance();
    $x = $ci->db->query("SELECT name FROM tbl_kota WHERE id = $kota")->row();
    return $x->name;
}

function nama_kec($kec){
    $ci = get_instance();
    $y = $ci->db->query("SELECT name FROM tbl_kecamatan WHERE id = $kec")->row();
    return $y->name;
}

function nama_kel($kel){
    $ci = get_instance();
    $z = $ci->db->query("SELECT name FROM tbl_desa WHERE id = $kel")->row();
    return $z->name;
}

function nama_kontingen($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT `nama_kontingen` as ktn FROM e_kontingen WHERE id_kontingen = '$id'")->row();
    if(!$z){
        echo 'AKUN ADMIN - JAWA TENGAH';
    }else{
        return $z->ktn;
    }
}

function nama_kompetisi($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT `nama_kompetisi` as ktn FROM e_kompetisi WHERE id_kompetisi  = '$id'")->row();
    if(!$z){
        echo '-';
    }else{
        return $z->ktn;
    }
}

function icon_kontingen($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT `icon` as ktn FROM e_kontingen WHERE id_kontingen = '$id'")->row();
    return $z->ktn;
}

function nama_cabor($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT `nama_cabor` as ktn FROM e_cabor WHERE id_cabor = $id")->row();
    return $z->ktn;
}

function nama_kelas($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT `nama_kelas` as ktn FROM e_kelas WHERE id_kelas = $id")->row();
    return $z->ktn;
}

function jenis_kelas($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT `jenis_kelas` as ktn FROM e_kelas WHERE id_kelas = $id")->row();
    return $z->ktn;
}


function nama_akses($id){
    if($id == '99'){
        $data = 'Admin';
    }elseif($id == '88'){
        $data = 'Kontingen';
    }else{
        $data = 'Tidak terdeteksi, hapus User ini untuk keamanan !';
    }

    return $data;
}

function nama_jenis($kd){
    if($kd == 'Pa'){
        $data = 'Putra';
    }elseif($kd == 'Pi'){
        $data = 'Putri';
    }else{
        $data = '-';
    }

    return $data;
}

function nama_tahap($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT `nama_tahap` as ktn FROM e_tahap WHERE id_tahap = $id")->row();
    return $z->ktn;
}

function nama_atlit($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT `nama_atlit` as ktn FROM e_atlit WHERE id_atlit = $id")->row();
    return $z->ktn;
}

function info_atlit($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT * FROM e_atlit WHERE id_atlit = $id")->row();
    return $z;
}

function nama_pelatih($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT `nama_pelatih` as ktn FROM e_pelatih WHERE id_pelatih = $id")->row();
    return $z->ktn;
}

function info_pelatih($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT * FROM e_pelatih WHERE id_pelatih = $id")->row();
    return $z;
}

function nama_medali($id){
    if($id == 1){
        echo 'Emas';
    }elseif($id == 2){
        echo 'Perak';
    }elseif($id == 3){
        echo 'Perunggu';
    }else{
        echo '-';
    }
}

function nama_juara($id){
    if($id == 1){
        echo 'Juara I';
    }elseif($id == 2){
        echo 'Juara II';
    }elseif($id == 3){
        echo 'Juara III';
    }else{
        echo '-';
    }
}

function select_kompetisi($id){
    $ci = get_instance();
    $z = $ci->db->query("SELECT * FROM e_kompetisi WHERE id_kompetisi = $id")->row();
    return $z;
}

function get_tombol_late($id_kompetisi, $id_kontingen){
    $ci = get_instance();
    $z = $ci->db->query("SELECT * FROM e_kompetisi_kontingen_late WHERE id_kompetisi = '$id_kompetisi' AND id_kontingen = '$id_kontingen' AND deleted_at IS NULL")->result();
    return $z;
}

function get_tombol_late_keikutsertaan($id_kompetisi, $id_kontingen){
    $ci = get_instance();
    $z = $ci->db->query("SELECT count(id_tahap) as cnt FROM e_kompetisi_kontingen_late WHERE id_kompetisi = '$id_kompetisi' AND id_kontingen = '$id_kontingen' AND id_tahap = 1 AND deleted_at IS NULL")->row();
    return $z->cnt;
}

function get_tombol_late_by_number($id_kompetisi, $id_kontingen){
    $ci = get_instance();
    $z = $ci->db->query("SELECT count(id_tahap) as cnt FROM e_kompetisi_kontingen_late WHERE id_kompetisi = '$id_kompetisi' AND id_kontingen = '$id_kontingen' AND id_tahap = 2 AND deleted_at IS NULL")->row();
    return $z->cnt;
}

function get_tombol_late_by_name($id_kompetisi, $id_kontingen){
    $ci = get_instance();
    $z = $ci->db->query("SELECT count(id_tahap) as cnt FROM e_kompetisi_kontingen_late WHERE id_kompetisi = '$id_kompetisi' AND id_kontingen = '$id_kontingen' AND id_tahap = 3 AND deleted_at IS NULL")->row();
    return $z->cnt;
}




?>
