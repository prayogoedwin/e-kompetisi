<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crud_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_all($tabel){
        
        //$this->db->where('deleted_at', ' ');
        //$this->db->where(array('deleted_at' => NULL));
        // = $this->db->get($tabel);
        $get =$this->db->query("SELECT * FROM $tabel WHERE deleted_at IS NULL");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }

     function get_all_order_by($tabel, $order_by, $order){
        
        //$this->db->where('deleted_at', ' ');
        //$this->db->where(array('deleted_at' => NULL));
        // = $this->db->get($tabel);
        $get =$this->db->query("SELECT * FROM $tabel WHERE deleted_at IS NULL Order by $order_by $order ");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }

     function get_all_where_order_by($tabel, $where, $is, $order_by, $order){
        
        //$this->db->where('deleted_at', ' ');
        //$this->db->where(array('deleted_at' => NULL));
        // = $this->db->get($tabel);
        $get =$this->db->query("SELECT * FROM $tabel WHERE deleted_at IS NULL AND $where = '$is' Order by $order_by $order ");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }

     function get_all_by($tabel, $where, $id){
        
        //$this->db->where('deleted_at', ' ');
        //$this->db->where(array('deleted_at' => NULL));
        // = $this->db->get($tabel);
        $get =$this->db->query("SELECT * FROM $tabel WHERE $where = '$id' AND deleted_at IS NULL");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }

     function get_all_by_limit($tabel, $where, $id){
        
        //$this->db->where('deleted_at', ' ');
        //$this->db->where(array('deleted_at' => NULL));
        // = $this->db->get($tabel);
        $get =$this->db->query("SELECT * FROM $tabel WHERE $where = '$id' AND deleted_at IS NULL LIMIT 4");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }

     function get_all_by_2_where($tabel, $where, $id, $where1, $is){
        
        //$this->db->where('deleted_at', ' ');
        //$this->db->where(array('deleted_at' => NULL));
        // = $this->db->get($tabel);
        $get =$this->db->query("SELECT * FROM $tabel WHERE $where = '$id' AND $where1 = '$is' AND deleted_at IS NULL");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }


     function get_all_by_3_where($tabel, $where, $id, $where1, $is, $where2, $is2){
        
        //$this->db->where('deleted_at', ' ');
        //$this->db->where(array('deleted_at' => NULL));
        // = $this->db->get($tabel);
        $get =$this->db->query("SELECT * FROM $tabel WHERE $where = '$id' AND $where1 = '$is' AND $where2 = '$is2' AND deleted_at IS NULL");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }

     function get_all_by_4_where($tabel, $where, $id, $where1, $is1, $where2, $is2, $where3, $is3){
        
        //$this->db->where('deleted_at', ' ');
        //$this->db->where(array('deleted_at' => NULL));
        // = $this->db->get($tabel);
        $get =$this->db->query("SELECT * FROM $tabel WHERE $where = '$id' AND $where1 = '$is1' AND $where2 = '$is2' AND $where3 = '$is3' AND deleted_at IS NULL");
        if($get){
           return $get->result();
         }else{
            return FALSE;
         }
     }


     function get_by_id($tabel, $where, $id){
        $this->db->where($where, $id);
        $get = $this->db->get($tabel);
        if($get->num_rows() > 0){
            return $get->row();
         }else{
             return FALSE;
         }
     }

    // insert data
    function insert($table, $data)
    {
        $add = $this->db->insert($table, $data);
        if($add){
            return true;
        }else{
            return false;
        }
    }

    // insert batch
    function insert_batch($table, $data)
    {
        $add = $this->db->insert_batch($table, $data);
        if($add){
            return true;
        }else{
            return false;
        }
    }

    // update data
    function update($table, $where_id, $id, $data)
    {
        $this->db->where($where_id, $id);
        $edit = $this->db->update($table, $data);
        if($edit){
            return true;
        }else{
            return false;
        }
    }

    // hidden data from aplikasi
    function is_delete($table, $where_id, $id)
    {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
        );
        $this->db->where($where_id, $id);
        $del = $this->db->update($table, $data);
        if($del){
            return true;
        }else{
            return false;
        }
    }

    // delete data
    function delete($table, $where_id, $id)
    {
        $this->db->where($where_id, $id);
        $this->db->delete($table);
    }

    


}

/* End of file Tbl_banner_model.php */
/* Location: ./application/models/Tbl_banner_model.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-21 07:00:03 */
/* http://harviacode.com */
