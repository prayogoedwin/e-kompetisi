<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{

    public function __construct(){
		parent::__construct();

    }

    public function cek_login($username, $password){
		$query = $this->db->query(
			"SELECT * FROM `e_user` WHERE `username_` = '$username' AND `status` = 'ON' AND `deleted_at` IS NULL");
		if ($query->num_rows() > 0) {
			$user = $query->row();
			if(password_verify($password,$user->password_)) {
				$data = $user;
			} else {
				$data = FALSE;
			}
		} else {
			$data = FALSE;
		}
		return $data;
	}

	public function update_lastlogin($id){
		$this->db->where('id_user', $id);
		$now = date('Y-m-d H:i:s');
		$data = array(
				'last_login' => $now
			);
		$this->db->update('e_user', $data);
	  }
	  

	  function get_by_id($tabel, $where, $id){
        $this->db->where($where, $id);
        $get = $this->db->get($tabel);
        if($get->num_rows() > 0){
            return $get->row_array();
         }else{
             return FALSE;
         }
	 }
	 
	 public function getListTanggalBeritaFO()
	{
		$sql_tanggal = "SELECT DISTINCT MONTHNAME(created_at) as bulan, YEAR(created_at) as tahun FROM e_informasi WHERE status = 'ON' AND deleted_at IS NULL order by created_at DESC";
		$query = $this->db->query($sql_tanggal)->result();
		return $query;
	}
	public function jumlahTanggalBerita($bulan,$tahun)
	{
		$this->db->where('MONTHNAME(created_at)', $bulan);
		$this->db->where('YEAR(created_at)', $tahun);
		$this->db->where('status','ON');
		$this->db->where('deleted_at',NULL);
		$query=$this->db->get('e_informasi');
		return $query;
	}
}
