<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <!-- <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li>
        </ul>
    </div> -->
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

        <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title "> Daftar Kompetisi</span>
                    </div>
                    

                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <table border="0" style="width:100%" id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center">No</th>
                                    <th class="text-center">Nama kompetisi</th>
                                    <th class="text-center">Tahap</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if($alldata == FALSE ){ ?>
                                    <tr class="bg-light">                                    
                                    <th class="text-center" colspan="5">Data Kosong</th>
                                    
                                </tr>
                                <?php }else{ 
                                $no = 0;
                                foreach($alldata as $all): 
                                $no++
                                ?>
                                <tr>
                                    <td class=""><?=$no?></td>
                                    <td class=""><?=$all->nama_kompetisi?>
                                    <?php $lates = get_tombol_late($all->id_kompetisi, $kontingenz);

                                    foreach ($lates as $late) : ?>
                                       
                                       <?php if($late->id_tahap == 1){ ?>
                                       <a href="<?=base_url('kompetisi_user/keikutsertaan/'.$all->id_kompetisi)?>"><button type="button"
                                                    class="btn btn-success br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     <span class="fa fa-file-o pr5">&nbsp; Input Terlambat Keikutsertaan
                                                    </button>
                                        </a>
                                       <?php }elseif($late->id_tahap == 2){ ?>

                                        <a href="<?=base_url('kompetisi_user/by_number/'.$all->id_kompetisi)?>"><button type="button"
                                                    class="btn btn-warning br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     <span class="fa fa-file-o pr5">&nbsp; Input Terlambat By Number
                                                    </button>
                                        </a>

                                        <?php }elseif($late->id_tahap == 3){ ?>

                                            <a href="<?=base_url('kompetisi_user/by_name/'.$all->id_kompetisi)?>"><button type="button"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     <span class="fa fa-file-o pr5">&nbsp; Input Terlambat By Name
                                                    </button>
                                        </a>


                                        <?php }else{ echo '' ;} ?>



                                   <?php  endforeach ?>
                                    
                                    </td>
                                    <td class="">
                                    <?=nama_tahap($all->tahap)?>
                                    <?php if($all->tahap == 1){ ?>

                                        <a href="<?=base_url('kompetisi_user/keikutsertaan/'.$all->id_kompetisi)?>"><button type="button"
                                                    class="btn btn-success br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     <span class="fa fa-file-o pr5">&nbsp; Keikutsertaan
                                                    </button>
                                        </a>


                                    <?php }elseif($all->tahap == 2){ ?>

                                        <a href="<?=base_url('kompetisi_user/by_number/'.$all->id_kompetisi)?>"><button type="button"
                                                    class="btn btn-warning br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     <span class="fa fa-file-o pr5">&nbsp; By Number
                                                    </button>
                                        </a>

                                    
                                    <?php }elseif($all->tahap == 3){ ?>

                                        <a href="<?=base_url('kompetisi_user/by_name/'.$all->id_kompetisi)?>"><button type="button"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     <span class="fa fa-file-o pr5">&nbsp; By Name
                                                    </button>
                                        </a>

                                    <?php } ?>
                                    </td>
                                    
                                    
                                    
                                </tr>
                                <?php endforeach;} ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>
<?php include(__DIR__ . "/../template/datatable.php"); ?>
<script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "info": false,   
      "bLengthChange": false,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "autoWidth": true,
      "scrollX": true,
      "autoWidth": true
    });
});
</script>



</body>
</html>


