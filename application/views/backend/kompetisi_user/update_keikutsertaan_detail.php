<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <!-- <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li>
        </ul>
    </div> -->
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<?php echo form_open('kompetisi_user/edit_keikutsertaan_detail') ?>
<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

        <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title "> Detail Keikutsertaan Pada Kompetisi <?=$rowdata2->nama_kompetisi?></span>
                    </div>
                    
                    
                    <?php echo form_open('kompetisi_user/adds_keikutsertaan_detail') ?>
                    <input type="hidden" name="id_kompetisi" value="<?=$rowdata2->id_kompetisi?>">
                    <input type="hidden" name="id_kontingen" value="<?=$this->session->userdata('kontingen')?>">
                    <input type="hidden" name="id_kompetisi_keikutsertaan" value="<?=$this->uri->segment('3')?>">
                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <table border="0" style="width:100%"  class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center">Cabor</th>
                                    <th class="text-center">Atlit Pa</th>
                                    <th class="text-center">Atlit Pi</th>
                                    <th class="text-center">Pelatih Pa</th>
                                    <th class="text-center">Pelatih Pi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class=""><?=nama_cabor($rowdata->id_cabor)?></td>
                                
                                    <td class=""><?=$rowdata->atlit_pa?></td>
                                    <td class=""><?=$rowdata->atlit_pi?></td>
                                    <td class=""><?=$rowdata->pelatih_pa?></td>
                                    <td class=""><?=$rowdata->pelatih_pi?></td>
                                </tr>
                                

                                </tbody>
                            </table>

                            <hr/>


                            <div class="row">
                            <div class="col-xs-6">
                            <table border="0" style="width:100%"  class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center" width="10">Pilih Atlit Putra</th>
                                </tr>
                                </thead>

                                
                                <tbody>
                                <?php
                               
                                foreach($atlit_pa_ as $apa_):
                                
                                ?>
                                <tr>
                                    <td class="">
                                    <input type="hidden" name="id_pa[]" value="<?=$apa_->id_kompetisi_keikutsertaan_detail?>">
                                    <input type="hidden" name="cabor_pa[]" value="<?=$apa_->id_cabor?>">
                                    <select id="atlit_pa"  style="width:100%" name="atlit_pa[]">
                                        <option  value="<?=$apa_->id_atlit?>"><?=nama_atlit($apa_->id_atlit)?></option>
                                        <?php foreach($atlit_pa AS $apa):  ?>
                                        <option value="<?=$apa->id_atlit?>"><?=$apa->nama_atlit?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    
                                    </td>
                                </tr>
                                <?php 
                                endforeach;
                                 ?>

                                

                                </tbody>
                            </table>
                            </div>
                            

                            <div class="col-md-6">
                            <table border="0" style="width:100%"  class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center" width="10">Pilih Atlit Putri</th>
                                </tr>
                                </thead>

                                
                                <tbody>
                                <?php
                                foreach($atlit_pi_ as $api_){
                                
                                ?>
                                <tr>
                                    <td class="">
                                    <input type="hidden" name="id_pi[]" value="<?=$api_->id_kompetisi_keikutsertaan_detail?>">
                                    <input type="hidden" name="cabor_pa[]" value="<?=$api_->id_cabor?>">
                                    <select id="atlit_pi"  style="width:100%" name="atlit_pi[]">
                                        <option  value="<?=$api_->id_atlit?>"><?=nama_atlit($api_->id_atlit)?></option>
                                        <?php foreach($atlit_pi AS $api): ?>
                                        <option value="<?=$api->id_atlit?>"><?=$api->nama_atlit?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    
                                    </td>
                                </tr>
                                <?php } ?>


                                

                                </tbody>
                            </table>
                            </div>

                         </div>

                         <hr/>
                            

                            
                        <div class="row">
                            <div class="col-xs-6">
                            <table border="0" style="width:100%"  class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center" width="10">Pilih Pelatih Putra</th>
                                </tr>
                                </thead>

                                
                                <tbody>
                                <?php
                                foreach($pelatih_pa_ as $ppa_){
                                
                                ?>
                                <tr>
                                    <td class="">
                                    <input type="hidden" name="id_ppa[]" value="<?=$ppa_->id_kompetisi_keikutsertaan_detail?>">
                                    <input type="hidden" name="cabor_ppa[]" value="<?=$ppa_->id_cabor?>">
                                    <select id="pelatih_pa"  style="width:100%" name="pelatih_pa[]">
                                        <option  value="<?=$ppa_->id_pelatih?>"><?=nama_pelatih($ppa_->id_pelatih)?></option>
                                        <?php foreach($pelatih_pa AS $ppa):  ?>
                                        <option value="<?=$ppa->id_pelatih?>"><?=$ppa->nama_pelatih?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    
                                    </td>
                                </tr>
                                <?php } ?>

                                

                                </tbody>
                            </table>
                            </DIV>

                            <div class="col-xs-6">
                            <table border="0" style="width:100%"  class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center" width="10">Pilih Pelatih Putri</th>
                                </tr>
                                </thead>

                                
                                <tbody>
                                <?php
                                foreach($pelatih_pi_ as $ppi_){
                                
                                ?>
                                <tr>
                                    <td class="">
                                    <input type="hidden" name="id_ppi[]" value="<?=$ppi_->id_kompetisi_keikutsertaan_detail?>">
                                    <input type="hidden" name="cabor_ppi[]" value="<?=$ppi_->id_cabor?>">
                                    <select id="pelatih_pi"  style="width:100%" name="pelatih_pi[]">
                                        <option  value="<?=$ppi_->id_pelatih?>"><?=nama_pelatih($ppi_->id_pelatih)?></option>
                                        <?php foreach($pelatih_pi AS $ppi):  ?>
                                        <option value="<?=$ppi->id_pelatih?>"><?=$ppi->nama_pelatih?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    
                                    </td>
                                </tr>
                                <?php } ?>
                                

                                </tbody>
                            </table>
                            </DIV>

                        </DIV>
                        
                        <br/>
                        <div class="section">
                                            
                        <div class="pull-right">
                            <button type="submit" class="btn btn-bordered btn-primary">
                                Update Data
                            </button>
                        </div>
                    </div>
                    <!-- -------------- /section -------------- -->



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close();?>

      

</section>
<!-- -------------- /Content -------------- -->
<?php echo form_close();?>

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>
<?php include(__DIR__ . "/../template/datatable.php"); ?>
<script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "info": false,   
      "bLengthChange": false,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "autoWidth": true,
      "scrollX": true,
      "autoWidth": true
    });
});
</script>



</body>
</html>


