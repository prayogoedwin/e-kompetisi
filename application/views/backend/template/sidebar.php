 <!-- -------------- Sidebar  -------------- -->
 <aside id="sidebar_left" class="nano nano-light affix">

<!-- -------------- Sidebar Left Wrapper  -------------- -->
<div class="sidebar-left-content nano-content">

    <!-- -------------- Sidebar Header -------------- -->
    <header class="sidebar-header">

        <!-- -------------- Sidebar - Author -------------- -->
        <div class="sidebar-widget author-widget">
            <div class="media">
            

                <a class="media-left" href="<?=base_url('dashboard')?>">
                    <img src="<?=base_url().icon_kontingen($this->session->userdata('id_kontingen'))?>" class="img-responsive">
                </a>
                <div class="media-body">
                    <div class="media-author"><?=nama_kontingen($this->session->userdata('id_kontingen'))?></div>
                    <div class="media-author"><?=nama_akses($this->session->userdata('akses'))?></div>
                </div>

          
            </div>
        </div>

        <!-- -------------- Sidebar - Author Menu  -------------- -->
        <div class="sidebar-widget menu-widget">
            <div class="row text-center mbn">
                <div class="col-xs-2 pln prn">
                    <a href="dashboard1.html" class="text-primary" data-toggle="tooltip" data-placement="top"
                       title="Dashboard">
                        <span class="fa fa-dashboard"></span>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-2 pln prn">
                    <a href="charts-highcharts.html" class="text-info" data-toggle="tooltip"
                       data-placement="top"
                       title="Stats">
                        <span class="fa fa-bar-chart-o"></span>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-2 pln prn">
                    <a href="sales-stats-products.html" class="text-system" data-toggle="tooltip"
                       data-placement="top" title="Orders">
                        <span class="fa fa-info-circle"></span>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-2 pln prn">
                    <a href="sales-stats-purchases.html" class="text-warning" data-toggle="tooltip"
                       data-placement="top" title="Invoices">
                        <span class="fa fa-file"></span>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-2 pln prn">
                    <a href="basic-profile.html" class="text-alert" data-toggle="tooltip" data-placement="top"
                       title="Users">
                        <span class="fa fa-users"></span>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-2 pln prn">
                    <a href="management-tools-dock.html" class="text-danger" data-toggle="tooltip"
                       data-placement="top" title="Settings">
                        <span class="fa fa-cogs"></span>
                    </a>
                </div>

                <div class="col-xs-4 col-sm-2 pln prn">
                    <a href="management-tools-dock.html" class="text-danger" data-toggle="tooltip"
                       data-placement="top" title="Settings">
                        <span class="fa fa-cogs"></span>
                    </a>
                </div>
            </div>
        </div>

    </header>
    <!-- -------------- /Sidebar Header -------------- -->

    
    <!-- -------------- Sidebar Menu  -------------- -->
    <ul class="nav sidebar-menu">
        <li class="sidebar-label pt30">Menu</li>

        <li>
            <a class="menu-open" href="<?=base_url('dashboard')?>">
                <span class="fa fa-dashboard"></span>
                <span class="sidebar-title">Dashboard</span>
                <span class="caret"></span>
            </a>
            
        </li>      

        <?php if($this->session->userdata('akses') == '99') {?>
        

        <li>
            <a class="menu-open" href="<?=base_url('kelolauser')?>">
                <span class="fa fa-users"></span>
                <span class="sidebar-title">User</span>
                <span class="caret"></span>
            </a>
            
        </li>

        <li>
            <a class="accordion-toggle" href="<?=base_url('kompetisi')?>">
                <span class="fa fa-gears"></span>
                <span class="sidebar-title">Setting Web</span>
                <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
                <li>
                    <a href="<?=base_url('banner')?>">
                        <span class="glyphicon glyphicon-tags"></span> Banner </a>
                </li>
                <li>
                    <a href="<?=base_url('informasi')?>">
                        <span class="glyphicon glyphicon-tags"></span> Berita & Pengumuman </a>
                </li>   
                <li>
                    <a href="<?=base_url('foto')?>">
                        <span class="glyphicon glyphicon-tags"></span> Galeri </a>
                </li>    

               
            </ul>
        </li>

        <li>
            <a class="menu-open" href="<?=base_url('cabor')?>">
                <span class="fa fa-random"></span>
                <span class="sidebar-title">Cabor</span>
                <span class="caret"></span>
            </a>
        </li>

        <li>
            <a class="menu-open" href="<?=base_url('kontingen')?>">
                <span class="fa fa-calendar-o"></span>
                <span class="sidebar-title">Kontingen</span>
                <span class="caret"></span>
            </a> 
        </li>

        <li>
            <a class="menu-open" href="<?=base_url('kompetisi')?>">
                <span class="fa fa-share-square-o"></span>
                <span class="sidebar-title">Kompetisi</span>
                <span class="caret"></span>
            </a> 
        </li>

        

        <?php }else{ ?>

        <li>
            <a class="menu-open" href="<?=base_url('atlit')?>">
                <span class="fa fa-diamond"></span>
                <span class="sidebar-title">Atlit</span>
                <span class="caret"></span>
            </a> 
        </li>


        <li>
            <a class="menu-open" href="<?=base_url('pelatih')?>">
                <span class="fa fa-user-plus"></span>
                <span class="sidebar-title">Pelatih</span>
                <span class="caret"></span>
            </a> 
        </li>

        <li>
            <a class="menu-open" href="<?=base_url('kompetisi_user')?>">
                <span class="fa fa-share-square-o"></span>
                <span class="sidebar-title">Kompetisi</span>
                <span class="caret"></span>
            </a> 
        </li>

        <?php } ?>

       

        


    
</div>
<!-- -------------- /Sidebar Left Wrapper  -------------- -->

</aside>