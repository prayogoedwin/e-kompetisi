 <!-- -------------- Page Footer -------------- -->
<footer id="content-footer" class="affix">
<div class="row">
    <div class="col-md-6">
        <span class="footer-legal">© 2019 Disporapar Provinsi Jawa Tengah. </span>
    </div>
    <div class="col-md-6 text-right">
        <span class="footer-legal">Supported by <a href="#">Ezra Pratama.</a></span>
    </div>
   
</div>
</footer>
<!-- -------------- /Page Footer -------------- -->


 
 <!-- -------------- Sidebar Right -------------- -->
 <aside id="sidebar_right" class="nano affix">

<!-- -------------- Sidebar Right Content -------------- -->
<div class="sidebar-right-wrapper nano-content">

    <div class="sidebar-block br-n p15">

        <h6 class="title-divider text-muted mb20"> Visitors Stats
        <span class="pull-right"> 2015
          <i class="fa fa-caret-down ml5"></i>
        </span>
        </h6>

        <div class="progress mh5">
            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="34"
                 aria-valuemin="0"
                 aria-valuemax="100" style="width: 34%">
                <span class="fs11">New visitors</span>
            </div>
        </div>
        <div class="progress mh5">
            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="66"
                 aria-valuemin="0"
                 aria-valuemax="100" style="width: 66%">
                <span class="fs11 text-left">Returnig visitors</span>
            </div>
        </div>
        <div class="progress mh5">
            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="45"
                 aria-valuemin="0"
                 aria-valuemax="100" style="width: 45%">
                <span class="fs11 text-left">Orders</span>
            </div>
        </div>

        <h6 class="title-divider text-muted mt30 mb10">New visitors</h6>

        <div class="row">
            <div class="col-xs-5">
                <h3 class="text-primary mn pl5">350</h3>
            </div>
            <div class="col-xs-7 text-right">
                <h3 class="text-warning mn">
                    <i class="fa fa-caret-down"></i> 15.7% </h3>
            </div>
        </div>

        <h6 class="title-divider text-muted mt25 mb10">Returnig visitors</h6>

        <div class="row">
            <div class="col-xs-5">
                <h3 class="text-primary mn pl5">660</h3>
            </div>
            <div class="col-xs-7 text-right">
                <h3 class="text-success-dark mn">
                    <i class="fa fa-caret-up"></i> 20.2% </h3>
            </div>
        </div>

        <h6 class="title-divider text-muted mt25 mb10">Orders</h6>

        <div class="row">
            <div class="col-xs-5">
                <h3 class="text-primary mn pl5">153</h3>
            </div>
            <div class="col-xs-7 text-right">
                <h3 class="text-success mn">
                    <i class="fa fa-caret-up"></i> 5.3% </h3>
            </div>
        </div>

        <h6 class="title-divider text-muted mt40 mb20"> Site Statistics
            <span class="pull-right text-primary fw600">Today</span>
        </h6>
    </div>
</div>
</aside>
<!-- -------------- /Sidebar Right -------------- -->

</div>
<!-- -------------- /Body Wrap  -------------- -->

<!-- -------------- Scripts -------------- -->

<!-- -------------- jQuery -------------- -->
<script src="<?=base_url()?>assets/all/assets/js/jquery/jquery-1.11.3.min.js"></script>
<script src="<?=base_url()?>assets/all/assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- -------------- JvectorMap Plugin -------------- -->
<script src="<?=base_url()?>assets/all/assets/js/plugins/jvectormap/jquery.jvectormap.min.js"></script>
<script src="<?=base_url()?>assets/all/assets/js/plugins/jvectormap/assets/jquery-jvectormap-world-mill-en.js"></script>

<!-- -------------- HighCharts Plugin -------------- -->
<script src="<?=base_url()?>assets/all/assets/js/plugins/highcharts/highcharts.js"></script>
<script src="<?=base_url()?>assets/all/assets/js/plugins/c3charts/d3.min.js"></script>
<script src="<?=base_url()?>assets/all/assets/js/plugins/c3charts/c3.min.js"></script>

<!-- -------------- Theme Scripts -------------- -->
<script src="<?=base_url()?>assets/all/assets/js/utility/utility.js"></script>
<script src="<?=base_url()?>assets/all/assets/js/demo/demo.js"></script>
<script src="<?=base_url()?>assets/all/assets/js/main.js"></script>
<script src="<?=base_url()?>assets/all/assets/js/demo/widgets_sidebar.js"></script>
<script src="<?=base_url()?>assets/all/assets/js/pages/dashboard2.js"></script>

<!-- -------------- Page JS -------------- -->
<script src="<?=base_url()?>assets/all/assets/js/demo/charts/highcharts.js"></script>

<!-- -------------- /Scripts -------------- -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<?php
$info= $this->session->flashdata('info');
$pesan= $this->session->flashdata('message');

if($info == 'success'){ ?>
  <script>
      swal({
          title: "Berhasil",
          text: "<?php echo $pesan; ?>",
          timer: 1500,
          showConfirmButton: false,
          type: 'success'
      });
  </script>
  <?php    }elseif($info == 'danger'){ ?>
  <script>
      swal({
          title: "Gagal",
          text: "<?php echo $pesan; ?>",
          timer: 1500,
          showConfirmButton: false,
          type: 'danger'
      });
  </script>
  <?php  }else{ } ?>