 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <!-- <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li> -->
        </ul>
    </div>
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

  <!-- -------------- Column Center -------------- -->
  <div class="chute chute-center">

                       
                            <!-- -------------- Registration -------------- -->
                            <div class="allcp-form theme-primary tab-pane mw600" id="register" role="tabpanel">
                            <div class="panel">
                                <div class="panel-heading">
                                    <span class="panel-title">
                                      Form Tambah Cabor
                                    </span>
                                </div>
                                <!-- -------------- /Panel Heading -------------- -->

                                 <?php echo form_open_multipart('cabor/adds') ?>
                                    <div class="panel-body pn">
                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="nama" name="nama_cabor" id="nama_cabor" class="gui-input"
                                                       placeholder="Nama Cabor">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-bicycle"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                            <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="file" name="icon" id="icon" class="gui-input"
                                                       placeholder="Icon Cabor">
                                                <label for="icon" class="field-icon">
                                                    <i class="fa fa-file-image-o"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                            
                                        <div class="section">
                                            
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-bordered btn-primary">
                                                    Tambah Cabor
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                    </div>
                                    <!-- -------------- /Form -------------- -->
                                    <div class="panel-footer"></div>
                                <?php echo form_close();?>
                            </div>
                            <!-- -------------- /Panel -------------- -->
                        </div>
                        <!-- -------------- /Registration -------------- -->

                        

</div>
<!-- -------------- //End Column Center -------------- -->

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>



</body>
</html>


