 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <!-- <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li> -->
        </ul>
    </div>
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

  <!-- -------------- Column Center -------------- -->
  <div class="chute chute-center">
  

                       
                            <!-- -------------- Registration -------------- -->
                            <div class="allcp-form theme-primary tab-pane mw600" id="register" role="tabpanel">
                            <div class="panel">
                                <div class="panel-heading">
                                    <span class="panel-title">
                                      Form Tambah Kontingen Terlambat Input Pada Kompetisi <?=ucfirst($rowdata->nama_kompetisi)?>
                                    </span>
                                </div>
                                <!-- -------------- /Panel Heading -------------- -->

                                 <?php echo form_open_multipart('kompetisi/adds_kontingen_late') ?>
                                 <input type="hidden" name="id_kompetisi" value="<?=$rowdata->id_kompetisi?>">
                                    <div class="panel-body pn">
                                    <div class="section">
                                            <label class="field select prepend-icon">
                                                <select id="id_kontingen"  name="id_kontingen">
                                                <option disabled selected>Pilih Kontingen</option>
                                                <?php foreach($select1 as $kon): ?>
                                                    
                                                    <option value="<?=$kon->id_kontingen?>"><?=nama_kontingen($kon->id_kontingen)?></option>

                                                <?php endforeach; ?>    
                                                </select>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label class="field select prepend-icon">
                                                <select id="id_tahap"  name="id_tahap">
                                                <option disabled selected>Pilih Tahap</option>
                                                <?php foreach($select2 as $thp): ?>     
                                                    <option value="<?=$thp->id_tahap?>"><?=$thp->nama_tahap?></option>
                                                <?php endforeach; ?>  
                                                </select>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                            
                                        <div class="section">
                                            
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-bordered btn-primary">
                                                    Tambah Kontingen Terlambat Input
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                    </div>
                                    <!-- -------------- /Form -------------- -->
                                    <div class="panel-footer"></div>
                                <?php echo form_close();?>
                            </div>
                            <!-- -------------- /Panel -------------- -->
                        </div>
                        <!-- -------------- /Registration -------------- -->

                        

</div>
<!-- -------------- //End Column Center -------------- -->

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>



</body>
</html>


