<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <!-- <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li>
        </ul>
    </div> -->
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

                <!-- -------------- Registration -------------- -->
                <div class="allcp-form theme-primary tab-pane mw600" id="register" role="tabpanel">
                            <div class="panel">
                                <div class="panel-heading">
                                    <span class="panel-title">
                                      Verifikasi & Validasi Peserta <?=$rowdata->nama_kompetisi?>
                                    </span>
                                </div>
                                <!-- -------------- /Panel Heading -------------- -->

                                 <?php echo form_open_multipart('kompetisi/validasi_/'.$rowdata->id_kompetisi) ?>
                                    <div class="panel-body pn" >

                                        <div class="section">
                                            
                                            <label class="field select prepend-icon">
                                            
                                          
                                                <select id="kontingen" name="kontingen">
                                                    <?php if($k == NULL){ ?>
                                                        <option disabled selected>Kontingen</option>
                                                    <?php }else{ ?>
                                                        <option selected value="<?=$k?>"><?=nama_kontingen($k)?></option>
                                                    <?php } ?>

                                                    <?php
                                                        foreach ($kontingen as $ktn) {
                                                            echo "<option value='".$ktn->id_kontingen."'>".nama_kontingen($ktn->id_kontingen)."</option>";
                                                        }
                                                    ?>
                                                   
                                                   
                                                </select>
                                                <i class="arrow double"></i>
                                            </label>
                                            <input type="hidden" name="kompetisi" id="kompetisi" value="<?=$rowdata->id_kompetisi?>">
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label class="field select prepend-icon">
                                          
                                                <select id="cabor"  name="cabor">
                                                <?php if($c == NULL){ ?>
                                                        <option disabled selected>Cabor</option>
                                                    <?php }else{ ?>
                                                        <option selected value="<?=$c?>"><?=nama_cabor($c)?></option>
                                                    <?php } ?>
                                                   
                                                   
                                                </select>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                    
                                        <div class="section">
                                            <label class="field select prepend-icon">
                                                
                                                <select id="jk"  name="jk">
                                                <?php if($j == NULL){ ?>
                                                        <option disabled selected>Jenis Kelamin</option>
                                                    <?php }else{ ?>
                                                        <option selected value="<?=$j?>"><?=nama_jenis($j)?></option>
                                                    <?php } ?>
                                                    <option value="Pa">Putra</option>
                                                    <option value="Pi">Putri</option>
                                                   
                                                </select>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->
                                            
                                        <div class="section">
 
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-bordered btn-primary">
                                                   Tampilkan Data
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                    </div>
                                    <!-- -------------- /Form -------------- -->
                                    <div class="panel-footer"></div>
                                <?php echo form_close();?>
                            </div>
                            <!-- -------------- /Panel -------------- -->
                        </div>
                        <!-- -------------- /Registration -------------- -->


                        

        <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    
                    <div class="panel-body pn">
                        <div class="table-responsive">
                        <?=form_open('kompetisi/simpan_validasi/', 'autocomplete="off" class="form-horizontal" role="form"'); ?>
                            <table border="0" style="width:100%" id="example1" class="table table-bordered table-striped">
                            <thead>
									<tr>
										
										<th rowspan="2">Nama Atlit</th>
                                        <th rowspan="2">Ijazah</th>
                                        <th rowspan="2">Akte</th>
                                        <th rowspan="2">Raport</th>
										<th rowspan="2">Sekolah</th>
										<th rowspan="2">Kelas</th>
										<th colspan="2">SK Sekolah</th>
										<th colspan="2">STTB</th>
										<th colspan="2">Raport</th>
										<th colspan="2">Akta Lahir</th>
										<th colspan="2">SK Dispora</th>
										<th colspan="2">NISN</th>
										<th colspan="2">SK SKB</th>
										<th colspan="2">Kartu Hasil Belajar</th>
										<th rowspan="2">Ket</th>
									</tr>
									<tr>
										<th>A</th>
										<th>C</th>
										<th>A</th>
										<th>C</th>
										<th>A</th>
										<th>C</th>
										<th>A</th>
										<th>C</th>
										<th>A</th>
										<th>C</th>
										<th>A</th>
										<th>C</th>
										<th>A</th>
										<th>C</th>
										<th>A</th>
										<th>C</th>
									</tr>
								</thead>
                                <tbody>
                                
                                <?php foreach($atlit as $atl){ ?>
                                    <tr>
                                    <input type="hidden" name="k" value="<?=$k?>">
                                    <input type="hidden" name="c" value="<?=$c?>">
                                    <input type="hidden" name="j" value="<?=$j?>">

											<td>
                                            <input type="hidden" name="id[]" value="<?=$atl->id_kompetisi_by_name_atlit?>">
                                            <?=nama_atlit($atl->id_atlit)?>
                                            </td>

                                            <td>
                                            <?php if($atl->ijazah != NULL){ ?>

                                                <a href="<?=base_url($atl->ijazah)?>"><span class="fa fa-check"></span></a>
                                            <?php }else{ ?>
                                                <span class="fa fa-times"></span>
                                            <?php } ?>
                                            
                                            </td>
                                            

                                            <td>

                                            <?php if($atl->akte != NULL){ ?>

                                            <a href="<?=base_url($atl->ijazah)?>"><span class="fa fa-check"></span></a>
                                            <?php }else{ ?>
                                            <span class="fa fa-times"></span>
                                            <?php } ?>
                                           
                                            </td>

                                            <td>


                                            <?php if($atl->raport != NULL){ ?>

                                                <a href="<?=base_url($atl->ijazah)?>"><span class="fa fa-check"></span></a>
                                                <?php }else{ ?>
                                                <span class="fa fa-times"></span>
                                                <?php } ?>
                                            
                                            </td>

											<td><textarea readonly name="sekolah[]" rows="2"><?=$atl->sekolah?></textarea></td>
											<td><input type="text" name="kelas[]" size="2" value="<?=$atl->kelas?>"></td>
											<td>
												<select name="sksa[]">
                                                    <option <?php if($atl->sksa == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->sksa == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="sksc[]">
													<option <?php if($atl->sksc == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->sksc == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="sttba[]">
                                                    <option <?php if($atl->sttba == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->sttba == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="sttbc[]">
                                                    <option <?php if($atl->sttbc == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->sttbc == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="raporta[]">
                                                    <option <?php if($atl->raporta == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->raporta == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="raportc[]">
                                                <option <?php if($atl->raportc == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->raportc == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="aktaa[]">
                                                <option <?php if($atl->aktaa == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->aktaa == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="aktac[]">
                                                <option <?php if($atl->aktac == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->aktac == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="skda[]">
                                                <option <?php if($atl->skda == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->skda == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="skdc[]">
                                                <option <?php if($atl->skdc == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->skdc == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="nisna[]">
                                                <option <?php if($atl->nisna == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->nisna == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="nisnc[]">
                                                <option <?php if($atl->nisnc == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->nisnc == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="skskba[]">
                                                    <option <?php if($atl->skskba == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->skskba == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="skskbc[]">
                                                    <option <?php if($atl->skskbc == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->skskbc == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="khba[]">
                                                    <option <?php if($atl->khba == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->khba == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="khbc[]">
                                                    <option <?php if($atl->khbc == 1){echo 'selected';}?> value="1">Ada</option>
													<option <?php if($atl->khbc == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
											<td>
												<select name="ket[]">
                                                    <option <?php if($atl->ket == 1){echo 'selected';}?> value="1">Sah</option>
													<option <?php if($atl->ket == 0){echo 'selected';}?> value="0">Tidak</option>
												</select>
											</td>
										</tr>
                                <?php }  ?>
                                

                                </tbody>
                                
                            </table>
                            <div class="topbar-right  mt5 mr35">
                            <button type="submit" class="btn btn-primary">Simpan Data <i class="fa fa-save"></i></button>
							    <?=form_close(); ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>
<?php include(__DIR__ . "/../template/datatable.php"); ?>
<script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "info": false,   
      "bLengthChange": false,
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "autoWidth": true,
      "scrollX": true,
      "autoWidth": true
    });
});
</script>

<script>
  $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
    // Kita sembunyikan dulu untuk loadingnya
    $("#loading").hide();
    
    $("#kontingen").change(function(){ // Ketika user mengganti atau memilih data provinsi
      $("#cabor").hide(); // Sembunyikan dulu combobox kota nya
      $("#loading").show(); // Tampilkan loadingnya
    
      $.ajax({
        type: "POST", // Method pengiriman data bisa dengan GET atau POST
        url: "<?php echo base_url("kompetisi/listCaborKontingenKompetisi"); ?>", // Isi dengan url/path file php yang dituju
        data: {kontingen : $("#kontingen").val(),
                kompetisi : $("#kompetisi").val()
            }, // data yang akan dikirim ke file yang dituju
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ // Ketika proses pengiriman berhasil
          $("#loading").hide(); // Sembunyikan loadingnya
          // set isi dari combobox kota
          // lalu munculkan kembali combobox kotanya
          $("#cabor").html(response.list_cabor).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
    });
  });
  </script>
  




</body>
</html>


