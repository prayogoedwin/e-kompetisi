 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <!-- <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li> -->
        </ul>
    </div>
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

  <!-- -------------- Column Center -------------- -->
  <div class="chute chute-center">

                       
                             <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title "> Daftar Kontingen Pada Kompetisi <?=ucfirst($rowdata->nama_kompetisi)?></span>
                    </div>
                    

                    <div class="panel-body pn">
                        <div class="table-responsive">
                        <?php echo form_open('kompetisi/adds_kontingen') ?>
                        <input type="hidden" name="id_kompetisi" value="<?=$rowdata->id_kompetisi?>">
                            <table border="0" id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center" width="5%">
                                    <input type="checkbox" name="select-all" onclick="toggle(this);">
                                    Keikutsertaan</th>
                                    <th class="text-center">Nama Kontingen</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if($alldata == FALSE ){ ?>
                                    <tr class="bg-light">                                    
                                    <th class="text-center" colspan="3">Data Kosong</th>
                                    
                                </tr>
                                <?php }else{ 
                                $no = 0;
                                foreach($alldata as $all): 
                                $no++
                                ?>
                                <tr>
                                    
                                    <td class="text-center">
                                                <label class="option block mn">
                                                
                                                    <input type="checkbox" name="id_kontingen[]" value="<?=$all->id_kontingen?>">
                                                    <span class="checkbox mn"></span>
                                                </label>
                                    </td>
   
                                    <td class=""><?=nama_kontingen($all->id_kontingen)?></td>
                                    
                                    
                                </tr>
                                <?php endforeach;} ?>

                                </tbody>
                            </table>
                            <br/>
                            <div class="section">
                                            
                                            <div class="pull-left">
                                                <button type="submit" class="btn btn-bordered btn-primary">
                                                    Tambah Kontingen
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                           <?php echo form_close();?>

                        </div>
                    </div>
                </div>
            </div>

                        

</div>
<!-- -------------- //End Column Center -------------- -->

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>
<script>
function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
</script>


</body>
</html>


