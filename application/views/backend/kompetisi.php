 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <!-- <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li> -->
        </ul>
    </div>
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
        <a href="sales-stats-purchases.html" class="btn btn-primary btn-sm ml10" title="New Order">
            <span class="fa fa-plus pr5"></span><span class="fa fa-file-o pr5"></span>Tambah Kompetisi</a>
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

        <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title hidden-xs"> Daftar Kompetisi</span>
                    </div>
                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <table border="1" class="table border allcp-form theme-warning tc-checkbox-1 fs13">
                                <thead>
                                <tr class="bg-light">
                                    
                                    <th class="text-center">Kompetisi</th>
                                    <th class="text-center" colspan="2">Pendaftran</th>
                                    <th class="text-center" colspan="2">Jadwal</th>
                                    <th class="text-center">link detail</th>
                                    <th class="text-right">Status</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    
                                    <td class="">POPDA</td>
                                    <td class="">1 Oktober 2019</td>
                                    <td class="">10 Oktober 2019</td>
                                    <td class="">1 Oktober 2019</td>
                                    <td class="">10 Oktober 2019</td>
                                    <td class="text-center">
                                    <a href="<?=base_url('admin/detail')?>"><button type="button"
                                                    class="btn btn-info br2 btn-xs fs12"
                                                     aria-expanded="false"> Detail
                                                     <span class="fa fa-file-o pr5">
                                            </button>
                                    </a>
                                    </td>
                                    <td class="text-right">
                                        <div class="btn-group text-right">
                                            <button type="button"
                                                    class="btn btn-success br2 btn-xs fs12 dropdown-toggle"
                                                    data-toggle="dropdown" aria-expanded="false"> Active
                                                <span class="caret ml5"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="#">Edit</a>
                                                </li>
                                                <li>
                                                    <a href="#">Delete</a>
                                                </li>
                                               
                                            
                                            </ul>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    
                                    <td class="">POSPEDA</td>
                                    <td class="">2 Juni 2019</td>
                                    <td class="">5 Juni 2019</td>
                                    <td class="">1 Agustus 2019</td>
                                    <td class="">17 Agustus 2019</td>
                                    <td class="text-center">
                                    <button type="button"
                                                    class="btn btn-info br2 btn-xs fs12"
                                                     aria-expanded="false"> Detail
                                                     <span class="fa fa-file-o pr5">
                                            </button>
                                    </td>
                                    <td class="text-right">
                                        <div class="btn-group text-right">
                                            <button type="button"
                                                    class="btn btn-success br2 btn-xs fs12 dropdown-toggle"
                                                    data-toggle="dropdown" aria-expanded="false"> Inactive
                                                <span class="caret ml5"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="#">Edit</a>
                                                </li>
                                                <li>
                                                    <a href="#">Delete</a>
                                                </li>
                                                
                                            
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/template/footer.php"); ?>

</body>

</html>


