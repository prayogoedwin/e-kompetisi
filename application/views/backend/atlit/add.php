 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">





<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

  <!-- -------------- Column Center -------------- -->
  <div class="chute chute-center">

                       
                            <!-- -------------- Registration -------------- -->
                            <div class="allcp-form theme-primary tab-pane mw600" id="register" role="tabpanel">
                            <div class="panel">
                                <div class="panel-heading">
                                    <span class="panel-title">
                                      Form Tambah Atlit
                                    </span>
                                </div>
                                <!-- -------------- /Panel Heading -------------- -->

                                 <?php echo form_open_multipart('atlit/adds') ?>
                                    <div class="panel-body pn">
                                        <div class="section">
                                        NO KTP
                                            <label for="email" class="field prepend-icon">
                                            
                                                <input type="number" name="nik" id="nik" class="gui-input" required
                                                       placeholder="KTP">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-book"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        NISN
                                            <label for="email" class="field prepend-icon">
                                                <input type="number" name="nisn" id="nisn" class="gui-input" required
                                                       placeholder="NISN">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-check"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Sekolah
                                            <label for="email" class="field prepend-icon">
                                                <input type="text" name="sekolah" id="sekolah" class="gui-input" required
                                                       placeholder="Sekolah">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-building"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Nama Lengkap Atlit
                                            <label for="email" class="field prepend-icon">
                                                <input type="text" name="nama_atlit" id="nama_atlit" class="gui-input" required
                                                       placeholder="Nama Atlit">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-user"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Jenis Kelamin
                                            <label class="field select prepend-icon">
                                                <select id="jk"  name="jk" required>
                                                    <option value="Pa">Putra</option>
                                                    <option value="Pi">Putri</option>
                                                   
                                                </select>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Alamat
                                            <label for="email" class="field prepend-icon">
                                                <input type="text" name="alamat" id="alamat" class="gui-input" required
                                                       placeholder="Alamat">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-map-marker"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Tempat Lahir
                                            <label for="email" class="field prepend-icon">
                                                <input type="text" name="tempat_lahir" id="tempat_lahir" class="gui-input" required
                                                       placeholder="tempat_lahir">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-home"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Tanggal Lahir
                                            <label for="email" class="field prepend-icon">
                                                <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="gui-input" required
                                                       placeholder="tanggal_lahir">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-calendar"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Tinggi Badan (CM)
                                            <label for="email" class="field prepend-icon">
                                                <input type="number" name="tinggi" id="tinggi" class="gui-input" required
                                                       placeholder="Tinggi Badan">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-arrows-v"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Berat Badan (Kg)
                                            <label for="email" class="field prepend-icon">
                                                <input type="number" name="berat" id="berat" class="gui-input" required
                                                       placeholder="Berat Badan">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-arrows-h"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                            <div class="section">
                                            Pas Foto Atlit (JPG/JPEG/PNG) Max 500kb
                                            <label for="email" class="field prepend-icon">
                                                <input type="file" name="foto" id="foto" class="gui-input" required
                                                       placeholder="Foto atlit">
                                                <label for="icon" class="field-icon">
                                                    <i class="fa fa-file-image-o"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                            
                                        <div class="section">
                                            
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-bordered btn-primary">
                                                    Tambah atlit
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                    </div>
                                    <!-- -------------- /Form -------------- -->
                                    <div class="panel-footer"></div>
                                <?php echo form_close();?>
                            </div>
                            <!-- -------------- /Panel -------------- -->
                        </div>
                        <!-- -------------- /Registration -------------- -->

                        

</div>
<!-- -------------- //End Column Center -------------- -->

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>



</body>
</html>


