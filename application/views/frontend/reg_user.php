
    <section class="blog-wrapper">
    	<div class="container">
        	<div id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                   <div class="col-md-12">
 						<div class="widget">
                        	<div class="title">
                                <h3>Buat Akun Baru</h3><br>
                                <h4>Silahkan lengkapi form berikut untuk mendaftar</h4>
                            </div>
                            <?php 
                                if (!empty($this->session->flashdata('sukses'))){
                                    echo '<div class="alert alert-success">'.$this->session->flashdata('sukses').'</div>';
                                }elseif(!empty($this->session->flashdata('gagal'))){
                                    echo '<div class="alert alert-danger">'.$this->session->flashdata('gagal').'</div>';
                                }
                            ?>
              		        <?php echo form_open_multipart('flogin/daftar',array('class'=>'form-horizontal'));?>
                                <div class="form-group">
                                    <input type="text" name="nama_depan" class="form-control" placeholder="Nama Depan" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="nama_belakang" class="form-control" placeholder="Nama Belakang" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Password" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="repassword" class="form-control" placeholder="Ulangi password" required="required">
                                </div>
                                <h4>Sudah Punya Akun? Klik <b><a href="<?=base_url('Flogin');?>">Disini</a></b> Untuk Login</h4>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="Register an account">
                                </div>
           			        <?php echo form_close(); ?>
                        </div><!-- end widget -->
					</div><!-- end col-md-6 -->
            	</div><!-- end row --> 
            </div><!-- end content -->
    	</div><!-- end container -->
    </section><!--end white-wrapper -->