<section class="slider-wrapper">
    
    <div class="tp-banner-container">
        <div class="tp-banner" >
            <ul><!-- SLIDE  -->
            <?php foreach($banner as $bnr): ?>
               <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                    <img src="<?php echo base_url().$bnr->foto ?>"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                    
                    <div class="tp-caption small_title  customin customout start"
                        data-x="right" data-hoffset="0"
                        data-y="330"
                        data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                        data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-speed="1600"
                        data-start="1100"
                        data-easing="Back.easeInOut"
                        data-endspeed="300"><a target="BLANK" href="<?=$bnr->link?>" class="btn btn-primary btn-lg"><?=$bnr->button?></a>
                    </div>
                </li>
            <?php endforeach; ?>
                
                
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</section><!-- end slider-wrapper -->

<section class="grey-wrapper jt-shadow">
    <div class="container">
        
        <div class="col-lg-12">
        <div class="container">
        	<div class="general-title">
            	<h2>Pengumuman & Berita</h2>
                <hr>
            </div><!-- end general title -->
        </div><!-- end container -->
        
            <div class="padding-top margin-top">
                <div id="owl_blog_two_line" class="owl-carousel">
                <?php foreach($informasi as $info) : ?>
                    <div class="blog-carousel">
                        <div class="entry">
                            <img src="<?php echo base_url('assets/images/informasi/').$info->foto ?>" alt="" class="img-responsive">
                            <div class="magnifier">
                                <div class="buttons">
                                    <a class="st" rel="bookmark" href="blog-single-sidebar.html"><i class="fa fa-link"></i></a>
                                </div><!-- end buttons -->
                            </div><!-- end magnifier -->
                            <div class="post-type">
                                <i class="fa fa-picture-o"></i>
                            </div><!-- end pull-right -->
                        </div><!-- end entry -->
                        <div class="blog-carousel-header">
                            <h3><a title="" href="blog-single-sidebar.html"><?=$info->judul?></a></h3>
                            <div class="blog-carousel-meta">
                                <span><i class="fa fa-calendar"></i> <?=$this->formatter->getDateMonthFormatUser($info->created_at)?></span>
                                <!-- <span><i class="fa fa-comment"></i> <a href="#">03 Comments</a></span>
                                <span><i class="fa fa-eye"></i> <a href="#">84 Views</a></span> -->
                            </div><!-- end blog-carousel-meta -->
                        </div><!-- end blog-carousel-header -->
                        <div class="blog-carousel-desc">
                            <p></p>
                        </div><!-- end blog-carousel-desc -->
                    </div><!-- end blog-carousel -->
                <?php endforeach; ?>
                     
                </div><!-- end owl-blog -->
            </div><!-- end padding-top -->
        </div><!-- end col-lg-12 -->

        <div class="container">
                <div class="messagebox">
                    <br/>
                    <a class="btn btn-primary btn-lg" href="#">Lihat Semua Berita</a>
                </div><!-- end messagebox -->
            </div><!-- end container -->
    </div><!-- end container -->
</section><!-- end grey wrapper -->



  
