<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Cek QR Code</title>
  </head>
  <body>
    <!-- Class Card untuk container dari Card -->
    <div
      class="card"
      style="width:320px;margin: 0 auto; display: flex; flex-direction: column;"
    >
      <!-- Class Content untuk Container Data Content -->
      <div class="content" style="padding: 20px 20px 0 20px;">
        <!-- Class Heading untuk bagian head QrCode dan Logo -->
        <div class="heading" style="display:flex;">
          <div class="qrcode" style="width: 50%;">
            <img src="<?=base_url('assets/qr/'.$rowdata->qr)?>" alt="QR Code" style="height:90px;" />
          </div>
          <div class="logo" style="width: 50%; text-align: right;">
            <img src="<?=base_url()?>assets/images/kontingen/Provinsi_Jawa_Tengah.png" alt="Logo" style="height:90px;" />
          </div>
        </div>
        <div class="foto" style="margin: 24px 0 24px 0;text-align: center;">
          <img src="<?=base_url($rowdata->foto)?>" style="height: 164px;" alt="Foto" />
        </div>
        <h4 style="text-align: center; margin: 0; text-transform:uppercase;"><?=$rowdata->nama_atlit?></h4>
        <h4 style="text-align: center; margin: 0; text-transform:uppercase;"><?=nama_kontingen($rowdata->id_kontingen)?></h4>
          <h4 style="text-align: center; margin: 8px 0 0 0; text-transform:uppercase;">Atlit</h4>
          
      </div>

     
      <!-- Class Footer sebagai footer copyright -->
      <div
        class="footer"
        style="background-color: #ff5757; padding: 16px 12px; text-align: center; margin: 24px 0;font-size:10px;"
      >
        &copy; 2019 Copyright Nama Aplikasi
      </div>

    
  <table style="width:100%">
  <?php if($alldata == FALSE ){ ?>
  
  <?php }else{ ?>

    <tr>
    <th>Kompetisi</th>
    <th>Tahun</th>
   
  </tr>

  <?php } ?>
  <?php if($alldata == FALSE ){ ?>
    <!-- <tr class="bg-light">                                    
    <th class="text-center" colspan="2">Data Kosong</th>  
</tr> -->
<?php }else{ 
$no = 0;
foreach($alldata as $all): 
$no++
?>
  <tr>
    <td><?=nama_kompetisi($all->id_kompetisi)?></td>
    <td><?php echo date('Y',strtotime(select_kompetisi($all->id_kompetisi)->created_at)) ?></td>
    
  </tr>
  <?php endforeach; }?>
  
</table>
    </div>
  </body>
</html>
