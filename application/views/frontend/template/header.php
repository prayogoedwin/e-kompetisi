<body>
	

<header id="header-style-1">
		<div class="container">
			<nav class="navbar yamm navbar-default">
				<div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?=base_url()?>" class="navbar-brand">e-Kompetisi</a>
        		</div><!-- end navbar-header -->
                
				<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right">
					<ul class="nav navbar-nav">
						
            <li><a href="<?=base_url();?>">Beranda</a></li>
						
                        <!-- standard drop down -->
                        <!-- <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Kompetisi <div class="arrow-up"></div></a>
                            <ul class="dropdown-menu" role="menu">
                                <?php foreach($kompetisi as $kmp): ?>
                                <li><a target="BLANK" href="<?=base_url($kmp->url)?>"><?=$kmp->nama_kompetisi?></a></li>
                                <?php endforeach; ?>
                               
                                
                            </ul>
                        </li> -->
                        
                        <!-- <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Galeri <div class="arrow-up"></div></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="portfolio-masonry.html">Foto</a></li>
                                <li><a href="portfolio-masonry.html">Video</a></li>
                                
                            </ul>
                        </li> -->

                        <li><a href="<?=base_url('sys/foto');?>">Galeri</a></li>
                        <li><a href="<?=base_url('sys/informasi');?>">Pengumuman & Berita</a></li>
                        <li><a href="#footer-style-1">Kontak</a></li>
                        <li><a href="<?=base_url('sys/login');?>">Masuk</a></li>
					</ul><!-- end navbar-nav -->
				</div><!-- #navbar-collapse-1 -->			</nav><!-- end navbar yamm navbar-default -->
		</div><!-- end container -->
	</header><!-- end header-style-1 -->
	