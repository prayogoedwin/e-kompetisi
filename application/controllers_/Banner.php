<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Banner extends CI_Controller
{
    function __construct()
    {
		parent::__construct();
		$this->akses = $this->session->userdata('akses');
    }

    public function index()
	{
		
		$data['alldata'] = $this->Crud_model->get_all('e_banner');
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/banner/index',$data);
    }

    public function add()
	{
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/banner/add');
    }

    public function adds(){
		//image
		ini_set('max_execution_time', 300);
		$ext = pathinfo($_FILES['foto']['name']);
		$filename = md5($this->input->post('foto')).'.'.$ext['extension'];
		$config['upload_path']   = './assets/images/banner/'; 
		$config['allowed_types'] = 'png|jpg|jpeg';
        $config['file_name'] = $filename;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
			if (!empty($_FILES['foto']['name'])) {
				$upload = $this->upload->data(); 
				$savenamefile = 'assets/images/banner/' . $filename;
			}
			$data = array(
                'judul' => $this->input->post('nama_banner'),
                'button' =>  $this->input->post('button'),	
                'link'  => 	$this->input->post('link'),			
				'foto' => $savenamefile,
				'created_at' => date('Y-m-d H:i:s'),
			);
			$insert = $this->Crud_model->insert('e_banner', $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('banner'));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('banner'));
			}
		} else { 
			if (($_FILES['foto']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('banner'));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload foto');
				redirect(site_url('banner'));
			}
		
		}
    }
    
    public function hapus($id){
        $del = $this->Crud_model->is_delete('e_banner', 'id', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('banner'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('banner'));
        }
    }
    
    public function rubah($id){
        $row = $this->Crud_model->get_by_id('e_banner', 'id', $id);
        if($row->status == 'ON'){
            $data = array(
                    'status' => 'OFF'
            );
        }else{
            $data = array(
                'status' => 'ON'
            );
        }
        
        $up = $this->Crud_model->update('e_banner', 'id', $id, $data);
        if($up != false){
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Rubah Status');
				redirect(site_url('banner'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Rubah Status');
				redirect(site_url('banner'));
        }
    }
	
	public function detail($id_banner)
	{
		$det = $this->Crud_model->get_by_id('e_banner', 'id', $id_banner);
		if($det != false){
				$data['detail'] = $det;
				$this->load->view('backend/template/head');
				$this->load->view('backend/template/header');
				$this->load->view('backend/template/sidebar');
				$this->load->view('backend/banner/detail',$data);
		}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'banner Tidak di Temukan');
				redirect(site_url('banner'));
		}
    }

}