<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Informasi extends CI_Controller
{
    function __construct()
    {
		parent::__construct();
		$this->akses = $this->session->userdata('akses');
    }

    public function index()
	{
		
		$data['alldata'] = $this->Crud_model->get_all('e_informasi');
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/informasi/index',$data);
    }

    public function add()
	{
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/informasi/add');
    }

    public function adds(){
		//image
		ini_set('max_execution_time', 300);
        // $ext = pathinfo($_FILES['foto']['name']);
        // $ext2 = pathinfo($_FILES['file']['name']);
        // $filename = md5($this->input->post('foto')).'.'.$ext['extension'];
        // $filename2 = md5($this->input->post('file')).'.'.$ext2['extension'];
		$config['upload_path']   = './assets/images/informasi/'; 
		$config['allowed_types'] = 'png|jpg|jpeg|pdf';
        $config['max_size'] = '500';
        $config['remove_spaces'] = TRUE;
           $config['encrypt_name'] = TRUE;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto') || empty($_FILES['foto']['name'])) {
			if (!empty($_FILES['foto']['name'])) {
				$upload = $this->upload->data(); 
				//$savenamefile = 'assets/images/informasi/' . $filename;
            }
		} else { 
            $upload = '-';
			if (($_FILES['foto']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('informasi'));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload foto');
				redirect(site_url('informasi'));
			}
        }

        if ($this->upload->do_upload('file') || empty($_FILES['file']['name'])) {
			if (!empty($_FILES['file']['name'])) {
				$upload2 = $this->upload->data(); 
				//$savenamefile2 = 'assets/images/informasi/' . $filename2;
            }
		} else { 
            $upload2 = '-';
			if (($_FILES['file']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('informasi'));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload foto');
				redirect(site_url('informasi'));
			}
        }
        
        $data = array(
            'judul' => $this->input->post('judul'),
            'isi' =>  $this->input->post('isi'),			
            'foto' => $upload['file_name'],
            'file' => $upload2['file_name'],
            'created_at' => date('Y-m-d H:i:s'),
        );
        $insert = $this->Crud_model->insert('e_informasi', $data);
        if ($insert) {
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Menambah');
            redirect(site_url('informasi'));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal Menambah');
            redirect(site_url('informasi'));
        }
    }
    
    public function hapus($id){
        $del = $this->Crud_model->is_delete('e_informasi', 'id', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('informasi'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('informasi'));
        }
    }
    
    public function rubah($id){
        $row = $this->Crud_model->get_by_id('e_informasi', 'id', $id);
        if($row->status == 'ON'){
            $data = array(
                    'status' => 'OFF'
            );
        }else{
            $data = array(
                'status' => 'ON'
            );
        }
        
        $up = $this->Crud_model->update('e_informasi', 'id', $id, $data);
        if($up != false){
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Rubah Status');
				redirect(site_url('informasi'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Rubah Status');
				redirect(site_url('informasi'));
        }
    }
	
	public function detail($id_informasi)
	{
		$det = $this->Crud_model->get_by_id('e_informasi', 'id', $id_informasi);
		if($det != false){
				$data['detail'] = $det;
				$this->load->view('backend/template/head');
				$this->load->view('backend/template/header');
				$this->load->view('backend/template/sidebar');
				$this->load->view('backend/informasi/detail',$data);
		}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'informasi Tidak di Temukan');
				redirect(site_url('informasi'));
		}
    }

}