<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Kompetisi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kompetisi_model');
    }

    public function index()
	{
        $data['alldata'] = $this->Crud_model->get_all_order_by('e_kompetisi','id_kompetisi','DESC');
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/index',$data);
    }

    public function add()
	{
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/add');
    }

    public function adds(){
		//image
		ini_set('max_execution_time', 300);
		$ext = pathinfo($_FILES['icon']['name']);
		$filename = md5($this->input->post('icon')).$ext['extension'];
		$config['upload_path']   = './assets/images/kompetisi/'; 
		$config['allowed_types'] = 'png';
        $config['max_size'] = '500';
        $config['file_name'] = $filename;

		if (!file_exists($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('icon') || empty($_FILES['icon']['name'])) {
			if (!empty($_FILES['icon']['name'])) {
				$upload = $this->upload->data(); 
				$savenamefile = 'assets/images/kompetisi/' . $filename;
			}
			$data = array(
                'nama_kompetisi' => $this->input->post('nama_kompetisi'),
                'icon' => $savenamefile,
                'deskripsi' => $this->input->post('deskripsi'),
                'tahap' => '1',
                'status' => 'OFF',
                'created_at' => date('Y-m-d H:i:s'),
                'mulai' => $this->input->post('mulai'),
                'selesai' => $this->input->post('selesai'),
                'lokasi' => $this->input->post('lokasi')
			);
			$insert = $this->Crud_model->insert('e_kompetisi', $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('kompetisi'));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('kompetisi'));
			}
		} else { 
			if (($_FILES['icon']['size']/1000)> 500) {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, ukuran file melebihi 500KB');
				redirect(site_url('kompetisi'));
			} else {
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal, upload icon');
				redirect(site_url('kompetisi'));
			}
		
		}
    }
    
    public function hapus($id){
        $del = $this->Crud_model->is_delete('e_kompetisi', 'id_kompetisi', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('kompetisi'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('kompetisi'));
        }
    }

    public function rubah($id){
        $row = $this->Crud_model->get_by_id('e_kompetisi', 'id_kompetisi', $id);
        if($row->status == 'ON'){
            $data = array(
                    'status' => 'OFF'
            );
        }else{
            $data = array(
                'status' => 'ON'
            );
        }
        
        $up = $this->Crud_model->update('e_kompetisi', 'id_kompetisi', $id, $data);
        if($up != false){
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Rubah Status');
				redirect(site_url('kompetisi'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Rubah Status');
				redirect(site_url('kompetisi'));
        }
    }


    // ini crud kontingen peserta
    public function kontingen($id)
	{
        $data['alldata'] = $this->Crud_model->get_all_by('e_kompetisi_kontingen','id_kompetisi', $id);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/kontingen',$data);
    }

    public function add_kontingen($id)
	{
        $data['alldata'] = $this->Crud_model->get_all('e_kontingen');
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/add_kontingen',$data);
    }

    public function adds_kontingen(){
            $id_kontingen = $this->input->post('id_kontingen');
            $id_kompetisi = $this->input->post('id_kompetisi');

            for($i = 0; $i < count($id_kontingen); $i++){
                $insert_data[] = array(
                    'id_kontingen'	=> $id_kontingen[$i],
                    'id_kompetisi' 	=> $id_kompetisi,
                    'created_at' => date('Y-m-d H:i:s'),
                );
            }
            //echo json_encode($update_data);
            $insert = $this->db->insert_batch('e_kompetisi_kontingen', $insert_data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('kompetisi/kontingen/'.$id_kompetisi));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('kompetisi/kontingen/'.$id_kompetisi));
			}
    }

    public function hapus_kontingen($id){
        $row = $this->Crud_model->get_by_id('e_kompetisi_kontingen', 'id_kompetisi_kontingen', $id);
        $del = $this->Crud_model->is_delete('e_kompetisi_kontingen', 'id_kompetisi_kontingen', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('kompetisi/kontingen/'.$row->id_kompetisi));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('kompetisi/kontingen/'.$row->id_kompetisi));
        }
    }

    // ini adalah crud cabor kompetisi
     public function cabor($id)
     {
         $data['alldata'] = $this->Crud_model->get_all_by('e_kompetisi_cabor','id_kompetisi', $id);
         $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
         $this->load->view('backend/template/head');
         $this->load->view('backend/template/header');
         $this->load->view('backend/template/sidebar');
         $this->load->view('backend/kompetisi/cabor',$data);
     }
 
     public function add_cabor($id)
     {
         $data['alldata'] = $this->Crud_model->get_all('e_cabor');
         $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
         $this->load->view('backend/template/head');
         $this->load->view('backend/template/header');
         $this->load->view('backend/template/sidebar');
         $this->load->view('backend/kompetisi/add_cabor',$data);
     }
 
     public function adds_cabor(){
             $id_cabor = $this->input->post('id_cabor');
             $id_kompetisi = $this->input->post('id_kompetisi');
 
             for($i = 0; $i < count($id_cabor); $i++){
                 $insert_data[] = array(
                     'id_cabor'	        => $id_cabor[$i],
                     'id_kompetisi' 	=> $id_kompetisi,
                     'created_at' => date('Y-m-d H:i:s'),
                 );
             }
             //echo json_encode($update_data);
             $insert = $this->db->insert_batch('e_kompetisi_cabor', $insert_data);
             if ($insert) {
                 $this->session->set_flashdata('info', 'success');
                 $this->session->set_flashdata('message', 'Berhasil Menambah');
                 redirect(site_url('kompetisi/cabor/'.$id_kompetisi));
             } else {
                 $this->session->set_flashdata('info', 'danger');
                 $this->session->set_flashdata('message', 'Gagal Menambah');
                 redirect(site_url('kompetisi/cabor/'.$id_kompetisi));
             }
     }
 
     public function hapus_cabor($id){
         $row = $this->Crud_model->get_by_id('e_kompetisi_cabor', 'id_kompetisi_cabor', $id);
         $del = $this->Crud_model->is_delete('e_kompetisi_cabor', 'id_kompetisi_cabor', $id);
         if($del != false){
              $this->session->set_flashdata('info', 'success');
                 $this->session->set_flashdata('message', 'Berhasil hapus');
                 redirect(site_url('kompetisi/cabor/'.$row->id_kompetisi));
         }else{
                 $this->session->set_flashdata('info', 'danger');
                 $this->session->set_flashdata('message', 'Gagal hapus');
                 redirect(site_url('kompetisi/cabor/'.$row->id_kompetisi));
         }
     }

     // ini adalah crud kelas kompetisi
     public function kelas($id)
     {
         $row = $this->Crud_model->get_by_id('e_kompetisi_cabor', 'id_kompetisi_cabor', $id);
         $data['alldata'] = $this->Crud_model->get_all_by('e_kompetisi_kelas','id_kompetisi_cabor', $id);
         $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $row->id_kompetisi);
         $data['rowdata2'] = $this->Crud_model->get_by_id('e_cabor', 'id_cabor', $row->id_cabor);
         $this->load->view('backend/template/head');
         $this->load->view('backend/template/header');
         $this->load->view('backend/template/sidebar');
         $this->load->view('backend/kompetisi/kelas',$data);
     }
 
    //  public function add_kelas($id)
    //  {
    //      $row = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
    //      $row2 = $this->Crud_model->get_by_id('e_kompetisi_cabor', 'id_kompetisi', $row->id_kompetisi);
    //      $data['alldata'] = $this->Crud_model->get_all_by('e_kelas','id_cabor',$row2->id_cabor);
    //      $data['rowdata'] =  $row;
    //      $data['rowdata3'] = $row2;
    //      $data['rowdata2'] = $this->Crud_model->get_by_id('e_cabor', 'id_cabor', $row2->id_cabor);
    //      $this->load->view('backend/template/head');
    //      $this->load->view('backend/template/header');
    //      $this->load->view('backend/template/sidebar');
    //      $this->load->view('backend/kompetisi/add_kelas',$data);
    //  }

     public function add_kelas($id)
     {
        $row = $this->Crud_model->get_by_id('e_kompetisi_cabor', 'id_kompetisi_cabor', $id);
        $data['alldata'] = $this->Crud_model->get_all_by('e_kelas','id_cabor',$row->id_cabor);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $row->id_kompetisi);
        $data['rowdata2'] = $row;
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/add_kelas',$data);
     }
 
     public function adds_kelas(){
             $id_kelas = $this->input->post('id_kelas');
             $id_cabor = $this->input->post('id_cabor');
             $id_kompetisi_cabor = $this->input->post('id_kompetisi_cabor');
             $id_kompetisi = $this->input->post('id_kompetisi');
 
             for($i = 0; $i < count($id_kelas); $i++){
                 $insert_data[] = array(
                     'id_kelas'	                => $id_kelas[$i],
                     'id_kompetisi_cabor'	    => $id_kompetisi_cabor,
                     'id_kompetisi' 	        => $id_kompetisi,
                     'id_cabor'                 => $id_cabor,
                     'created_at' => date('Y-m-d H:i:s'),
                 );
             }
             //echo json_encode($update_data);
             $insert = $this->db->insert_batch('e_kompetisi_kelas', $insert_data);
             if ($insert) {
                 $this->session->set_flashdata('info', 'success');
                 $this->session->set_flashdata('message', 'Berhasil Menambah');
                 redirect(site_url('kompetisi/kelas/'.$id_kompetisi_cabor));
             } else {
                 $this->session->set_flashdata('info', 'danger');
                 $this->session->set_flashdata('message', 'Gagal Menambah');
                 redirect(site_url('kompetisi/kelas/'.$id_kompetisi_cabor));
             }
     }
 
     public function hapus_kelas($id){
         $row = $this->Crud_model->get_by_id('e_kompetisi_kelas', 'id_kompetisi_kelas', $id);
         $del = $this->Crud_model->is_delete('e_kompetisi_kelas', 'id_kompetisi_kelas', $id);
         if($del != false){
              $this->session->set_flashdata('info', 'success');
                 $this->session->set_flashdata('message', 'Berhasil hapus');
                 redirect(site_url('kompetisi/kelas/'.$row->id_kompetisi_cabor));
         }else{
                 $this->session->set_flashdata('info', 'danger');
                 $this->session->set_flashdata('message', 'Gagal hapus');
                 redirect(site_url('kompetisi/kelas/'.$row->id_kompetisi_cabor));
         }
     }

     // ini crud keuikutsertaan peserta
    public function keikutsertaan($id)
	{
        $data['alldata'] = $this->Crud_model->get_all_by('e_kompetisi_keikutsertaan','id_kompetisi', $id);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/keikutsertaan',$data);
    }

    public function edit_keikutsertaan(){
        $id = $this->input->post('id');
        $row = $this->Crud_model->get_by_id('e_kompetisi_keikutsertaan', 'id_kompetisi_keikutsertaan', $id);
        $data = array(
            
            'atlit_pa' => $this->input->post('atlit_pa'),
            'atlit_pi' => $this->input->post('atlit_pi'),
            'pelatih_pa' => $this->input->post('pelatih_pa'),
            'pelatih_pi' => $this->input->post('pelatih_pi')
        );

        // echo json_encode($data);
        $up = $this->Crud_model->update('e_kompetisi_keikutsertaan', 'id_kompetisi_keikutsertaan', $id, $data);
        if($up != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Ubah');
				redirect(site_url('kompetisi/keikutsertaan/'.$row->id_kompetisi));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Ubah');
				redirect(site_url('kompetisi/keikutsertaan/'.$row->id_kompetisi));
        }
    }

    public function hapus_keikutsertaan($id){
        $row = $this->Crud_model->get_by_id('e_kompetisi_keikutsertaan', 'id_kompetisi_keikutsertaan', $id);
        $del = $this->Crud_model->is_delete('e_kompetisi_keikutsertaan', 'id_kompetisi_keikutsertaan', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('kompetisi/keikutsertaan/'.$row->id_kompetisi));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('kompetisi/keikutsertaan/'.$row->id_kompetisi));
        }
    }


    public function ke_by_number($id){ 
        $data = array(
                'tahap' => '2'
        );
        $up = $this->Crud_model->update('e_kompetisi', 'id_kompetisi', $id, $data);
        if($up != false){
               $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Ke Tahap By Number');
				redirect(site_url('kompetisi'));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Ke Tahap By Number');
				redirect(site_url('kompetisi'));
        }
    }

    // ini crud keuikutsertaan peserta
    public function update_keikutsertaan_detail($id)
	{
        $row = $this->Crud_model->get_by_id('e_kompetisi_keikutsertaan','id_kompetisi_keikutsertaan', $id);
        $data['rowdata'] = $row;
        $data['rowdata2'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $row->id_kompetisi);
        $data['atlit_pa_'] = $this->Crud_model->get_all_by_4_where('e_kompetisi_keikutsertaan_atlit','id_kontingen', $row->id_kontingen, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pa','id_kompetisi_keikutsertaan', $id);
        $data['atlit_pa'] = $this->Crud_model->get_all_by_2_where('e_atlit','id_kontingen', $row->id_kontingen, 'jk', 'Pa');
        $data['atlit_pi_'] = $this->Crud_model->get_all_by_4_where('e_kompetisi_keikutsertaan_atlit','id_kontingen', $row->id_kontingen, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pi','id_kompetisi_keikutsertaan', $id);
        $data['atlit_pi'] = $this->Crud_model->get_all_by_2_where('e_atlit','id_kontingen', $row->id_kontingen, 'jk', 'Pi');
        $data['pelatih_pa_'] = $this->Crud_model->get_all_by_4_where('e_kompetisi_keikutsertaan_pelatih','id_kontingen', $row->id_kontingen, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pa','id_kompetisi_keikutsertaan', $id);
        $data['pelatih_pa'] = $this->Crud_model->get_all_by_2_where('e_pelatih','id_kontingen', $row->id_kontingen, 'jk', 'Pa');
        $data['pelatih_pi_'] = $this->Crud_model->get_all_by_4_where('e_kompetisi_keikutsertaan_pelatih','id_kontingen', $row->id_kontingen, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pi','id_kompetisi_keikutsertaan', $id);
        $data['pelatih_pi'] = $this->Crud_model->get_all_by_2_where('e_pelatih','id_kontingen', $row->id_kontingen, 'jk', 'Pi');
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/update_keikutsertaan_detail',$data);
    }

    
    // ini crud keuikutsertaan peserta
    public function by_number($id)
	{
        $row = $this->Kompetisi_model->get_by_number($id);
        $data['alldata'] =   $row;
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        // $data['kelas'] = $this->Kompetisi_user_model->get_kelas_by();
        // $kel = array();
        // foreach($row as $xx){
        //     for($i = 0 ; $i > count($xx->id_cabor); $i ++){
        //         $kelas = $this->db->query("SELECT * FROM e_kelas WHERE id_cabor = '$xx->id_cabor' AND deleted_at IS NULL")->result();
        //     }
        // }
     
        // $data['kelas'] = $kelas;

        // echo json_encode($data);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/by_number',$data);
    }

    public function adds_by_number(){
        $data = array(
            'id_kompetisi' => $this->input->post('id_kompetisi'),
            'id_kontingen' => $this->input->post('id_kontingen'),
            'id_cabor' => $this->input->post('id_cabor'),
            'id_kelas' => $this->input->post('id_kelas'),
            'created_at' => date('Y-m-d H:i:s'),
        );
        $insert = $this->Crud_model->insert('e_kompetisi_by_number', $data);
        if ($insert) {
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Menambah');
            redirect(site_url('kompetisi/by_number/'.$this->input->post('id_kompetisi')));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal Menambah');
            redirect(site_url('kompetisi/by_number/'.$this->input->post('id_kompetisi')));
        }
}

public function hapus_by_number($id){
    $row = $this->Crud_model->get_by_id('e_kompetisi_by_number', 'id_kompetisi_by_number', $id);
    $del = $this->Crud_model->is_delete('e_kompetisi_by_number', 'id_kompetisi_by_number', $id);
    if($del != false){
         $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil hapus');
            redirect(site_url('kompetisi/by_number/'.$row->id_kompetisi));
    }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal hapus');
            redirect(site_url('kompetisi/by_number/'.$row->id_kompetisi));
    }
}

public function ke_by_name($id){ 
    $data = array(
            'tahap' => '3'
    );
    $up = $this->Crud_model->update('e_kompetisi', 'id_kompetisi', $id, $data);
    if($up != false){
           $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Ke Tahap By Name');
            redirect(site_url('kompetisi'));
    }else{
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal Ke Tahap By Name');
            redirect(site_url('kompetisi'));
    }
}   

    // ini crud keuikutsertaan peserta
    public function by_name($id)
	{
        $row = $this->Kompetisi_model->get_by_name($id);
        $data['alldata'] =   $row;
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        // $data['kelas'] = $this->Kompetisi_user_model->get_kelas_by();
        // $kel = array();
        // foreach($row as $xx){
        //     for($i = 0 ; $i > count($xx->id_cabor); $i ++){
        //         $kelas = $this->db->query("SELECT * FROM e_kelas WHERE id_cabor = '$xx->id_cabor' AND deleted_at IS NULL")->result();
        //     }
        // }
     
        // $data['kelas'] = $kelas;

        // echo json_encode($data);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/by_name',$data);
    }

    // ini crud keuikutsertaan peserta
    public function by_name_kontingen($id, $kontingen)
	{
        $row = $this->Kompetisi_user_model->get_by_number($id, $kontingen);
        $data['alldata'] = $this->Kompetisi_user_model->get_by_name($id, $kontingen);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $data['ikut'] = $this->Kompetisi_user_model->get_keikutsertaan($id, $kontingen);
        $data['kontingen'] = $kontingen;
        // $data['kelas'] = $this->Kompetisi_user_model->get_kelas_by();
        // $kel = array();
        // foreach($row as $xx){
        //     for($i = 0 ; $i > count($xx->id_cabor); $i ++){
        //         $kelas = $this->db->query("SELECT * FROM e_kelas WHERE id_cabor = '$xx->id_cabor' AND deleted_at IS NULL")->result();
        //     }
        // }
     
        // $data['kelas'] = $kelas;

        // echo json_encode($data);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/by_name_kontingen',$data);
    }

    public function listAtlit(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $id_kelas = $this->input->post('id_kelas');
        $id_kontingen = $this->input->post('id_kontingen');
        
        $kelas = $this->db->query("SELECT * FROM e_kelas WHERE id_kelas = '$id_kelas' AND deleted_at IS NULL")->row();
        $atlits = $this->db->query("SELECT * FROM e_atlit WHERE jk = '$kelas->jenis_kelas' AND id_kontingen = '$id_kontingen' AND deleted_at IS NULL")->result();
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option disabled selected value=''>Silakan Pilih Atlit</option>";
        
        foreach($atlits as $atls){
          $lists .= "<option value='".$atls->id_atlit."'>".$atls->nama_atlit."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_atlit'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }

    public function listAtlits(){
        // Ambil data ID Provinsi yang dikirim via ajax post
       
        $id_kontingen = $this->input->post('id_kontingen');
       
        $atlits = $this->db->query("SELECT * FROM e_atlit WHERE  id_kontingen = '$id_kontingen' AND deleted_at IS NULL")->result();
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option disabled selected value=''>Silakan Pilih Atlit</option>";
        
        foreach($atlits as $atls){
          $lists .= "<option value='".$atls->id_atlit."'>".$atls->nama_atlit."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_atlit'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }

    public function adds_atlit_by_name(){
        $row = $this->Crud_model->get_by_id('e_atlit', 'id_atlit', $this->input->post('id_atlit'));
        $data = array(
            'id_kompetisi' => $this->input->post('id_kompetisi'),
            'id_kontingen' => $this->input->post('id_kontingen'),
            'id_cabor' => $this->input->post('id_cabor'),
            'id_kelas' => $this->input->post('id_kelas'),
            'id_atlit' => $this->input->post('id_atlit'),
            'created_at' => date('Y-m-d H:i:s'),
            'sekolah' => $row->sekolah
        );
        $insert = $this->Crud_model->insert('e_kompetisi_by_name_atlit', $data);
        if ($insert) {
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Menambah');
            redirect(site_url('kompetisi/by_name_kontingen/'.$this->input->post('id_kompetisi').'/'.$this->input->post('id_kontingen') ));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal Menambah');
            redirect(site_url('kompetisi/by_name_kontingen/'.$this->input->post('id_kompetisi').'/'.$this->input->post('id_kontingen') ));
        }
    }

    public function hapus_atlit_by_name($id){
        $row = $this->Crud_model->get_by_id('e_kompetisi_by_name_atlit', 'id_kompetisi_by_name_atlit', $id);
        $del = $this->Crud_model->is_delete('e_kompetisi_by_name_atlit', 'id_kompetisi_by_name_atlit', $id);
        if($del != false){
            $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Berhasil hapus');
                redirect(site_url('kompetisi/by_name_kontingen/'.$row->id_kompetisi.'/'.$row->id_kontingen));
        }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal hapus');
                redirect(site_url('kompetisi/by_name_kontingen/'.$row->id_kompetisi.'/'.$row->id_kontingen));
        }
    }

    public function listPelatih(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $id_kelas = $this->input->post('id_kelas_p');
        $id_kontingen = $this->input->post('id_kontingen_p');
        
        $kelas = $this->db->query("SELECT * FROM e_kelas WHERE id_kelas = '$id_kelas' AND deleted_at IS NULL")->row();
        $pelatihs = $this->db->query("SELECT * FROM e_pelatih WHERE jk = '$kelas->jenis_kelas' AND id_kontingen = '$id_kontingen' AND deleted_at IS NULL")->result();
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option disabled selected value=''>Silakan Pilih Pelatih</option>";
        
        foreach($pelatihs as $plts){
          $lists .= "<option value='".$plts->id_pelatih."'>".$plts->nama_pelatih."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_pelatih'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }

    public function adds_pelatih_by_name(){
        $data = array(
            'id_kompetisi' => $this->input->post('id_kompetisi_p'),
            'id_kontingen' => $this->input->post('id_kontingen_p'),
            'id_cabor' => $this->input->post('id_cabor_p'),
            'id_kelas' => $this->input->post('id_kelas_p'),
            'id_pelatih' => $this->input->post('id_pelatih'),
            'created_at' => date('Y-m-d H:i:s'),
        );
        $insert = $this->Crud_model->insert('e_kompetisi_by_name_pelatih', $data);
        if ($insert) {
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Menambah');
            redirect(site_url('kompetisi/by_name_kontingen/'.$this->input->post('id_kompetisi_p').'/'.$this->input->post('id_kontingen_p') ));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal Menambah');
            redirect(site_url('kompetisi/by_name_kontingen/'.$this->input->post('id_kompetisi_p').'/'.$this->input->post('id_kontingen_p') ));
        }
    }

    public function hapus_pelatih_by_name($id){
        $row = $this->Crud_model->get_by_id('e_kompetisi_by_name_pelatih', 'id_kompetisi_by_name_pelatih', $id);
        $del = $this->Crud_model->is_delete('e_kompetisi_by_name_pelatih', 'id_kompetisi_by_name_pelatih', $id);
        if($del != false){
            $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Berhasil hapus');
                redirect(site_url('kompetisi/by_name_kontingen/'.$row->id_kompetisi.'/'.$row->id_kontingen));
        }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal hapus');
                redirect(site_url('kompetisi/by_name_kontingen/'.$row->id_kompetisi.'/'.$row->id_kontingen));
        }
    }

    public function ke_validasi($id){ 
        $data = array(
                'tahap' => '4'
        );
        $up = $this->Crud_model->update('e_kompetisi', 'id_kompetisi', $id, $data);
        if($up != false){
               $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Berhasil Ke Tahap By Name');
                redirect(site_url('kompetisi'));
        }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal Ke Tahap By Name');
                redirect(site_url('kompetisi'));
        }
    }  
    
    public function keabsahan($id)
	{
        $data['kontingen'] = $this->Crud_model->get_all_by('e_kompetisi_kontingen','id_kompetisi', $id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/keabsahan', $data);
    }



    function sebelum(){
  
        $this->load->library("excel");
  
        $object = new PHPExcel();
  
        $object->setActiveSheetIndex(0);
  
        $table_columns = array("Name", "Email");
  
        $column = 0;
  
        foreach($table_columns as $field){
  
          $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
  
          $column++;
  
        }
  
        $employee_data = $this->db->query("SELECT * FROM e_atlit")->result();
  
        $excel_row = 2;
  
        foreach($employee_data as $row){
  
          $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->id_atlit);
  
          $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nama_atlit);
  
          $excel_row++;
  
        }
  
        $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');
  
        header('Content-Type: application/vnd.ms-excel');
  
        header('Content-Disposition: attachment;filename="Employee Data.xls"');
  
        $object_writer->save('php://output');
  
      }


    public function sebelums($id){
        $kompetisi = nama_kompetisi($id);
        $kontingen = nama_kontingen($this->input->post('kontingen2'));
        $cabor = nama_cabor($this->input->post('cabor2'));
        $jk = nama_jenis($this->input->post('jk2'));

        $kontingen_ = $this->input->post('kontingen2');
        $cabor_ = $this->input->post('cabor2');
        $jk_ = $this->input->post('jk2');

        $atlit = $this->Kompetisi_model->get_validasi_by($id, $kontingen_, $cabor_, $jk_);
        $this->load->library("excel");
        $file = FCPATH.'assets/report/rpt-keabsahan.xls';

			$objExcel = PHPExcel_IOFactory::load($file);
			$objWriter = new PHPExcel_Writer_Excel5($objExcel);

			$styleBorder = array(
			  'borders' => array(
				'allborders' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			  )
			);
			
			$objExcel->setActiveSheetIndex(0);

            $objExcel->getActiveSheet()->SetCellValue('A2', $kompetisi);
			$objExcel->getActiveSheet()->SetCellValue('C3', ": ".$kontingen);
			$objExcel->getActiveSheet()->SetCellValue('C4', ": ".$cabor);
            $objExcel->getActiveSheet()->SetCellValue('C5', ": ".$jk);

            $startRow = 9;
			$no = 1;
			foreach($atlit as $row) {
				$objExcel->getActiveSheet()->SetCellValue('A'.$startRow, $no);
				$objExcel->getActiveSheet()->SetCellValue('B'.$startRow, nama_atlit($row->id_atlit));
				$objExcel->getActiveSheet()->SetCellValue('C'.$startRow, $this->formatter->getDateMonthFormatUser(info_atlit($row->id_atlit)->tanggal_lahir));

				$objExcel->getActiveSheet()->SetCellValue('D'.$startRow, $row->sekolah);
				$objExcel->getActiveSheet()->SetCellValue('E'.$startRow, $row->kelas);

				$objExcel->getActiveSheet()->getRowDimension($startRow)->setRowHeight(18);
				
				$startRow++;
				$no++;
            }
            
            $objExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$objExcel->getActiveSheet()->getStyle('A9:V'.($startRow-1))->applyFromArray($styleBorder);

			$startRow++;
			$objExcel->getActiveSheet()->SetCellValue('B'.$startRow, 'Semarang,        ');
			$startRow++;
			$objExcel->getActiveSheet()->SetCellValue('B'.$startRow, 'Ketua Keabsahan'.$kompetisi);
			$objExcel->getActiveSheet()->SetCellValue('T'.$startRow, 'Petugas Keabsahan');
			$startRow++;$startRow++;$startRow++;$startRow++;
			$objExcel->getActiveSheet()->SetCellValue('B'.$startRow, '...........................');
			$objExcel->getActiveSheet()->SetCellValue('T'.$startRow, '...........................');
            
			

			unset($styleBorder);
	
			header('Content-type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="KEABSAHAN-'.$kontingen.'-'.$cabor.'-'.$jk.'.xls"');
			$objWriter->save('php://output');
    }

    public function sesudahs($id){
        $kompetisi = nama_kompetisi($id);
        $kontingen = nama_kontingen($this->input->post('kontingen3'));
        $cabor = nama_cabor($this->input->post('cabor3'));
        $jk = nama_jenis($this->input->post('jk3'));

        $kontingen_ = $this->input->post('kontingen3');
        $cabor_ = $this->input->post('cabor3');
        $jk_ = $this->input->post('jk3');

        $atlit = $this->Kompetisi_model->get_validasi_by($id, $kontingen_, $cabor_, $jk_);
        $this->load->library("excel");
        $file = FCPATH.'assets/report/rpt-keabsahan.xls';

			$objExcel = PHPExcel_IOFactory::load($file);
			$objWriter = new PHPExcel_Writer_Excel5($objExcel);

			$styleBorder = array(
			  'borders' => array(
				'allborders' => array(
				  'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			  )
			);
			
			$objExcel->setActiveSheetIndex(0);

            $objExcel->getActiveSheet()->SetCellValue('A2', $kompetisi);
			$objExcel->getActiveSheet()->SetCellValue('C3', ": ".$kontingen);
			$objExcel->getActiveSheet()->SetCellValue('C4', ": ".$cabor);
            $objExcel->getActiveSheet()->SetCellValue('C5', ": ".$jk);

            $startRow = 9;
			$no = 1;
			foreach($atlit as $row) {
				$objExcel->getActiveSheet()->SetCellValue('A'.$startRow, $no);
				$objExcel->getActiveSheet()->SetCellValue('B'.$startRow, nama_atlit($row->id_atlit));
				$objExcel->getActiveSheet()->SetCellValue('C'.$startRow, $this->formatter->getDateMonthFormatUser(info_atlit($row->id_atlit)->tanggal_lahir));

				$objExcel->getActiveSheet()->SetCellValue('D'.$startRow, $row->sekolah);
                $objExcel->getActiveSheet()->SetCellValue('E'.$startRow, $row->kelas);
                $objExcel->getActiveSheet()->SetCellValue('F'.$startRow, ($row->sksa=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('G'.$startRow, ($row->sksc=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('H'.$startRow, ($row->sttba=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('I'.$startRow, ($row->sttbc=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('J'.$startRow, ($row->raporta=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('K'.$startRow, ($row->raportc=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('L'.$startRow, ($row->aktaa=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('M'.$startRow, ($row->aktac=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('N'.$startRow, ($row->skda=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('O'.$startRow, ($row->skdc=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('P'.$startRow, ($row->nisna=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('Q'.$startRow, ($row->nisnc=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('R'.$startRow, ($row->skskba=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('S'.$startRow, ($row->skskbc=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('T'.$startRow, ($row->khba=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('U'.$startRow, ($row->khbc=="1")?"✓":"-");
				$objExcel->getActiveSheet()->SetCellValue('V'.$startRow, ($row->ket=="1")?"SAH":"TDK SAH");

				$objExcel->getActiveSheet()->getRowDimension($startRow)->setRowHeight(18);
				
				$startRow++;
				$no++;
            }
            
            $objExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$objExcel->getActiveSheet()->getStyle('A9:V'.($startRow-1))->applyFromArray($styleBorder);

			$startRow++;
			$objExcel->getActiveSheet()->SetCellValue('B'.$startRow, 'Semarang,        ');
			$startRow++;
			$objExcel->getActiveSheet()->SetCellValue('B'.$startRow, 'Ketua Keabsahan'.$kompetisi);
			$objExcel->getActiveSheet()->SetCellValue('T'.$startRow, 'Petugas Keabsahan');
			$startRow++;$startRow++;$startRow++;$startRow++;
			$objExcel->getActiveSheet()->SetCellValue('B'.$startRow, '...........................');
			$objExcel->getActiveSheet()->SetCellValue('T'.$startRow, '...........................');
            
			

			unset($styleBorder);
	
			header('Content-type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="KEABSAHAN-'.$kontingen.'-'.$cabor.'-'.$jk.'.xls"');
			$objWriter->save('php://output');
    }

    public function validasi($id)
	{
        $data['kontingen'] = $this->Kompetisi_model->get_by_name($id);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $data['atlit']   = array();

        $data['k'] = null;
        $data['c'] = null;
        $data['j'] = null;

        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/validasi',$data);
    }

    public function validasi_($id)
	{
        $kontingen = $this->input->post('kontingen');
        $cabor = $this->input->post('cabor');
        $jk = $this->input->post('jk');
        $data['k'] = $kontingen;
        $data['c'] = $cabor;
        $data['j'] = $jk;

        $data['kontingen'] = $this->Kompetisi_model->get_by_name($id);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $data['atlit']   = $this->Kompetisi_model->get_validasi_by($id, $kontingen, $cabor, $jk);

        
        
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/validasi',$data);
    }

    public function validasi__($id, $kontingen, $cabor, $jk)
	{
        $kontingen = $this->uri->segment('4');
        $cabor = $this->uri->segment('5');
        $jk = $this->uri->segment('6');
        $data['k'] = $kontingen;
        $data['c'] = $cabor;
        $data['j'] = $jk;

        $data['kontingen'] = $this->Kompetisi_model->get_by_name($id);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $data['atlit']   = $this->Kompetisi_model->get_validasi_by($id, $kontingen, $cabor, $jk);

        
        
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/validasi',$data);
    }

    public function listCaborKontingenKompetisi(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $kontingen = $this->input->post('kontingen');
        $kompetisi = $this->input->post('kompetisi');

        $cabors = $this->db->query("SELECT distinct(a.id_cabor) FROM e_kompetisi_keikutsertaan a
        WHERE a.id_kompetisi = '$kompetisi'
        AND a.id_kontingen = '$kontingen'
        AND a.deleted_at IS NULL
        order by a.id_cabor DESC")->result();
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option disabled selected value='all'>Silakan Pilih Cabor</option>";
        
        foreach($cabors as $cbr){
          $lists .= "<option value='".$cbr->id_cabor."'>".nama_cabor($cbr->id_cabor)."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_cabor'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }

    public function listCaborKontingenKompetisi2(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $kontingen = $this->input->post('kontingen2');
        $kompetisi = $this->input->post('kompetisi2');

        $cabors = $this->db->query("SELECT distinct(a.id_cabor) FROM e_kompetisi_keikutsertaan a
        WHERE a.id_kompetisi = '$kompetisi'
        AND a.id_kontingen = '$kontingen'
        AND a.deleted_at IS NULL
        order by a.id_cabor DESC")->result();
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option disabled selected value='all'>Silakan Pilih Cabor</option>";
        
        foreach($cabors as $cbr){
          $lists .= "<option value='".$cbr->id_cabor."'>".nama_cabor($cbr->id_cabor)."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_cabor2'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }

    public function listCaborKontingenKompetisi3(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $kontingen = $this->input->post('kontingen3');
        $kompetisi = $this->input->post('kompetisi3');

        $cabors = $this->db->query("SELECT distinct(a.id_cabor) FROM e_kompetisi_keikutsertaan a
        WHERE a.id_kompetisi = '$kompetisi'
        AND a.id_kontingen = '$kontingen'
        AND a.deleted_at IS NULL
        order by a.id_cabor DESC")->result();
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option disabled selected value='all'>Silakan Pilih Cabor</option>";
        
        foreach($cabors as $cbr){
          $lists .= "<option value='".$cbr->id_cabor."'>".nama_cabor($cbr->id_cabor)."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_cabor3'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }

    public function simpan_validasi() {

            $k = $_POST['k'];
            $c = $_POST['c'];
            $j = $_POST['j'];

            $row = $this->Crud_model->get_by_id('e_kompetisi_by_name_atlit','id_kompetisi_by_name_atlit',  $_POST['k']);

			$id = $_POST['id'];
			$sekolah = $_POST['sekolah'];
			$kelas = $_POST['kelas'];
			$sksa = $_POST['sksa'];
			$sksc = $_POST['sksc'];
			$sttba = $_POST['sttba'];
			$sttbc = $_POST['sttbc'];
			$raporta = $_POST['raporta'];
			$raportc = $_POST['raportc'];
			$aktaa = $_POST['aktaa'];
			$aktac = $_POST['aktac'];
			$skda = $_POST['skda'];
			$skdc = $_POST['skdc'];
			$nisna = $_POST['nisna'];
			$nisnc = $_POST['nisnc'];
			$sksba = $_POST['skskba'];
			$sksbc = $_POST['skskbc'];
			$khba = $_POST['khba'];
			$khbc = $_POST['khbc'];
			$ket = $_POST['ket'];
			$count = count($id);

			for($i = 0; $i < count($id); $i++){
                $update_data[] = array(
                    'id_kompetisi_by_name_atlit' 		=> $id[$i],
                    'sekolah'	=> $sekolah[$i],
                    'kelas' 	=> $kelas[$i],
                    'sksa'      => $sksa[$i],
                    'sksc'      => $sksc[$i],
                    'sttba'     => $sttba[$i],
                    'sttbc'     => $sttbc[$i],
                    'raporta'   => $raporta[$i],
                    'raportc'   => $raportc[$i],
                    'aktaa'     => $aktaa[$i],
                    'aktac'     => $aktac[$i],
                    'skda'      => $skda[$i],
                    'skdc'      => $skdc[$i],
                    'nisna'     => $nisna[$i],
                    'nisnc'     => $nisnc[$i],
                    'skskba'    => $skskba[$i],
                    'skskbc'    => $skskbc[$i],
                    'khba'      => $khba[$i],
                    'khbc'      => $khbc[$i],   
                    'ket'       => $ket[$i]
                );
            }
    
            //echo json_encode($update_data);
            $update = $this->db->update_batch('e_kompetisi_by_name_atlit', $update_data, 'id_kompetisi_by_name_atlit');
            if($update){
                 $this->session->set_flashdata('info', 'success');
                 $this->session->set_flashdata('message', 'Berhasil Simpan Validasi');
                 redirect(site_url('kompetisi/validasi__/'.$row->id_kompetisi.'/'.$k.'/'.$c.'/'.$j));
            }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal Simpan Validasi');
                redirect(site_url('kompetisi/validasi__/'.$row->id_kompetisi.'/'.$k.'/'.$c.'/'.$j));
            }
            
		
    }
    
    public function cetak($id)
	{
        $data['kontingen'] = $this->Crud_model->get_all_by('e_kompetisi_kontingen','id_kompetisi', $id);
        $data['cabor'] = $this->Crud_model->get_all_by('e_kompetisi_cabor','id_kompetisi', $id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/cetak', $data);
    }


    public function cetak_keikutsertaan($id)
	{   
        $kontingen = $this->input->post('kontingen');
        $data['kontingen'] = $kontingen;
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $data['alldata'] = $this->Crud_model->get_all_by_2_where('e_kompetisi_keikutsertaan', 'id_kompetisi', $id, 'id_kontingen', $kontingen);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/cetak_keikutsertaan', $data);
    }

    public function cetak_by_number($id)
	{   
        $kontingen = $this->input->post('id_kontingen');
        //echo $kontingen ;
        $data['kontingen'] = $kontingen;
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $data['alldata'] = $this->Crud_model->get_all_by_2_where('e_kompetisi_by_number', 'id_kompetisi', $id, 'id_kontingen', $kontingen);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/cetak_by_number', $data);
    }


    public function cetak_by_name($id)
	{   
        $kontingen = $this->input->post('id_kontingen');
        $jenis = $this->input->post('jenis');
        //echo $kontingen ;
        $data['kontingen'] = $kontingen;
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        if($jenis == 'Atlit'){
            $data['alldata'] = $this->Crud_model->get_all_by_2_where('e_kompetisi_by_name_atlit', 'id_kompetisi', $id, 'id_kontingen', $kontingen);
        }else{
            $data['alldata'] = $this->Crud_model->get_all_by_2_where('e_kompetisi_by_name_pelatih', 'id_kompetisi', $id, 'id_kontingen', $kontingen);   
        }
        $data['jenis'] = $jenis;
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/cetak_by_name', $data);
    }

    public function cetak_atlit_per_cabor($id)
	{   
        $cabor = $this->input->post('cabor');
        $jenis = $this->input->post('jenis');
        //echo $kontingen ;
        $data['cabor'] = $cabor;
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        if($jenis == 'Atlit'){
            $data['alldata'] = $this->Crud_model->get_all_by_2_where('e_kompetisi_by_name_atlit', 'id_kompetisi', $id, 'id_cabor', $cabor);
        }else{
            $data['alldata'] = $this->Crud_model->get_all_by_2_where('e_kompetisi_by_name_pelatih', 'id_kompetisi', $id, 'id_cabor', $cabor);   
        }
        $data['jenis'] = $jenis;
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/cetak_atlit_per_cabor', $data);
    }
    

    public function medali($id)
	{
        $data['kontingens'] = $this->Crud_model->get_all_by('e_kompetisi_kontingen','id_kompetisi', $id);
        $data['kontingen'] = $id;
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $data['alldata'] = $this->Crud_model->get_all_by('e_kompetisi_kelas','id_kompetisi', $id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/medali', $data);
    }

    public function adds_medali_by_kelas(){
        
        $bulan = date('m',strtotime(date('Y-m-d H:i:s')));
        $tahun = date('Y',strtotime(date('Y-m-d H:i:s')));

        $nomor = $bulan.'/'.$tahun;
        $data = array(
            'id_kompetisi' => $this->input->post('id_kompetisi'),
            'id_kontingen' => $this->input->post('id_kontingen'),
            'id_cabor' => $this->input->post('id_cabor'),
            'id_kelas' => $this->input->post('id_kelas'),
            'id_atlit' => $this->input->post('id_atlit'),
            'id_medali' => $this->input->post('medali'),
            'created_at' => date('Y-m-d H:i:s'),
            'nomor_medali' =>  $nomor
        );
        $insert = $this->Crud_model->insert('e_kompetisi_medali', $data);
        if ($insert) {
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Menambah');
            redirect(site_url('kompetisi/medali/'.$this->input->post('id_kompetisi') ));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal Menambah');
            redirect(site_url('kompetisi/medali/'.$this->input->post('id_kompetisi') ));
        }
    }

    public function cetak_medali($id)
	{
        
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $data['alldata'] = $this->Crud_model->get_all_by('e_kompetisi_medali','id_kompetisi', $id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/cetak_medali', $data);
    }

    public function cetak_medali_kontingen($id)
	{
        $query = $this->db->query("SELECT DISTINCT(id_medali), id_cabor, id_kelas, id_kontingen FROM e_kompetisi_medali WHERE id_kompetisi = '$id' ORDER BY id_cabor, id_kelas, id_kontingen, id_medali ASC")->result();
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $data['alldata'] = $query;
        // $data['alldata'] = $this->Crud_model->get_all_by_2_where('e_kompetisi_medali', 'id_kompetisi', $id, 'id_kontingen', $kontingen);
        
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi/cetak_medali_kontingen', $data);
    }

    public function cetak_kartu_peserta($id)
	{   
        $kontingen = $this->input->post('id_kontingen');
        $jenis = $this->input->post('jenis');
        //echo $kontingen ;
        $data['kontingen'] = $kontingen;
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        if($jenis == 'Atlit'){
            // $data['alldata'] = $this->Crud_model->get_all_by_2_where('e_kompetisi_by_name_atlit', 'id_kompetisi', $id, 'id_kontingen', $kontingen);
            $data['alldata'] = $this->Kompetisi_model->get_atlit_by($id, $kontingen);
        }else{
            //$data['alldata'] = $this->Crud_model->get_all_by_2_where('e_kompetisi_by_name_pelatih', 'id_kompetisi', $id, 'id_kontingen', $kontingen);
            $data['alldata'] = $this->Kompetisi_model->get_pelatih_by($id, $kontingen);   
        }
        $data['jenis'] = $jenis;
        if($jenis == 'Atlit'){
        $this->load->view('backend/kompetisi/cetak_kartu_peserta_multi', $data);
        }elseif($jenis == 'Pelatih'){
        $this->load->view('backend/kompetisi/cetak_kartu_peserta_multi_pelatih', $data);
        }else{
            redirect('cetak/'.$this->uri->segment(3));
        }
    }

    public function cetak_piagam($id)
	{   
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $data['alldata'] = $this->Crud_model->get_all_by('e_kompetisi_medali','id_kompetisi', $id);
        $this->load->view('backend/kompetisi/cetak_piagam', $data);
    }


}