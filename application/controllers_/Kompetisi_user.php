<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kompetisi_user extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Kompetisi_user_model');
        $this->kontingen = $this->session->userdata('kontingen');
    }

    public function index()
	{
        $data['alldata'] = $this->Kompetisi_user_model->get_all($this->kontingen);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi_user/index',$data);
    }

    // ini crud keuikutsertaan peserta
    public function keikutsertaan($id)
	{
        $data['allcabor'] = $this->Crud_model->get_all_by('e_kompetisi_cabor', 'id_kompetisi', $id);
        $data['alldata'] = $this->Kompetisi_user_model->get_keikutsertaan($id, $this->kontingen);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi_user/keikutsertaan',$data);
    }

    public function add_keikutsertaan($id)
	{
        $data['allcabor'] = $this->Crud_model->get_all_by('e_kompetisi_cabor', 'id_kompetisi', $id);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi_user/add_keikutsertaan',$data);
    }

    public function adds_keikutsertaan(){
			$data = array(
                'id_kompetisi' => $this->input->post('id_kompetisi'),
                'id_kontingen' => $this->session->userdata('kontingen'),
                'id_cabor' => $this->input->post('cabor'),
                'atlit_pa' => $this->input->post('atlit_pa'),
                'atlit_pi' => $this->input->post('atlit_pi'),
                'pelatih_pa' => $this->input->post('pelatih_pa'),
                'pelatih_pi' => $this->input->post('pelatih_pi'),
				'created_at' => date('Y-m-d H:i:s'),
			);
			$insert = $this->Crud_model->insert('e_kompetisi_keikutsertaan', $data);
			if ($insert) {
                $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Menambah');
				redirect(site_url('kompetisi_user/keikutsertaan/'.$this->input->post('id_kompetisi')));
			} else {
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Menambah');
				redirect(site_url('kompetisi_user/keikutsertaan/'.$this->input->post('id_kompetisi')));
			}
    }

    public function edit_keikutsertaan(){
        $id = $this->input->post('id');
        $row = $this->Crud_model->get_by_id('e_kompetisi_keikutsertaan', 'id_kompetisi_keikutsertaan', $id);
        $data = array(
        
            'atlit_pa' => $this->input->post('atlit_pa'),
            'atlit_pi' => $this->input->post('atlit_pi'),
            'pelatih_pa' => $this->input->post('pelatih_pa'),
            'pelatih_pi' => $this->input->post('pelatih_pi')
        );

        // echo json_encode($data);
        $up = $this->Crud_model->update('e_kompetisi_keikutsertaan', 'id_kompetisi_keikutsertaan', $id, $data);
        if($up != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil Ubah');
				redirect(site_url('kompetisi_user/keikutsertaan/'.$row->id_kompetisi));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Ubah');
				redirect(site_url('kompetisi_user/keikutsertaan/'.$row->id_kompetisi));
        }
    }

    public function hapus_keikutsertaan($id){
        $row = $this->Crud_model->get_by_id('e_kompetisi_keikutsertaan', 'id_kompetisi_keikutsertaan', $id);
        $del = $this->Crud_model->is_delete('e_kompetisi_keikutsertaan', 'id_kompetisi_keikutsertaan', $id);
        if($del != false){
             $this->session->set_flashdata('info', 'success');
				$this->session->set_flashdata('message', 'Berhasil hapus');
				redirect(site_url('kompetisi_user/keikutsertaan/'.$row->id_kompetisi));
        }else{
                $this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal hapus');
				redirect(site_url('kompetisi_user/keikutsertaan/'.$row->id_kompetisi));
        }
    }

    // ini crud keuikutsertaan peserta
    public function add_keikutsertaan_detail($id)
	{
        $x = $this->kontingen;
        $row = $this->Crud_model->get_by_id('e_kompetisi_keikutsertaan','id_kompetisi_keikutsertaan', $id);
        $data['rowdata'] = $row;
        $data['rowdata2'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $row->id_kompetisi);
        $data['atlit_pa'] = $this->Crud_model->get_all_by_2_where('e_atlit','id_kontingen', $x, 'jk', 'Pa');
        $data['atlit_pi'] = $this->Crud_model->get_all_by_2_where('e_atlit','id_kontingen', $x, 'jk', 'Pi');
        $data['pelatih_pa'] = $this->Crud_model->get_all_by_2_where('e_pelatih','id_kontingen', $x, 'jk', 'Pa');
        $data['pelatih_pi'] = $this->Crud_model->get_all_by_2_where('e_pelatih','id_kontingen', $x, 'jk', 'Pi');
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi_user/add_keikutsertaan_detail',$data);
    }

    public function adds_keikutsertaan_detail(){

        $id_kompetisi_keikutsertaan = $this->input->post('id_kompetisi_keikutsertaan');
        $id_kompetisi = $this->input->post('id_kompetisi');
        $id_kontingen = $this->input->post('id_kontingen');
        $id_cabor = $this->input->post('id_cabor');
        $atlit_pa = $this->input->post('atlit_pa');
        $atlit_pi = $this->input->post('atlit_pi');
        $pelatih_pa = $this->input->post('pelatih_pa');
        $pelatih_pi = $this->input->post('pelatih_pi');

        for($apa = 0; $apa < count($atlit_pa); $apa++){
            $insert_apa[] = array(
                'id_kompetisi_keikutsertaan' => $id_kompetisi_keikutsertaan,
                'id_kompetisi' 	=> $id_kompetisi,
                'id_kontingen'	=> $id_kontingen,
                'id_cabor'      => $id_cabor,
                'id_atlit'      => $atlit_pa[$apa], 
                'jk'            => 'Pa',
                'created_at' => date('Y-m-d H:i:s'),
            );
        }
        $atlitpa = $this->db->insert_batch('e_kompetisi_keikutsertaan_atlit', $insert_apa);

        for($api = 0; $api < count($atlit_pi); $api++){
            $insert_api[] = array(
                'id_kompetisi_keikutsertaan' => $id_kompetisi_keikutsertaan,
                'id_kompetisi' 	=> $id_kompetisi,
                'id_kontingen'	=> $id_kontingen,
                'id_cabor'      => $id_cabor,
                'id_atlit'      => $atlit_pi[$api],
                'jk'            => 'Pi',
                'created_at' => date('Y-m-d H:i:s'),
            );
        }
        $atlitpi = $this->db->insert_batch('e_kompetisi_keikutsertaan_atlit', $insert_api);

        for($ppa = 0; $ppa < count($pelatih_pa); $ppa++){
            $insert_ppa[] = array(
                'id_kompetisi_keikutsertaan' => $id_kompetisi_keikutsertaan,
                'id_kompetisi' 	=> $id_kompetisi,
                'id_kontingen'	=> $id_kontingen,
                'id_cabor'      => $id_cabor,
                'id_pelatih'      => $pelatih_pa[$ppa],
                'jk'            => 'Pa',
                'created_at' => date('Y-m-d H:i:s'),
            );
        }
        $pelatihpa = $this->db->insert_batch('e_kompetisi_keikutsertaan_pelatih', $insert_ppa);

        for($ppi = 0; $ppi < count($pelatih_pi); $ppi++){
            $insert_ppi[] = array(
                'id_kompetisi_keikutsertaan' => $id_kompetisi_keikutsertaan,
                'id_kompetisi' 	=> $id_kompetisi,
                'id_kontingen'	=> $id_kontingen,
                'id_cabor'      => $id_cabor,
                'id_pelatih'     => $pelatih_pi[$ppi],
                'jk'            => 'Pi',
                'created_at' => date('Y-m-d H:i:s'),
            );
        }
        $pelatihpi = $this->db->insert_batch('e_kompetisi_keikutsertaan_pelatih', $insert_ppi);

        $row = $this->Crud_model->get_by_id('e_kompetisi_keikutsertaan', 'id_kompetisi', $id);
        $data = array(
            'generate_by_kontingen' => '1'
        );
        $up = $this->Crud_model->update('e_kompetisi_keikutsertaan', 'id_kompetisi_keikutsertaan', $id_kompetisi_keikutsertaan, $data);

        // if ($atlitpa && $atlitpi && $pelatihpa && $pelatihpi) {
        if ($up) {
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Menambah');
            redirect(site_url('kompetisi_user/keikutsertaan/'.$id_kompetisi));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal Menambah');
            redirect(site_url('kompetisi_user/keikutsertaan/'.$id_kompetisi));
        }
    }

    // ini crud keuikutsertaan peserta
    // public function update_keikutsertaan_detail($id)
	// {
    //     $x = $this->kontingen;
    //     $row = $this->Crud_model->get_by_id('e_kompetisi_keikutsertaan','id_kompetisi_keikutsertaan', $id);
    //     $data['rowdata'] = $row;
    //     $data['rowdata2'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $row->id_kompetisi);
    //     $data['atlit_pa_'] = $this->Crud_model->get_all_by_3_where('e_kompetisi_keikutsertaan_atlit','id_kontingen', $x, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pa');
    //     $data['atlit_pa'] = $this->Crud_model->get_all_by_2_where('e_atlit','id_kontingen', $x, 'jk', 'Pa');
    //     $data['atlit_pi_'] = $this->Crud_model->get_all_by_3_where('e_kompetisi_keikutsertaan_atlit','id_kontingen', $x, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pi');
    //     $data['atlit_pi'] = $this->Crud_model->get_all_by_2_where('e_atlit','id_kontingen', $x, 'jk', 'Pi');
    //     $data['pelatih_pa_'] = $this->Crud_model->get_all_by_3_where('e_kompetisi_keikutsertaan_pelatih','id_kontingen', $x, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pa' );
    //     $data['pelatih_pa'] = $this->Crud_model->get_all_by_2_where('e_pelatih','id_kontingen', $x, 'jk', 'Pa');
    //     $data['pelatih_pi_'] = $this->Crud_model->get_all_by_3_where('e_kompetisi_keikutsertaan_pelatih','id_kontingen', $x, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pi');
    //     $data['pelatih_pi'] = $this->Crud_model->get_all_by_2_where('e_pelatih','id_kontingen', $x, 'jk', 'Pi');
    //     $this->load->view('backend/template/head');
    //     $this->load->view('backend/template/header');
    //     $this->load->view('backend/template/sidebar');
    //     $this->load->view('backend/kompetisi_user/update_keikutsertaan_detail',$data);
    // }

    public function update_keikutsertaan_detail($id)
	{
        $x = $this->kontingen;
        $row = $this->Crud_model->get_by_id('e_kompetisi_keikutsertaan','id_kompetisi_keikutsertaan', $id);
        $data['rowdata'] = $row;
        $data['rowdata2'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $row->id_kompetisi);
        $data['atlit_pa_'] = $this->Crud_model->get_all_by_4_where('e_kompetisi_keikutsertaan_atlit','id_kontingen', $x, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pa','id_kompetisi_keikutsertaan', $id);
        $data['atlit_pa'] = $this->Crud_model->get_all_by_2_where('e_atlit','id_kontingen', $x, 'jk', 'Pa');
        $data['atlit_pi_'] = $this->Crud_model->get_all_by_4_where('e_kompetisi_keikutsertaan_atlit','id_kontingen', $x, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pi' ,'id_kompetisi_keikutsertaan', $id);
        $data['atlit_pi'] = $this->Crud_model->get_all_by_2_where('e_atlit','id_kontingen', $x, 'jk', 'Pi');
        $data['pelatih_pa_'] = $this->Crud_model->get_all_by_4_where('e_kompetisi_keikutsertaan_pelatih','id_kontingen', $x, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pa' ,'id_kompetisi_keikutsertaan', $id);
        $data['pelatih_pa'] = $this->Crud_model->get_all_by_2_where('e_pelatih','id_kontingen', $x, 'jk', 'Pa');
        $data['pelatih_pi_'] = $this->Crud_model->get_all_by_4_where('e_kompetisi_keikutsertaan_pelatih','id_kontingen', $x, 'id_kompetisi', $row->id_kompetisi, 'jk', 'Pi' ,'id_kompetisi_keikutsertaan', $id);
        $data['pelatih_pi'] = $this->Crud_model->get_all_by_2_where('e_pelatih','id_kontingen', $x, 'jk', 'Pi');
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi_user/update_keikutsertaan_detail',$data);
    }

    public function edit_keikutsertaan_detail(){
        $id_kompetisi_keikutsertaan = $this->input->post('id_kompetisi_keikutsertaan');
        $id_kompetisi = $this->input->post('id_kompetisi');
        $id_kontingen = $this->input->post('id_kontingen');
        $atlit_pa = $this->input->post('atlit_pa');
        $atlit_pi = $this->input->post('atlit_pi');
        $pelatih_pa = $this->input->post('pelatih_pa');
        $pelatih_pi = $this->input->post('pelatih_pi');
        for($apa = 0; $apa < count($this->input->post('id_pa')); $apa++){
            $insert_apa[] = array(
                'id_kompetisi_keikutsertaan_detail' => $this->input->post('id_pa')[$apa],
                'id_kompetisi_keikutsertaan' => $id_kompetisi_keikutsertaan,
                'id_atlit'      => $atlit_pa[$apa]
            );
        }
        $atlitpa = $this->db->update_batch('e_kompetisi_keikutsertaan_atlit', $insert_apa,'id_kompetisi_keikutsertaan_detail');

        for($api = 0; $api < count($this->input->post('id_pi')); $api++){
            $insert_api[] = array(
                'id_kompetisi_keikutsertaan_detail' => $this->input->post('id_pi')[$api],
                'id_kompetisi_keikutsertaan' => $id_kompetisi_keikutsertaan,
                'id_atlit'      => $atlit_pi[$api]
            );
        }
        $atlitpi = $this->db->update_batch('e_kompetisi_keikutsertaan_atlit', $insert_api,'id_kompetisi_keikutsertaan_detail');

        for($ppa = 0; $ppa < count($this->input->post('id_ppa')); $ppa++){
            $insert_ppa[] = array(
                'id_kompetisi_keikutsertaan_detail' => $this->input->post('id_ppa')[$ppa],
                'id_kompetisi_keikutsertaan' => $id_kompetisi_keikutsertaan,
                'id_pelatih'      => $pelatih_pa[$ppa]
            );
        }
        $pelatihpa = $this->db->update_batch('e_kompetisi_keikutsertaan_pelatih', $insert_ppa,'id_kompetisi_keikutsertaan_detail');

        for($ppi = 0; $ppi < count($this->input->post('id_ppi')); $ppi++){
            $insert_ppi[] = array(
                'id_kompetisi_keikutsertaan_detail' => $this->input->post('id_ppi')[$ppi],
                'id_kompetisi_keikutsertaan' => $id_kompetisi_keikutsertaan,
                'id_pelatih'     => $pelatih_pi[$ppi]
            );
        }
        $pelatihpi = $this->db->update_batch('e_kompetisi_keikutsertaan_pelatih', $insert_ppi, 'id_kompetisi_keikutsertaan_detail');

        $row = $this->Crud_model->get_by_id('e_kompetisi_keikutsertaan', 'id_kompetisi', $id_kompetisi);
        if ($row) {
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Menambah');
            redirect(site_url('kompetisi_user/update_keikutsertaan_detail/'.$id_kompetisi));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal Menambah');
            redirect(site_url('kompetisi_user/update_keikutsertaan_detail/'.$id_kompetisi));
        }
    }

    // ini crud keuikutsertaan peserta
    public function by_number($id)
	{
        $row = $this->Kompetisi_user_model->get_by_number($id, $this->kontingen);
        $data['alldata'] = $this->Kompetisi_user_model->get_by_number($id, $this->kontingen);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        // $data['kelas'] = $this->Kompetisi_user_model->get_kelas_by();
        // $kel = array();
        // foreach($row as $xx){
        //     for($i = 0 ; $i > count($xx->id_cabor); $i ++){
        //         $kelas = $this->db->query("SELECT * FROM e_kelas WHERE id_cabor = '$xx->id_cabor' AND deleted_at IS NULL")->result();
        //     }
        // }
     
        // $data['kelas'] = $kelas;

        // echo json_encode($data);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi_user/by_number',$data);
    }

    public function listKelas(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $id_cabor = $this->input->post('id_cabor');
        
        $kelas = $this->db->query("SELECT * FROM e_kelas WHERE id_cabor = '$id_cabor' AND deleted_at IS NULL")->result();
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option disabled selected value=''>Silakan Pilih Kelas</option>";
        
        foreach($kelas as $kel){
          $lists .= "<option value='".$kel->id_kelas."'>".$kel->nama_kelas.' '.$kel->jenis_kelas."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_kelas'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }

    public function listKelasKompetisi(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $id_cabor = $this->input->post('id_cabor');
        $id_kompetisi = $this->input->post('id_kompetisi');
        
        $kelas = $this->db->query("SELECT * FROM e_kompetisi_kelas WHERE id_kompetisi = '$id_kompetisi' AND id_cabor = '$id_cabor' AND deleted_at IS NULL")->result();
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option disabled selected value=''>Silakan Pilih Kelas</option>";
        
        foreach($kelas as $kel){
          $lists .= "<option value='".$kel->id_kelas."'>".nama_kelas($kel->id_kelas).' '.jenis_kelas($kel->id_kelas)."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_kelas'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }
    
        public function adds_by_number(){
            $data = array(
                'id_kompetisi' => $this->input->post('id_kompetisi'),
                'id_kontingen' => $this->input->post('id_kontingen'),
                'id_cabor' => $this->input->post('id_cabor'),
                'id_kelas' => $this->input->post('id_kelas'),
                'created_at' => date('Y-m-d H:i:s'),
            );
            $insert = $this->Crud_model->insert('e_kompetisi_by_number', $data);
            if ($insert) {
                $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Berhasil Menambah');
                redirect(site_url('kompetisi_user/by_number/'.$this->input->post('id_kompetisi')));
            } else {
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal Menambah');
                redirect(site_url('kompetisi_user/by_number/'.$this->input->post('id_kompetisi')));
            }
    }

    public function hapus_by_number($id){
        $row = $this->Crud_model->get_by_id('e_kompetisi_by_number', 'id_kompetisi_by_number', $id);
        $del = $this->Crud_model->is_delete('e_kompetisi_by_number', 'id_kompetisi_by_number', $id);
        if($del != false){
            $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Berhasil hapus');
                redirect(site_url('kompetisi_user/by_number/'.$row->id_kompetisi));
        }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal hapus');
                redirect(site_url('kompetisi_user/by_number/'.$row->id_kompetisi));
        }
    }

    // ini crud keuikutsertaan peserta
    public function by_name($id)
	{
        $row = $this->Kompetisi_user_model->get_by_number($id, $this->kontingen);
        $data['alldata'] = $this->Kompetisi_user_model->get_by_name($id, $this->kontingen);
        $data['rowdata'] = $this->Crud_model->get_by_id('e_kompetisi','id_kompetisi', $id);
        $data['ikut'] = $this->Kompetisi_user_model->get_keikutsertaan($id, $this->kontingen);
        // $data['kelas'] = $this->Kompetisi_user_model->get_kelas_by();
        // $kel = array();
        // foreach($row as $xx){
        //     for($i = 0 ; $i > count($xx->id_cabor); $i ++){
        //         $kelas = $this->db->query("SELECT * FROM e_kelas WHERE id_cabor = '$xx->id_cabor' AND deleted_at IS NULL")->result();
        //     }
        // }
     
        // $data['kelas'] = $kelas;

        // echo json_encode($data);
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kompetisi_user/by_name',$data);
    }

    public function listAtlit(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $id_kelas = $this->input->post('id_kelas');
        $id_kontingen = $this->input->post('id_kontingen');
        
        $kelas = $this->db->query("SELECT * FROM e_kelas WHERE id_kelas = '$id_kelas' AND deleted_at IS NULL")->row();
        $atlits = $this->db->query("SELECT * FROM e_atlit WHERE jk = '$kelas->jenis_kelas' AND id_kontingen = '$id_kontingen' AND deleted_at IS NULL AND ijazah IS NOT NULL AND akte IS NOT NULL AND raport IS NOT NULL")->result();
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option disabled selected value=''>Silakan Pilih Atlit</option>";
        
        foreach($atlits as $atls){
          $lists .= "<option value='".$atls->id_atlit."'>".$atls->nama_atlit."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_atlit'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }

    public function adds_atlit_by_name(){
        $row = $this->Crud_model->get_by_id('e_atlit', 'id_atlit', $this->input->post('id_atlit'));
        $data = array(
            'id_kompetisi' => $this->input->post('id_kompetisi'),
            'id_kontingen' => $this->input->post('id_kontingen'),
            'id_cabor' => $this->input->post('id_cabor'),
            'id_kelas' => $this->input->post('id_kelas'),
            'id_atlit' => $this->input->post('id_atlit'),
            'created_at' => date('Y-m-d H:i:s'),
            'sekolah' => $row->sekolah
        );
        $insert = $this->Crud_model->insert('e_kompetisi_by_name_atlit', $data);
        if ($insert) {
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Menambah');
            redirect(site_url('kompetisi_user/by_name/'.$this->input->post('id_kompetisi')));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal Menambah');
            redirect(site_url('kompetisi_user/by_name/'.$this->input->post('id_kompetisi')));
        }
    }

    public function hapus_atlit_by_name($id){
        $row = $this->Crud_model->get_by_id('e_kompetisi_by_name_atlit', 'id_kompetisi_by_name_atlit', $id);
        $del = $this->Crud_model->is_delete('e_kompetisi_by_name_atlit', 'id_kompetisi_by_name_atlit', $id);
        if($del != false){
            $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Berhasil hapus');
                redirect(site_url('kompetisi_user/by_name/'.$row->id_kompetisi));
        }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal hapus');
                redirect(site_url('kompetisi_user/by_name/'.$row->id_kompetisi));
        }
    }

    public function listPelatih(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $id_kelas = $this->input->post('id_kelas_p');
        $id_kontingen = $this->input->post('id_kontingen_p');
        
        $kelas = $this->db->query("SELECT * FROM e_kelas WHERE id_kelas = '$id_kelas' AND deleted_at IS NULL")->row();
        $pelatihs = $this->db->query("SELECT * FROM e_pelatih WHERE jk = '$kelas->jenis_kelas' AND id_kontingen = '$id_kontingen' AND deleted_at IS NULL")->result();
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        $lists = "<option disabled selected value=''>Silakan Pilih Pelatih</option>";
        
        foreach($pelatihs as $plts){
          $lists .= "<option value='".$plts->id_pelatih."'>".$plts->nama_pelatih."</option>"; // Tambahkan tag option ke variabel $lists
        }
        
        $callback = array('list_pelatih'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }

    public function adds_pelatih_by_name(){
        $data = array(
            'id_kompetisi' => $this->input->post('id_kompetisi_p'),
            'id_kontingen' => $this->input->post('id_kontingen_p'),
            'id_cabor' => $this->input->post('id_cabor_p'),
            'id_kelas' => $this->input->post('id_kelas_p'),
            'id_pelatih' => $this->input->post('id_pelatih'),
            'created_at' => date('Y-m-d H:i:s'),
        );
        $insert = $this->Crud_model->insert('e_kompetisi_by_name_pelatih', $data);
        if ($insert) {
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('message', 'Berhasil Menambah');
            redirect(site_url('kompetisi_user/by_name/'.$this->input->post('id_kompetisi_p')));
        } else {
            $this->session->set_flashdata('info', 'danger');
            $this->session->set_flashdata('message', 'Gagal Menambah');
            redirect(site_url('kompetisi_user/by_name/'.$this->input->post('id_kompetisi_p')));
        }
    }

    public function hapus_pelatih_by_name($id){
        $row = $this->Crud_model->get_by_id('e_kompetisi_by_name_pelatih', 'id_kompetisi_by_name_pelatih', $id);
        $del = $this->Crud_model->is_delete('e_kompetisi_by_name_pelatih', 'id_kompetisi_by_name_pelatih', $id);
        if($del != false){
            $this->session->set_flashdata('info', 'success');
                $this->session->set_flashdata('message', 'Berhasil hapus');
                redirect(site_url('kompetisi_user/by_name/'.$row->id_kompetisi));
        }else{
                $this->session->set_flashdata('info', 'danger');
                $this->session->set_flashdata('message', 'Gagal hapus');
                redirect(site_url('kompetisi_user/by_name/'.$row->id_kompetisi));
        }
    }

    public function infoAtlit(){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $id_atlit = $this->input->post('id_atlits');
        
        $atlit = $this->db->query("SELECT * FROM e_atlit WHERE id_atlit = '$id_atlit' AND deleted_at IS NULL")->row();
        // Buat variabel untuk menampung tag-tag option nya
        // Set defaultnya dengan tag option Pilih
        
        $callback = array(
                           'nama_pelatih' => $atlit->nama_atlit,
                           'nik'          => $atlit->nik,
                           'nisn'         => $atlit->nisn
    
        ); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }

    public function gantiPass(){
        $this->load->view('backend/template/head');
        $this->load->view('backend/template/header');
        $this->load->view('backend/template/sidebar');
        $this->load->view('backend/kelolauser/ganti');
    }

    public function gantiPass_act(){
        $id = $this->input->post('user_id');
        $password = $this->input->post('password');
        $options = [
			'cost' => 11
		];
        $hash_password = password_hash($password, PASSWORD_BCRYPT, $options);

        if($password == ''){
            redirect('dashboard');
        }else{
            $data = array(
           
                    'password_' => $hash_password
            );
            $up = $this->Crud_model->update('e_user', 'id_user', $id, $data);
            if($up != false){
                    $this->session->set_flashdata('info', 'success');
                    $this->session->set_flashdata('message', 'Berhasil Rubah Password');
                    redirect(site_url('dashboard'));
            }else{
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('message', 'Gagal Rubah Password');
                    redirect(site_url('dashboard'));
            }
        }
    }
    

}