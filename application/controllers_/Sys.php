<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sys extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('User_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['kompetisi'] = $this->Crud_model->get_all_by('e_kompetisi','status','ON');
		$data['banner'] = $this->Crud_model->get_all_by('e_banner','status','ON');
		$data['informasi'] = $this->Crud_model->get_all_by_limit('e_informasi','status','ON');
		$this->load->view('frontend/template/head',$data);
		$this->load->view('frontend/template/header');
		$this->load->view('frontend/home');
		$this->load->view('frontend/template/footer');
	}

	public function informasi()
	{
		$data['kompetisi'] = $this->Crud_model->get_all_by('e_kompetisi','status','ON');
		$data['informasi'] = $this->Crud_model->get_all_by('e_informasi','status','ON');
		$data['tgl_berita']=$this->User_model->getListTanggalBeritaFO();

		$this->load->view('frontend/template/head',$data);
		$this->load->view('frontend/template/header');
		$this->load->view('frontend/berita');
		$this->load->view('frontend/template/footer');
	}

	public function read_berita($id)
	{
		$data['kompetisi'] = $this->Crud_model->get_all_by('e_kompetisi','status','ON');
		$data['informasi'] = $this->Crud_model->get_all_by('e_informasi','status','ON');
		$data['tgl_berita']=$this->User_model->getListTanggalBeritaFO();
		$data['berita'] = $this->Crud_model->get_by_id('e_informasi','id',$id);
		$this->load->view('frontend/template/head',$data);
		$this->load->view('frontend/template/header');
		$this->load->view('frontend/berita_detail');
		$this->load->view('frontend/template/footer');
	}

	public function foto()
	{
		$data['kompetisi'] = $this->Crud_model->get_all_by('e_kompetisi','status','ON');
		$data['informasi'] = $this->Crud_model->get_all_by('e_informasi','status','ON');
		$data['gambar']= $this->Crud_model->get_all_by('e_foto','status','ON');

		$this->load->view('frontend/template/head',$data);
		$this->load->view('frontend/template/header');
		$this->load->view('frontend/foto');

	}

	public function login()
	{
		$this->load->helper('url');
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < 5; $i++) {
				$index = rand(0, strlen($characters) - 1);
				$randomString .= $characters[$index];
		}
		$data['rand'] = $randomString; 
		$this->load->helper('url');
		$this->load->view('frontend/login_user', $data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(site_url('sys'));
	}

	public function act_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if(isset($_SESSION['logged_in'])) {
			redirect('dashboard');
		} else {
			$cap 		= $this->input->post('verify');
			$sesscap 	= $this->input->post('verify2');
			if( $cap != $sesscap ){
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Captcha Salah');
				redirect(site_url('sys/login'));
			}else{
			$cek = $this->User_model->cek_login($username, $password);

			if ($cek) {
						$row = $this->User_model->get_by_id('e_user','id_user',$cek->id_user);
						//login sukses
						$session = array(
								'id'  				=> $cek->id_user,
								'username'  		=> $cek->username_,
								'akses'     		=> $cek->akses,
								'kontingen'     	=> $cek->id_kontingen,
								'logged_in' 		=> TRUE
						);
						$this->session->set_userdata($session);
						$this->session->set_userdata($row);
						$this->User_model->update_lastlogin($cek->id);
						redirect('Dashboard');
			}else{
				$this->session->set_flashdata('info', 'danger');
				$this->session->set_flashdata('message', 'Gagal Login');
				redirect(site_url('sys/login'));
				}
			}
		}
	}
}
