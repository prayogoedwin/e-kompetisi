 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <!-- <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li> -->
        </ul>
    </div>
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

  <!-- -------------- Column Center -------------- -->
  <div class="chute chute-center">

                       
                            <!-- -------------- Registration -------------- -->
                            <div class="allcp-form theme-primary tab-pane mw600" id="register" role="tabpanel">
                            <div class="panel">
                                <div class="panel-heading">
                                    <span class="panel-title">
                                      Form Tambah User
                                    </span>
                                </div>
                                <!-- -------------- /Panel Heading -------------- -->

                                 <?php echo form_open_multipart('kelolauser/adds') ?>
                                    <div class="panel-body pn">
                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="nama" name="username" id="username" required class="gui-input"
                                                       placeholder="Username">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-user"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label class="field select prepend-icon">
                                                
                                                <select id="id_kontingen"  required name="id_kontingen" class="js-example-basic-single">
                                                    <option disabled selected>Pilih Kontingen</option>
                                                    <?php
                                                    foreach($alldata as $sel): 
                                                    ?>
                                                    <option value="<?=$sel->id_kontingen?>"><?=$sel->nama_kontingen?></option>
                                                    <?php endforeach; ?>
                                                   
                                                </select>
                                                
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label class="field select prepend-icon">
                                                
                                                <select id="akses" name="akses">
                                                    <option disabled selected>User Akses</option>
                                                    <option value="88">Kontingen</option>
                                                    <option value="99">Admin</option>
                                                   
                                                </select>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->



                                            
                                        <div class="section">
                                            
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-bordered btn-primary">
                                                    Tambah User
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                    </div>
                                    <!-- -------------- /Form -------------- -->
                                    <div class="panel-footer"></div>
                                <?php echo form_close();?>
                            </div>
                            <!-- -------------- /Panel -------------- -->
                        </div>
                        <!-- -------------- /Registration -------------- -->

                        

</div>
<!-- -------------- //End Column Center -------------- -->

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>



</body>
</html>


