<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">

</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

        <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title "> Daftar User</span>
                    </div>
                    <div class="topbar-right  mt5 mr35">
                        <a href="<?=base_url('kelolauser/add')?>" class="btn btn-primary btn-sm ml10" title="New Order">
                            <span class="fa fa-plus pr5"></span><span class="fa fa-file-o pr5"></span>Tambah User</a>

                    </div>

                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <table border="0" style="width:100%" id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">
                                    <th class="text-center">No</th>
                                    <th class="text-center">Username</th>
                                    <th class="text-center">Kontingen</th>
                                    <th class="text-center">Akses</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($alldata == FALSE ){ ?>
                                    <tr class="bg-light">
                                    <th class="text-center" colspan="6">Data Kosong</th>

                                </tr>
                                <?php }else{
                                $no = 0;
                                foreach($alldata as $all):
                                $no++
                                ?>
                                <tr>
                                    <td class=""><?=$no?></td>
                                    <td class=""><?=$all->username_?></td>
                                    <td class=""><?=nama_kontingen($all->id_kontingen)?></td>
                                    <td class=""><?=nama_akses($all->akses)?></td>
                                    <td>
                                    <?php if($all->status == 'ON'){?>
                                        <a href="<?=base_url('kelolauser/rubah/'.$all->id_user)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin non aktifkan user ?')"
                                                    class="btn btn-primary br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                    ON <span class="fa fa-refresh pr5">
                                                    </button>
                                    </a>
                                    <?php }else{?>
                                        <a href="<?=base_url('kelolauser/rubah/'.$all->id_user)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin mengaktifkan user ?')"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                    OFF <span class="fa fa-refresh pr5">
                                                    </button>
                                    </a>

                                    <?php } ?>
                                    </td>
                                    <td class="text-center">

                                    <!-- <a href="<?=base_url('kelolauser/edit')?>"><button type="button"
                                                    class="btn btn-info br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     <span class="fa fa-edit pr5">
                                                    </button>
                                    </a> -->

                                    <a href="<?=base_url('kelolauser/reset/'.$all->id_user)?>"><button type="button"
                                                    class="btn btn-warning br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     Reset Password <span class="fa fa-spinner pr5">
                                                    </button>
                                    </a>

                                    <a href="<?=base_url('kelolauser/hapus/'.$all->id_user)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin menghapus?')"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                     <span class="fa fa-trash pr5">
                                                    </button>
                                    </a>


                                    </td>

                                </tr>
                                <?php endforeach;} ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>



</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>
<?php include(__DIR__ . "/../template/datatable.php"); ?>
<script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "info": false,
      "bLengthChange": false,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "autoWidth": true,
      "scrollX": true,
      "autoWidth": true
    });
});
</script>



</body>
</html>
