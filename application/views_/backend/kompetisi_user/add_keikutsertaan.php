 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <!-- <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li> -->
        </ul>
    </div>
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

  <!-- -------------- Column Center -------------- -->
  <div class="chute chute-center">

                       
                            <!-- -------------- Registration -------------- -->
                            <div class="allcp-form theme-primary tab-pane mw600" id="register" role="tabpanel">
                            <div class="panel">
                                <div class="panel-heading">
                                    <span class="panel-title">
                                      Form Keikutsertaan <?=$rowdata->nama_kompetisi?>
                                    </span>
                                </div>
                                <!-- -------------- /Panel Heading -------------- -->

                                 <?php echo form_open('kompetisi_user/adds_keikutsertaan') ?>
                                    <div class="panel-body pn">
                                    <input type="hidden" name="id_kompetisi" value="<?=$rowdata->id_kompetisi?>">

                                    <div class="section">
                                            <label class="field select prepend-icon">                                              
                                                <select id="cabor"  name="cabor" >
                                                    <option disabled selected>Pilih Cabor</option>
                                                    <?php foreach($allcabor as $cabor): ?>
                                                    <option value="<?=$cabor->id_cabor?>"><?=nama_cabor($cabor->id_cabor)?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->



                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="text" name="atlit_pa" id="atlit_pa" class="gui-input"
                                                       placeholder="Atlit Putra">
                                                <label for="icon" class="field-icon">
                                                    <i class="fa fa-mars"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="text" name="atlit_pi" id="atlit_pi" class="gui-input"
                                                       placeholder="Atlit Putri">
                                                <label for="icon" class="field-icon">
                                                    <i class="fa fa-venus"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->


                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="text" name="pelatih_pa" id="pelatih_pa" class="gui-input"
                                                       placeholder="Pelatih Putra">
                                                <label for="icon" class="field-icon">
                                                    <i class="fa fa-mars"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="text" name="pelatih_pi" id="pelatih_pi" class="gui-input"
                                                       placeholder="Pelatih Putri">
                                                <label for="icon" class="field-icon">
                                                    <i class="fa fa-venus"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->


                                        

                                            
                                        <div class="section">
                                            
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-bordered btn-primary">
                                                    Tambah kompetisi
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                    </div>
                                    <!-- -------------- /Form -------------- -->
                                    <div class="panel-footer"></div>
                                <?php echo form_close();?>
                            </div>
                            <!-- -------------- /Panel -------------- -->
                        </div>
                        <!-- -------------- /Registration -------------- -->

                        

</div>
<!-- -------------- //End Column Center -------------- -->

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>



</body>
</html>


