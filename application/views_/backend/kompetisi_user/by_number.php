<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <!-- <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li>
        </ul>
    </div> -->
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

        <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title "> Daftar Keikutsertaan By Number Pada Kompetisi <?=$rowdata->nama_kompetisi?></span>
                    </div>
                    
                    

                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <table border="0" style="width:100%" id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center" width="5">No</th>
                                    <th class="text-center">Cabor</th>
                                    <th class="text-center">Kelas</th>
                                    
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if($alldata == FALSE ){ ?>
                                    <tr class="bg-light">                                    
                                    <th class="text-center" colspan="9">Data Kosong</th>
                                    
                                </tr>
                                <?php }else{ 
                                $no = 0;
                                foreach($alldata as $all): 
                                $no++;
                                ?>
                                <tr>
                                    <td class=""><?=$no?></td>
                                    <td class=""><?=nama_cabor($all->id_cabor)?></td>
                                    
                                   
                                    
                                    <td class="">

                                    <!-- Button trigger modal -->
                                

                                    <button type="button" class="btn btn-success btn-sm btn-add" data-placement="top" data-toggle="Tambah"  
                                        data-original-title="Tambah" 
                                        data-id="<?=$all->id_kompetisi_keikutsertaan?>"
                                        data-id_kompetisi="<?=$all->id_kompetisi?>"
                                        data-id_kontingen="<?=$all->id_kontingen?>"
                                        data-cabor="<?=nama_cabor($all->id_cabor)?>"
                                        data-id_cabor="<?=$all->id_cabor?>"
                                    ><i class="fa fa-plus"></i></button>

                                    <br/>
                                    <?php
                                    $kelas = $this->db->query("SELECT a.* FROM e_kompetisi_by_number a
                                    WHERE a.id_kompetisi = '$all->id_kompetisi'
                                    AND a.id_kontingen = '$all->id_kontingen' 
                                    AND a.id_cabor = '$all->id_cabor'
                                    AND a.deleted_at IS NULL")->result();
                                    foreach($kelas AS $kls):
                                    ?>

                                    <p><?=nama_kelas($kls->id_kelas)?> <?=jenis_kelas($kls->id_kelas)?>
                                    
                                    <a href="<?=base_url('kompetisi_user/hapus_by_number/'.$kls->id_kompetisi_by_number)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin menghapus?')"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                     <span class="fa fa-trash pr5">
                                                    </button>
                                    </a>
                                    </p>

                                    <?php endforeach; ?>


                                   

                                    

                                    
                                    </td>
                                    
                                </tr>
                                <?php endforeach;} ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>
<?php include(__DIR__ . "/../template/datatable.php"); ?>
<script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "info": false,   
      "bLengthChange": false,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "autoWidth": true,
      "scrollX": true,
      "autoWidth": true
    });
});
</script>

<!-- Modal Edit -->
<?=form_open_multipart('kompetisi_user/adds_by_number', 'method="POST" autocomplete="off" class="form-horizontal form-label-left" role="form" enctype="multipart/form-data"'); ?>
        <div class="modal fade" id="modal-add">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Tambah Keikutsertaan By Number
              </div>
              <div class="modal-body">
                        <input type="hidden" class="form-control" name="id" id="id">
                        <input type="hidden" class="form-control" name="id_kompetisi" id="id_kompetisi">
                        <input type="hidden" class="form-control" name="id_kontingen" id="id_kontingen">
                        <input type="hidden" class="form-control" name="id_cabor" id="id_cabor">
                        

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Cabor</label>
                                 <div class="col-sm-10">
                                 <input type="text" class="form-control" name="cabor" id="cabor" readonly>
                                </div>
                        </div>

                        <div class="form-group">
                                    
                                    
                                    </div>
                         
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                            <div class="col-sm-10">                
                            <select required class="form-control select2" id="kelas" name="id_kelas" style="width: 100%;">
                                    <option>Pilih Kelas</option>
                            <select>
                            </div>
                        </div>



                        
                         


              </div>
              <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah <i class="fa fa-send"></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <?=form_close(); ?>
        <!-- End Modal Edit -->

    <script>
    $(document).ready(function(){
      $('#example1').on('click','.btn-add', function(ev){
      ev.preventDefault();
      $('#id').val($(this).data('id'));
      $('#id_kompetisi').val($(this).data('id_kompetisi'));
      $('#id_kontingen').val($(this).data('id_kontingen'));
      $('#cabor').val($(this).data('cabor'));
      $('#id_cabor').val($(this).data('id_cabor'));
      
      $.ajax({
        type: "POST", // Method pengiriman data bisa dengan GET atau POST
        url: "<?php echo base_url("kompetisi_user/listKelasKompetisi"); ?>", // Isi dengan url/path file php yang dituju
        data: {id_cabor : $("#id_cabor").val(),
               id_kompetisi : $("#id_kompetisi").val()
        }, // data yang akan dikirim ke file yang dituju
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ // Ketika proses pengiriman berhasil
          $("#loading").hide(); // Sembunyikan loadingnya
          // set isi dari combobox kota
          // lalu munculkan kembali combobox kotanya
          $("#kelas").html(response.list_kelas).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
      $('#modal-add').modal();
    });

    });
  </script>
  




</body>
</html>


