<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <!-- <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li>
        </ul>
    </div> -->
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

        <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title "> Daftar Keikutsertaan By Name Pada Kompetisi <?=$rowdata->nama_kompetisi?></span>
                    </div>
                    
                    
                    
                    <div class="panel-body pn">
                        <div class="table-responsive">
                        <table border="0" style="width:100%" id="example3" class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center">No</th>
                                    <th class="text-center">Cabor</th>
                
                                    <th class="text-center">Atlit Pa</th>
                                    <th class="text-center">Atlit Pi</th>
                                    <th class="text-center">Pelatih Pa</th>
                                    <th class="text-center">Pelatih Pi</th>
                                   
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if($ikut == FALSE ){ ?>
                                    <tr class="bg-light">                                    
                                    <th class="text-center" colspan="9">Data Kosong</th>
                                    
                                </tr>
                                <?php }else{ 
                                $no = 0;
                                foreach($ikut as $ikt): 
                                $no++
                                ?>
                                <tr>
                                    <td class=""><?=$no?></td>
                                    <td class=""><?=nama_cabor($ikt->id_cabor)?></td>
                         
                                    <td class=""><?=$ikt->atlit_pa?></td>
                                    <td class=""><?=$ikt->atlit_pi?></td>
                                    <td class=""><?=$ikt->pelatih_pa?></td>
                                    <td class=""><?=$ikt->pelatih_pi?></td>
                                    
                                    
                                </tr>
                                <?php endforeach;} ?>

                                </tbody>
                            </table>

                            <hr/>
                        

                            <table border="0" style="width:100%" id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center" width="5">No</th>
                                    <th class="text-center">Cabor</th>
                                    <th class="text-center">Kelas</th>
                                    <th class="text-center">Atlit</th> 
                                    <th class="text-center">Pelatih</th> 
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if($alldata == FALSE ){ ?>
                                    <tr class="bg-light">                                    
                                    <th class="text-center" colspan="9">Data Kosong</th>
                                    
                                </tr>
                                <?php }else{ 
                                $no = 0;
                                foreach($alldata as $all): 
                                $no++;
                                ?>
                                <tr>
                                    <td class=""><?=$no?></td>
                                    <td class=""><?=nama_cabor($all->id_cabor)?></td>
                                    <td class=""><?=nama_kelas($all->id_kelas)?> <?=jenis_kelas($all->id_kelas)?></td>
                                    
                                    <td class="">
                                    <br/>
                                    <?php
                                    $hitatl = $this->db->query("SELECT count(id_atlit) as atl FROM e_kompetisi_by_name_atlit a
                                    WHERE a.id_kompetisi = '$all->id_kompetisi'
                                    AND a.id_kontingen = '$all->id_kontingen' 
                                    AND a.id_cabor = '$all->id_cabor'
                                    AND a.id_kelas = '$all->id_kelas'
                                    AND a.deleted_at IS NULL")->row();
                                    // echo $hitatl->atl;
                                    $hitkls = $this->Kompetisi_user_model->get_keikutsertaan_cabor($all->id_kompetisi, $all->id_kontingen, $all->id_cabor);
                                    if(jenis_kelas($all->id_kelas) == 'Pa'){
                                        $klss = $hitkls->atlit_pa;
                                    }else{
                                        $klss =  $hitkls->atlit_pi;
                                    }
                                    ?>
                                
                                    <?php if($hitatl->atl < $klss){ ?>
                                    <button type="button" class="btn btn-success btn-sm btn-add" data-placement="top" data-toggle="Tambah"  
                                        data-original-title="Tambah" 
                                        data-id_kompetisi="<?=$all->id_kompetisi?>"
                                        data-id_kontingen="<?=$all->id_kontingen?>"
                                        data-cabor="<?=nama_cabor($all->id_cabor)?>"
                                        data-kelas="<?=nama_kelas($all->id_kelas)?>"
                                        data-id_cabor="<?=$all->id_cabor?>"
                                        data-id_kelas="<?=$all->id_kelas?>"
                                    ><i class="fa fa-plus"></i></button>
                                    <?php } ?>

                                    <br/>
                                    <?php
                                    $atlit = $this->db->query("SELECT a.* FROM e_kompetisi_by_name_atlit a
                                    WHERE a.id_kompetisi = '$all->id_kompetisi'
                                    AND a.id_kontingen = '$all->id_kontingen' 
                                    AND a.id_cabor = '$all->id_cabor'
                                    AND a.id_kelas = '$all->id_kelas'
                                    AND a.deleted_at IS NULL")->result();
                                    foreach($atlit AS $atl):
                                    ?>

                                    <p><?=nama_atlit($atl->id_atlit)?>
                                    
                                    <a target="BLANK" href="<?=base_url('atlit/detail/'.$atl->id_atlit)?>"><button type="button"
                                                    class="btn btn-warning br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     <span class="fa fa-eye">
                                                    </button>
                                    </a>

                                    <a href="<?=base_url('kompetisi_user/hapus_atlit_by_name/'.$atl->id_kompetisi_by_name_atlit)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin menghapus?')"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                     <span class="fa fa-trash">
                                                    </button>
                                    </a>
                                    </p>

                                    <?php endforeach; ?>

                                    </td>


                                    <td class="">

                                    <!-- Button trigger modal -->

                                    <br/>
                                    <?php
                                    $hitplt = $this->db->query("SELECT count(id_pelatih) as plt FROM e_kompetisi_by_name_pelatih a
                                    WHERE a.id_kompetisi = '$all->id_kompetisi'
                                    AND a.id_kontingen = '$all->id_kontingen' 
                                    AND a.id_cabor = '$all->id_cabor'
                                    AND a.id_kelas = '$all->id_kelas'
                                    AND a.deleted_at IS NULL")->row();
                                    // echo $hitatl->atl;
                                    $hitkls = $this->Kompetisi_user_model->get_keikutsertaan_cabor($all->id_kompetisi, $all->id_kontingen, $all->id_cabor);
                                    if(jenis_kelas($all->id_kelas) == 'Pa'){
                                        $klsz = $hitkls->pelatih_pa;
                                    }else{
                                        $klsz =  $hitkls->pelatih_pi;
                                    }
                                    ?>
                                
                                    <?php if($hitplt->plt < $klsz){ ?>
                                    <button type="button" class="btn btn-success btn-sm btn-add-p" data-placement="top" data-toggle="Tambah"  
                                        data-original-title="Tambah" 
                                        data-id_kompetisi_p="<?=$all->id_kompetisi?>"
                                        data-id_kontingen_p="<?=$all->id_kontingen?>"
                                        data-cabor_p="<?=nama_cabor($all->id_cabor)?>"
                                        data-kelas_p="<?=nama_kelas($all->id_kelas)?>"
                                        data-id_cabor_p="<?=$all->id_cabor?>"
                                        data-id_kelas_p="<?=$all->id_kelas?>"
                                    ><i class="fa fa-plus"></i></button>
                                    <?php } ?>

                                    <br/>
                                    <?php
                                    $pelatih = $this->db->query("SELECT a.* FROM e_kompetisi_by_name_pelatih a
                                    WHERE a.id_kompetisi = '$all->id_kompetisi'
                                    AND a.id_kontingen = '$all->id_kontingen' 
                                    AND a.id_cabor = '$all->id_cabor'
                                    AND a.id_kelas = '$all->id_kelas'
                                    AND a.deleted_at IS NULL")->result();
                                    foreach($pelatih AS $plt):
                                    ?>

                                    <p><?=nama_pelatih($plt->id_pelatih)?>
                                    
                                    <a href="<?=base_url('kompetisi_user/hapus_pelatih_by_name/'.$plt->id_kompetisi_by_name_pelatih)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin menghapus?')"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                     <span class="fa fa-trash">
                                                    </button>
                                    </a>
                                    </p>

                                    <?php endforeach; ?>

                                    </td>
                                    
                                </tr>
                                <?php endforeach;} ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>
<?php include(__DIR__ . "/../template/datatable.php"); ?>
<script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "info": false,   
      "bLengthChange": false,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "scrollX": true,
      "autoWidth": true
    });
    $('#example3').DataTable( {
      "info": false,   
      "bLengthChange": false,
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "scrollX": true,
      "autoWidth": true
    });
});
</script>

<!-- Modal Tambah Atlit -->
<?=form_open_multipart('kompetisi_user/adds_atlit_by_name', 'method="POST" autocomplete="off" class="form-horizontal form-label-left" role="form" enctype="multipart/form-data"'); ?>
        <div class="modal fade" id="modal-add">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Tambah Keikutsertaan By Name
              </div>
              <div class="modal-body">
                        
                        <input type="hidden" class="form-control" name="id_kompetisi" id="id_kompetisi">
                        <input type="hidden" class="form-control" name="id_kontingen" id="id_kontingen">
                        <input type="hidden" class="form-control" name="id_cabor" id="id_cabor">
                        <input type="hidden" class="form-control" name="id_kelas" id="id_kelas">

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Cabor</label>
                                 <div class="col-sm-10">
                                 <input type="text" class="form-control" name="cabor" id="cabor" readonly>
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                                 <div class="col-sm-10">
                                 <input type="text" class="form-control" name="kelas" id="kelas" readonly>
                                </div>
                        </div>
                         
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Atlit</label>
                            <div class="col-sm-10">                
                            <select required class="form-control select2" id="id_atlit" name="id_atlit" style="width: 100%;">
                            <option selected disabled>Pilih Atlit</option>
                            
                            <select>
                            Agar atlit dapat masuk ke daftar di atas, silakan lengkapi data atlit terlebih dahulu.
                            </div>
                        </div>

              </div>
              <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah <i class="fa fa-send"></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <?=form_close(); ?>
        <!-- End Modal Edit -->

    <script>
    $(document).ready(function(){
      $('#example1').on('click','.btn-add', function(ev){
      ev.preventDefault();
      $('#id').val($(this).data('id'));
      $('#id_kompetisi').val($(this).data('id_kompetisi'));
      $('#id_kontingen').val($(this).data('id_kontingen'));
      $('#cabor').val($(this).data('cabor'));
      $('#kelas').val($(this).data('kelas'));
      $('#id_cabor').val($(this).data('id_cabor'));
      $('#id_kelas').val($(this).data('id_kelas'));
      $.ajax({
        type: "POST", // Method pengiriman data bisa dengan GET atau POST
        url: "<?php echo base_url("kompetisi_user/listAtlit"); ?>", // Isi dengan url/path file php yang dituju
        data: {id_kelas : $("#id_kelas").val(),
                id_kontingen : $("#id_kontingen").val() 
                }, // data yang akan dikirim ke file yang dituju
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ // Ketika proses pengiriman berhasil
          $("#loading").hide(); // Sembunyikan loadingnya
          // set isi dari combobox kota
          // lalu munculkan kembali combobox kotanya
          $("#id_atlit").html(response.list_atlit).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
      $('#modal-add').modal();
    });

    });
  </script>

<!-- Modal Tambah Pelatih -->
<?=form_open_multipart('kompetisi_user/adds_pelatih_by_name', 'method="POST" autocomplete="off" class="form-horizontal form-label-left" role="form" enctype="multipart/form-data"'); ?>
        <div class="modal fade" id="modal-add-p">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Tambah Keikutsertaan By Name 
              </div>
              <div class="modal-body">
                        
                        <input type="hidden" class="form-control" name="id_kompetisi_p" id="id_kompetisi_p">
                        <input type="hidden" class="form-control" name="id_kontingen_p" id="id_kontingen_p">
                        <input type="hidden" class="form-control" name="id_cabor_p" id="id_cabor_p">
                        <input type="hidden" class="form-control" name="id_kelas_p" id="id_kelas_p">

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Cabor</label>
                                 <div class="col-sm-10">
                                 <input type="text" class="form-control" name="cabor_p" id="cabor_p" readonly>
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                                 <div class="col-sm-10">
                                 <input type="text" class="form-control" name="kelas_p" id="kelas_p" readonly>
                                </div>
                        </div>
                         
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Pelatih</label>
                            <div class="col-sm-10">                
                            <select required class="form-control select2" id="id_pelatih" name="id_pelatih" style="width: 100%;">
                            <option selected disabled>Pilih Pelatih</option>
                            
                            <select>
                            </div>
                        </div>

              </div>
              <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah <i class="fa fa-send"></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <?=form_close(); ?>
        <!-- End Modal Edit -->

    <script>
    $(document).ready(function(){
      $('#example1').on('click','.btn-add-p', function(ev){
      ev.preventDefault();
      $('#id_kompetisi_p').val($(this).data('id_kompetisi_p'));
      $('#id_kontingen_p').val($(this).data('id_kontingen_p'));
      $('#cabor_p').val($(this).data('cabor_p'));
      $('#kelas_p').val($(this).data('kelas_p'));
      $('#id_cabor_p').val($(this).data('id_cabor_p'));
      $('#id_kelas_p').val($(this).data('id_kelas_p'));
      $.ajax({
        type: "POST", // Method pengiriman data bisa dengan GET atau POST
        url: "<?php echo base_url("kompetisi_user/listPelatih"); ?>", // Isi dengan url/path file php yang dituju
        data: {id_kelas_p : $("#id_kelas_p").val(),
                id_kontingen_p : $("#id_kontingen_p").val() 
                }, // data yang akan dikirim ke file yang dituju
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ // Ketika proses pengiriman berhasil
          $("#loading").hide(); // Sembunyikan loadingnya
          // set isi dari combobox kota
          // lalu munculkan kembali combobox kotanya
          $("#id_pelatih").html(response.list_pelatih).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
      $('#modal-add-p').modal();
    });

    });
  </script>

  




</body>
</html>


