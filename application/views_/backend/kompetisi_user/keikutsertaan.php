<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <!-- <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li> 
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li>
        </ul>
    </div> -->
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

        <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title "> Daftar Keikutsertaan Pada Kompetisi <?=$rowdata->nama_kompetisi?></span>
                    </div>
                    <div class="topbar-right  mt5 mr35">
                    <?php if($rowdata->tahap == 1){ ?>
                        <a href="<?=base_url('kompetisi_user/add_keikutsertaan/'.$rowdata->id_kompetisi)?>" class="btn btn-primary btn-sm ml10" title="New Order">
                            <span class="fa fa-plus pr5"></span><span class="fa fa-file-o pr5"></span>Tambah Keikutsertaan</a>

                     <?php } ?>
                        
                    </div>
                    

                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <table border="0" style="width:100%" id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center">No</th>
                                    <th class="text-center">Cabor</th>
                                    <!-- <th class="text-center">Kelas Pa</th>
                                    <th class="text-center">Kelas Pi</th> -->
                                    <th class="text-center">Atlit Pa</th>
                                    <th class="text-center">Atlit Pi</th>
                                    <th class="text-center">Pelatih Pa</th>
                                    <th class="text-center">Pelatih Pi</th>
                                    <th class="text-center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if($alldata == FALSE ){ ?>
                                    <tr class="bg-light">                                    
                                    <th class="text-center" colspan="9">Data Kosong</th>
                                    
                                </tr>
                                <?php }else{ 
                                $no = 0;
                                foreach($alldata as $all): 
                                $no++
                                ?>
                                <tr>
                                    <td class=""><?=$no?></td>
                                    <td class=""><?=nama_cabor($all->id_cabor)?></td>
                                    <!-- <td class=""><?=$all->kelas_pa?></td>
                                    <td class=""><?=$all->kelas_pi?></td> -->
                                    <td class=""><?=$all->atlit_pa?></td>
                                    <td class=""><?=$all->atlit_pi?></td>
                                    <td class=""><?=$all->pelatih_pa?></td>
                                    <td class=""><?=$all->pelatih_pi?></td>
                                    <td class="">
                                   
                                    
                                    
                                   

                                    <?php if($all->generate_by_kontingen == 0){ ?>

                                        <button type="button" class="btn btn-success btn-xs tooltips btn-edit" data-placement="top" data-toggle="modal"  
                                        data-original-title="Edit" 
                                        data-id="<?=$all->id_kompetisi_keikutsertaan?>"
                                        data-cabor="<?=nama_cabor($all->id_cabor)?>"
                                        data-atlit_pa="<?=$all->atlit_pa?>"
                                        data-atlit_pi="<?=$all->atlit_pi?>"
                                        data-pelatih_pa="<?=$all->pelatih_pa?>"
                                        data-pelatih_pi="<?=$all->pelatih_pi?>"
                                        ><i class="fa fa-eye"></i> Edit </button>
                                    

                                        <!-- <a href="<?=base_url('kompetisi_user/add_keikutsertaan_detail/'.$all->id_kompetisi_keikutsertaan)?>"><button type="button"
                                                    class="btn btn-warning br2 btn-xs fs12" onclick="return confirm('Anda yakin ingin menambah detail keikutsertaan?')"
                                                     aria-expanded="false">
                                                     <span class="fa fa-plus"> Tambah Detail Keikutsertaan
                                                    </button>
                                        </a> -->


                                    <?php }else{ ?>
                                    <!-- <a href="<?=base_url('kompetisi_user/update_keikutsertaan_detail/'.$all->id_kompetisi_keikutsertaan)?>"><button type="button"
                                                    class="btn btn-warning br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     <span class="fa fa-eye"> Detail Keikutsertaan
                                                    </button>
                                    </a> -->
                                    <?php } ?>

                                    <a href="<?=base_url('kompetisi_user/hapus_keikutsertaan/'.$all->id_kompetisi_keikutsertaan)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin menghapus?')"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                     <span class="fa fa-trash pr5">
                                                    </button>
                                    </a>

                                    

                                    
                                    </td>
                                    
                                </tr>
                                <?php endforeach;} ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>
<?php include(__DIR__ . "/../template/datatable.php"); ?>
<script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "info": false,   
      "bLengthChange": false,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "autoWidth": true,
      "scrollX": true,
      "autoWidth": true
    });
});
</script>

  <!-- Modal Edit -->
  <?=form_open_multipart('kompetisi_user/edit_keikutsertaan', 'method="POST" autocomplete="off" class="form-horizontal form-label-left" role="form" enctype="multipart/form-data"'); ?>
        <div class="modal fade" id="modal-edit">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Edit Keikutsertaan 
              </div>
              <div class="modal-body">
                        <input type="hidden" class="form-control" name="id" id="id">

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Atlit Putra</label>
                                 <div class="col-sm-10">
                        <input type="text" class="form-control"  id="cabor" readonly>
                        </div>
                        </div>
                         
                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Atlit Putra</label>
                                 <div class="col-sm-10">
                                
                                 <input type="text" class="form-control" name="atlit_pa" id="atlit_pa">
                                </div>
                         </div>

                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Atlit Putri</label>
                                 <div class="col-sm-10">
              
                                 <input type="text" class="form-control" name="atlit_pi" id="atlit_pi">
                                </div>
                         </div>

                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Pelatih Putra</label>
                                 <div class="col-sm-10">
                                
                                 <input type="text" class="form-control" name="pelatih_pa" id="pelatih_pa">
                                </div>
                         </div>

                         <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Pelatih Putri</label>
                                 <div class="col-sm-10">
                                
                                 <input type="text" class="form-control" name="pelatih_pi" id="pelatih_pi">
                                </div>
                         </div>



                        
                         


              </div>
              <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Edit <i class="fa fa-send"></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <?=form_close(); ?>
        <!-- End Modal Edit -->

<script>
    $(document).ready(function(){
      $('#example1').on('click','.btn-edit', function(ev){
      ev.preventDefault();
      $('#id').val($(this).data('id'));
      $('#cabor').val($(this).data('cabor'));
      $('#atlit_pa').val($(this).data('atlit_pa'));
      $('#atlit_pi').val($(this).data('atlit_pi'));
      $('#pelatih_pa').val($(this).data('pelatih_pa'));
      $('#pelatih_pi').val($(this).data('pelatih_pi'));
      $('#modal-edit').modal();
    });

    });
  </script>



</body>
</html>


