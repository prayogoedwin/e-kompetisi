 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">





<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

  <!-- -------------- Column Center -------------- -->
                        <div class="chute chute-center">
                            <!-- -------------- Registration -------------- -->
                            <div class="allcp-form theme-primary tab-pane mw600" id="register" role="tabpanel">
                            <div class="panel">
                                <div class="panel-heading">
                                    <span class="panel-title">
                                      Informasi Pelatih
                                    </span>
                                </div>
                                <!-- -------------- /Panel Heading -------------- -->

                                 <?php echo form_open_multipart('pelatih/updates') ?>
                                    <div class="panel-body pn" id="example1">

                                    <div class="section">
                                             <!-- Button trigger modal -->
                                             <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-add-p">
                                            <i class="fa fa-picture-o">&nbsp;Pas Foto</i>
                                            </button>
                                            
                                           

                                        </div>
                                        <!-- -------------- /section -------------- -->


                                        <div class="section">
                                        NO KTP
                                            <label for="email" class="field prepend-icon">
                                                <input required type="number" name="nik" id="nik" class="gui-input" value="<?=$detail->nik?>" placeholder="NIK">
                                                <input required type="hidden" name="id_pelatih" id="id_pelatih" class="gui-input" value="<?=$detail->id_pelatih?>" placeholder="NIK">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-book"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                               

                                       

                                        <div class="section">
                                        Nama Lengkap Pelatih
                                            <label for="email" class="field prepend-icon">
                                                <input required type="text" name="nama_pelatih" id="nama_pelatih" class="gui-input"
                                                       placeholder="Nama Pelatih" value="<?=$detail->nama_pelatih?>">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-user"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Jenis Kelamin
                                            <label class="field select prepend-icon">
                                                
                                                <select id="jk"  name="jk">
                                                    
                                                    <option <?php if($detail->jk == 'Pa'){echo 'selected';}  ?> value="Pa">Putra</option>
                                                    <option <?php if($detail->jk == 'Pi'){echo 'selected';}  ?> value="Pi">Putri</option>
                                                   
                                                </select>
                                                <i class="arrow double"></i>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Alamat
                                            <label for="email" class="field prepend-icon">
                                                <input required type="text" name="alamat" id="alamat" class="gui-input"
                                                       placeholder="Alamat" value="<?=$detail->alamat?>">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-map-marker"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Tempat Lahir
                                            <label for="email" class="field prepend-icon">
                                                <input required type="text" name="tempat_lahir" id="tempat_lahir" class="gui-input"
                                                       placeholder="tempat_lahir" value="<?=$detail->tempat_lahir?>">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-home"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Tanggal Lahir
                                            <label for="email" class="field prepend-icon">
                                                <input required type="date" name="tanggal_lahir" id="tanggal_lahir" class="gui-input"
                                                       placeholder="tanggal_lahir" value="<?=$detail->tanggal_lahir?>">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-calendar"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Tinggi Badan (CM)
                                            <label for="email" class="field prepend-icon">
                                                <input required type="number" name="tinggi" id="tinggi" class="gui-input"
                                                       placeholder="Tinggi Badan" value="<?=$detail->tinggi_badan?>">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-arrows-v"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                        Berat Badan (Kg)
                                            <label for="email" class="field prepend-icon">
                                                <input required type="number" name="berat" id="berat" class="gui-input"
                                                       placeholder="Berat Badan" value="<?=$detail->berat_badan?>">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-arrows-h"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                            <img src="<?=base_url('assets/qr/').$detail->qr?>" alt="" width="100" height="110">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-arrows-h"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        

                                       
                                            
                                        <div class="section">
 
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-bordered btn-primary">
                                                    Update informasi pelatih
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                    </div>
                                    <!-- -------------- /Form -------------- -->
                                    <div class="panel-footer"></div>
                                <?php echo form_close();?>
                            </div>
                            <!-- -------------- /Panel -------------- -->
                        </div>
                        <!-- -------------- /Registration -------------- -->

                        

</div>
<!-- -------------- //End Column Center -------------- -->

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>

<!-- Modal Upload Foto -->
<?=form_open_multipart('pelatih/adds_foto', 'method="POST" autocomplete="off" class="form-horizontal form-label-left" role="form" enctype="multipart/form-data"'); ?>
        <div class="modal fade" id="modal-add-p">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Pas Foto
              </div>
              <div class="modal-body">
                        
                        
                        <input required type="hidden" class="form-control" name="id_pelatih" id="id_pelatih" readonly value="<?=$detail->id_pelatih?>">

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Pelatih</label>
                                 <div class="col-sm-10">
                                 <input required type="text" class="form-control" readonly value="<?=$detail->nama_pelatih?>">
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Foto</label>
                                 <div class="col-sm-10">
                                 <img src="<?=base_url().$detail->foto?>" alt="" width="100" height="110">
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Upload Foto</label>
                                 <div class="col-sm-10">
                                 <input required type="file" class="form-control" name="foto" id="foto">
                                </div>
                        </div>

                        
                         
                       

              </div>
              <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload Foto <i class="fa fa-send"></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <?=form_close(); ?>
        <!-- End Modal Upload Foto -->


        <!-- Modal Upload Ijazah -->
<?=form_open_multipart('atlit/adds_ijazah', 'method="POST" autocomplete="off" class="form-horizontal form-label-left" role="form" enctype="multipart/form-data"'); ?>
        <div class="modal fade" id="modal-add-i">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Ijazah
              </div>
              <div class="modal-body">
                        
                        
                        <input required type="hidden" class="form-control" name="id_atlit" id="id_atlit" readonly value="<?=$detail->id_atlit?>">

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Atlit</label>
                                 <div class="col-sm-10">
                                 <input required type="text" class="form-control" readonly value="<?=$detail->nama_atlit?>">
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Ijazah</label>
                                 <div class="col-sm-10">
                                 <iframe src="https://docs.google.com/viewer?url=http://www.pdf995.com/samples/pdf.pdf&embedded=true" frameborder="0" height="300" width="100%"></iframe>
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Upload Ijazah</label>
                                 <div class="col-sm-10">
                                 <input required type="file" class="form-control" name="foto" id="foto">
                                </div>
                        </div>

                        
                         
                       

              </div>
              <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload Foto <i class="fa fa-send"></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <?=form_close(); ?>
        <!-- End Modal Edit -->


         <!-- Modal Upload Ijazah -->
<?=form_open_multipart('atlit/adds_ijazah', 'method="POST" autocomplete="off" class="form-horizontal form-label-left" role="form" enctype="multipart/form-data"'); ?>
        <div class="modal fade" id="modal-add-k">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Kartu Keluarga
              </div>
              <div class="modal-body">
                        
                        
                        <input required type="hidden" class="form-control" name="id_atlit" id="id_atlit" readonly value="<?=$detail->id_atlit?>">

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Atlit</label>
                                 <div class="col-sm-10">
                                 <input required type="text" class="form-control" readonly value="<?=$detail->nama_atlit?>">
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">KK</label>
                                 <div class="col-sm-10">
                                 <iframe src="https://docs.google.com/viewer?url=http://www.pdf995.com/samples/pdf.pdf&embedded=true" frameborder="0" height="300" width="100%"></iframe>
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Upload KK</label>
                                 <div class="col-sm-10">
                                 <input required type="file" class="form-control" name="foto" id="foto">
                                </div>
                        </div>

                        
                         
                       

              </div>
              <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload Foto <i class="fa fa-send"></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <?=form_close(); ?>
        <!-- End Modal Edit -->




</body>
</html>


