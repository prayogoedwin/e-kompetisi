<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

        <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title "> Daftar Pelatih</span>
                    </div>
                    <div class="topbar-right  mt5 mr35">

                    <?php
                    
                    $yy = $this->session->userdata('akses');
                    if($yy == '99'){

                    }else{
                    ?>
                        <a href="<?=base_url('pelatih/add')?>" class="btn btn-primary btn-sm ml10" title="New Order">
                            <span class="fa fa-plus pr5"></span><span class="fa fa-file-o pr5"></span>Tambah Pelatih</a>
                            <?php } ?>   
                    </div>

                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <table border="0" style="width:100%" id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center">No</th>
                                    <th class="text-center">NIK</th>
                                    <th class="text-center">Nama pelatih</th>
                                    <th class="text-center">Jenis Kelamin</th>
                                    <th class="text-center">TTL</th>
                                    <th class="text-center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if($alldata == FALSE ){ ?>
                                    <tr class="bg-light">                                    
                                    <th class="text-center" colspan="7">Data Kosong</th>
                                    
                                </tr>
                                <?php }else{ 
                                $no = 0;
                                foreach($alldata as $all): 
                                $no++
                                ?>
                                <tr>
                                    <td class=""><?=$no?></td>
                                    <td class=""><?=$all->nik?></td>
                                 
                                    <td class=""><?=$all->nama_pelatih?></td>
                                    <td class=""><?=nama_jenis($all->jk)?></td>
                                    <td class=""><?=$all->tempat_lahir?>, <?=$this->formatter->getDateMonthFormatUser($all->tanggal_lahir)?></td>
                                    <td class="text-center">
                                    

                                    <a href="<?=base_url('pelatih/detail/'.$all->id_pelatih)?>"><button type="button"
                                                    class="btn btn-info br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     <span class="fa fa-edit pr5">
                                                    </button>
                                    </a>

                                    <a href="<?=base_url('pelatih/hapus/'.$all->id_pelatih)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin menghapus?')"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                     <span class="fa fa-trash pr5">
                                                    </button>
                                    </a>

                                    
                                    </td>
                                    
                                </tr>
                                <?php endforeach;} ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>
<?php include(__DIR__ . "/../template/datatable.php"); ?>
<script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "info": false,   
      "bLengthChange": false,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "autoWidth": true,
      "scrollX": true,
      "autoWidth": true
    });
});
</script>



</body>
</html>


