     <!-- -------------- Main Wrapper -------------- -->
     <section id="content_wrapper">

<!-- -------------- Topbar Menu Wrapper -------------- -->
<div id="topbar-dropmenu-wrapper">
    <div class="topbar-menu row">
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="service-box bg-danger">
                <span class="fa fa-music"></span>
                <span class="service-title">Audio</span>
            </a>
        </div>
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="service-box bg-success">
                <span class="fa fa-picture-o"></span>
                <span class="service-title">Images</span>
            </a>
        </div>
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="service-box bg-primary">
                <span class="fa fa-video-camera"></span>
                <span class="service-title">Videos</span>
            </a>
        </div>
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="service-box bg-alert">
                <span class="fa fa-envelope"></span>
                <span class="service-title">Messages</span>
            </a>
        </div>
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="service-box bg-system">
                <span class="fa fa-cog"></span>
                <span class="service-title">Settings</span>
            </a>
        </div>
        <div class="col-xs-4 col-sm-2">
            <a href="#" class="service-box bg-dark">
                <span class="fa fa-user"></span>
                <span class="service-title">Users</span>
            </a>
        </div>
    </div>
</div>
<!-- -------------- /Topbar Menu Wrapper -------------- -->

<!-- -------------- Topbar -------------- -->
<header id="topbar" class="alt">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="breadcrumb-icon">
                <a href="dashboard1.html">
                    <span class="fa fa-home"></span>
                </a>
            </li>
            <li class="breadcrumb-active">
                <a href="dashboard1.html">Dashboard</a>
            </li>
            <li class="breadcrumb-link">
                <a href="<?=base_url('sys')?>">Home</a>
            </li>
            <li class="breadcrumb-current-item">Dashboard</li>
        </ol>
    </div>
    <!-- <div class="topbar-right">
        <div class="ib topbar-dropdown">
            <label for="topbar-multiple" class="control-label">Reporting Period</label>
            <select id="topbar-multiple" class="hidden">
                <optgroup label="Filter By:">
                    <option value="1-1">Last 30 Days</option>
                    <option value="1-2" selected="selected">Last 60 Days</option>
                    <option value="1-3">Last Year</option>
                </optgroup>
            </select>
        </div>
        <div class="ml15 ib va-m" id="sidebar_right_toggle">
            <div class="navbar-btn btn-group btn-group-number mv0">
                <button class="btn btn-sm btn-default btn-bordered prn pln">
                    <i class="fa fa-bar-chart fs22 text-default"></i>
                </button>

            </div>
        </div>
    </div> -->
</header>
<!-- -------------- /Topbar -------------- -->
 <!-- -------------- Content -------------- -->
 <section id="content" class="table-layout animated fadeIn">

<!-- -------------- Column Center -------------- -->
<div class="chute chute-center">

    <!-- -------------- Quick Links -------------- -->
    <div class="row">
        <div class="col-sm-6 col-xl-3">
            <div class="panel panel-tile">
                <div class="panel-body">
                    <div class="row pv10">
                        <div class="col-xs-5 ph10"><img src="<?=base_url()?>assets/all/assets/img/pages/clipart0.png"
                                                        class="img-responsive mauto" alt=""/></div>
                        <div class="col-xs-7 pl5">
                            <h6 class="text-muted">JUMLAH KOMPETISI AKTIF</h6>

                            <h2 class="fs50 mt5 mbn">385</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="panel panel-tile">
                <div class="panel-body">
                    <div class="row pv10">
                        <div class="col-xs-5 ph10"><img src="<?=base_url()?>assets/all/assets/img/pages/clipart1.png"
                                                        class="img-responsive mauto" alt=""/></div>
                        <div class="col-xs-7 pl5">
                            <h6 class="text-muted">JUMLAH KOMPETISI SUDAH DIIKUTI</h6>

                            <h2 class="fs50 mt5 mbn">197</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="panel panel-tile">
                <div class="panel-body">
                    <div class="row pv10">
                        <div class="col-xs-5 ph10"><img src="<?=base_url()?>assets/all/assets/img/pages/clipart2.png"
                                                        class="img-responsive mauto" alt=""/></div>
                        <div class="col-xs-7 pl5">
                            <h6 class="text-muted">JUMLAH ATLIT</h6>

                            <h2 class="fs50 mt5 mbn">6789</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="panel panel-tile">
                <div class="panel-body">
                    <div class="row pv10">
                        <div class="col-xs-5 ph10"><img src="<?=base_url()?>assets/all/assets/img/pages/clipart3.png"
                                                        class="img-responsive mauto" alt=""/></div>
                        <div class="col-xs-7 pl5">
                            <h6 class="text-muted">JUMLAH PELATIH</h6>

                            <h2 class="fs50 mt5 mbn">256</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




        </div>


    </div>


</div>
<!-- -------------- /Column Center -------------- -->


</section>
<!-- -------------- /Content -------------- -->


</section>
<!-- -------------- /Main Wrapper -------------- -->


<?php include(__DIR__ . "/template/footer.php"); ?>

</body>

</html>