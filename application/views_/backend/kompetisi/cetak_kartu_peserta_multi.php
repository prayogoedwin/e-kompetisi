<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>CETAK ID CARD ATLIT</title>
    <style>
      @media print {
        @page {
          size: auto;
          margin: 0mm 0mm 0mm 0mm;
        }
        .cards{
            display: table !important;
        }

        .card{
            display: inline-block !important;
            margin: 0 20px !important
        }
      }
    </style>
  </head>
  <body onload="window.print()">
    <!-- Class Cards untuk mengelompokkan menjadi Grid -->
    <div class="cards" style="display: flex; flex-wrap: wrap;">
      <!-- Class Card untuk container dari Card -->
      <?php foreach($alldata as $kartu): ?>
      <div
        class="card"
        style="width:320px;margin: 0 auto; display: flex; flex-direction: column;"
      >
        <!-- Class Content untuk Container Data Content -->
        <div class="content" style="padding: 20px 20px 0 20px;">
          <!-- Class Heading untuk bagian head QrCode dan Logo -->
          <div class="heading" style="display:flex;">
            <div class="qrcode" style="width: 50%;">
              <img src="<?=base_url('assets/qr/'.$kartu->qr)?>" alt="QR Code" style="height:90px;" />
            </div>
            <div class="logo" style="width: 50%; text-align: right;">
              <img src="<?=base_url()?>assets/images/kontingen/Provinsi_Jawa_Tengah.png" alt="Logo" style="height:90px;" />
            </div>
          </div>
          <div class="foto" style="margin: 24px 0 24px 0;text-align: center;">
            <img src="<?=base_url($kartu->foto)?>" style="height: 164px;" alt="Foto" />
          </div>
          <h4 style="text-align: center; margin: 0; text-transform:uppercase;"><?=$kartu->nama?></h4>
          <h4 style="text-align: center; margin: 0; text-transform:uppercase;"><?=nama_kontingen($kartu->id_kontingen)?></h4>
          <h4 style="text-align: center; margin: 8px 0 0 0; text-transform:uppercase;">Atlit</h4>
          <h4 style="text-align: center; margin: 0; text-transform:uppercase;"><?=nama_kompetisi($kartu->id_kompetisi)?></h4>
        </div>
        <!-- Class Footer sebagai footer copyright -->
        <div
          class="footer"
          style="background-color: #ff5757; padding: 16px 12px; text-align: center; margin: 24px 0;font-size:10px;"
        >
          &copy; 2020 Copyright Disporapar Provinsi Jawa Tengah
        </div>
      </div>
    <?php endforeach; ?>

      
    </div>
  </body>
</html>
