<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Cetak Piagam</title>
    <style>
      @media print {
        html,
        body {
          height: 100%;
          padding: 0;
          margin: 0;
        }
        @page {
          size: A4 landscape;
          margin: 0mm 0mm 0mm 0mm;
        }
        .lists {
          /* display: table !important; */
          height: 100%;
          font-size: 20px;
        }

        .piagam {
          margin: 1em;
          min-width: calc(97% - 4.4em);
          min-height: calc(96% - 4.4em);
          border: 1em solid red;
          padding: 1em;
        }
        .page-break {
          page-break-before: always;
        }
        .avoid-break {
          page-break-inside: avoid;
        }
        td,
        .desc {
          line-height: 1.8em;
        }
        .desc {
          text-align: center;
        }
      }
    </style>
  </head>
  <body onload="window.print()">
    <div class="lists">
    <?php 
    if($alldata == FALSE ){ ?>
        Data Kosong  
    </tr>
    <?php }else{ 
    foreach($alldata as $all): 
    ?>
      <div class="piagam page-break" style="position: relative;">
        <div
          class="watermark"
          style="position: absolute;width: 100%;height: 100%;text-align: center;"
        >
          <img
            src="<?=base_url()?>assets/images/kontingen/Provinsi_Jawa_Tengah.png""
            alt="watermark"
            style="height: 76%;margin: 4em auto;filter: opacity(.1);"
          />
        </div>
        <div
          class="heading"
          style="display:flex;justify-content: center;align-items: center;"
        >
          <div class="logo">
            <img style="width: 4em;" src="<?=base_url()?>assets/images/kontingen/Provinsi_Jawa_Tengah.png" />
          </div>
          <h1
            class="title"
            style="font-size:2em;padding: 0 0 0 1em;letter-spacing: 0.2em;"
          >
            PIAGAM PENGHARGAAN
          </h1>
        </div>
        <div
          class="content"
          style="display: flex; flex-direction: column;align-items: center;margin-top: -0.8em;"
        >
          <div class="number">Nomor : <?=$all->id_medali.'/'.$all->nomor_medali?></div>
          <div class="text" style="margin-bottom: 1em;">Diberikan kepada :</div>
          <table style="width: 36%;margin-bottom: 1em;">
            <tr style="margin-bottom: 1em;">
              <td style="width: 35%;">Nama</td>
              <td>:</td>
              <td style="width: 60%;"><b><?=nama_atlit($all->id_atlit)?></b></td>
            </tr>
            <tr>
              <td>Asal</td>
              <td>:</td>
              <td><?=nama_kontingen($all->id_kontingen)?> </td>
            </tr>
            <tr>
              <td>Sebagai</td>
              <td>:</td>
              <td><?=nama_juara($all->id_medali)?> - <?=nama_kelas($all->id_kelas)?> - <?=nama_cabor($all->id_cabor)?></td>
            </tr>
          </table>

          <div class="desc">
            Atas Partisipasi dalam kegiatan <?=nama_kompetisi($all->id_kompetisi)?> Tahun <?php echo date('Y',strtotime(select_kompetisi($all->id_kompetisi)->created_at)) ?>
          </div>
          <div class="desc">
            Yang diselenggarakan pada tanggal <?php echo date('d',strtotime(select_kompetisi($all->id_kompetisi)->mulai)) ?> 
            s.d <?php echo $this->formatter->getDateMonthFormatUser(select_kompetisi($all->id_kompetisi)->selesai) ?> 
            di <?=select_kompetisi($all->id_kompetisi)->lokasi?>
          </div>
          <div
            class="main"
            style="margin-top: 2em; display: flex; flex-direction: column; align-items: center;">
            <div class="date" style="font-size: 0.9em;">
              Semarang, <?=$this->formatter->getDayDateFormatUserId($all->created_at)?>
            </div>
            <div class="dinas">
              KEPALA DINAS KEPEMUDAAN, OLAHRAGA DAN PARIWISATA
            </div>
            <div class="province">PROVINSI JAWA TENGAH</div>
          </div>
          <div
            class="main"
            style="margin-top: 2em; display: flex; flex-direction: column; align-items: center;"
          >
            <div
              class="small"
              style="font-size: 0.9em;border-bottom: 2px solid black;"
            >
              Drs. SINOENG N RACHMADI, MM
            </div>
            <div class="small" style="font-size: 0.9em;">
              Pembina Utama Muda
            </div>
            <div class="small" style="font-size: 0.9em;">
              NIP. 19691231 199402 1 006
            </div>
          </div>
        </div>
      </div>
    <?php endforeach; }?>
      
      <div class="avoid-break"></div>
    </div>
  </body>
</html>
