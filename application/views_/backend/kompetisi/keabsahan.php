<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

<!-- -------------- Main Wrapper -------------- -->
<section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
   <!-- <div class="topbar-left">
       <ul class="nav nav-list nav-list-topbar pull-left">
           <li class="active">
               <a href="dashboard2.html">Overview</a>
           </li>
           <li>
               <a href="sales-stats-products.html">Products</a>
           </li>
           <li>
               <a href="sales-stats-purchases.html">Orders</a>
           </li>
           <li>
               <a href="sales-stats-clients.html">Clients</a>
           </li>
           <li>
               <a href="sales-stats-general-settings.html">Settings</a>
           </li>
       </ul>
   </div> -->
   <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
      
       
   </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">
<!-- -------------- Column Center -------------- -->
<div class="chute chute-center">

<div class="mw1000 center-block">

   <!-- -------------- Spec Form -------------- -->
   <div class="allcp-form">
       <div class="panel">
           <div class="panel-heading">
               <div class="panel-title">Form Keabsahan
               </div>
           </div>
           <div class="panel-body">
           
                 
                   

                   <!-- -------------- Multi Selects -------------- -->
                   <div class="row">
                       <div class="col-md-4">
                       <?php echo form_open('kompetisi/sebelums/'.$this->uri->segment('3'), 'target="BLANK"') ?>
                       <input type="hidden" name="kompetisi2" id="kompetisi2" value="<?=$this->uri->segment('3')?>">
                           <div class="section">
                               <label class="field select">
                                   <select id="kontingen2" name="kontingen2">
                                       <option value="">Kontingen</option>
                                       <?php foreach($kontingen as $k2): ?>
                                           <option value="<?=$k2->id_kontingen?>"><?=nama_kontingen($k2->id_kontingen)?></option>
                                       <?php endforeach; ?>
                                   </select>
                                   <i class="arrow double"></i>
                               </label>
                           </div>

                           <div class="section">
                               <label class="field select">
                                   <select id="cabor2"  name="cabor2">
                                       
                                   </select>
                                   <i class="arrow double"></i>
                               </label>
                           </div>

                           <div class="section">
                               <label class="field select">
                                  
                                   <select id="jk2"  name="jk2">
                                               
                                    <option value="Pa">Putra</option>
                                    <option value="Pi">Putri</option>
                                                   
                                                </select>
                                   </select>
                                   <i class="arrow double"></i>
                               </label>
                           </div>

                           <div class="section">
                                           
                                           <div class="pull-left">
                                               <button type="submit" class="btn btn-bordered btn-success">
                                                   Sebelum
                                               </button>
                                           </div>
                                       </div>
                                       <!-- -------------- /section -------------- -->
                       <?php echo form_close();?>

                       </div>
                       <div class="col-md-4">
                       <?php echo form_open('kompetisi/validasi_/'.$this->uri->segment('3'), 'target="BLANK"') ?>
                        <input type="hidden" name="kompetisi" id="kompetisi" value="<?=$this->uri->segment('3')?>">
                           <div class="section">
                               <label class="field select">
                                   <select id="kontingen" name="kontingen">
                                       <option value="">Kontingen</option>
                                       <?php foreach($kontingen as $k2): ?>
                                           <option value="<?=$k2->id_kontingen?>"><?=nama_kontingen($k2->id_kontingen)?></option>
                                       <?php endforeach; ?>
                                   </select>
                                   <i class="arrow double"></i>
                               </label>
                           </div>

                           <div class="section">
                               <label class="field select">
                                   <select id="cabor"  name="cabor">
                                       
                                   </select>
                                   <i class="arrow double"></i>
                               </label>
                           </div>

                           <div class="section">
                               <label class="field select">
                                  
                                   <select id="jk"  name="jk">
                                               
                                    <option value="Pa">Putra</option>
                                    <option value="Pi">Putri</option>
                                                   
                                                </select>
                                   </select>
                                   <i class="arrow double"></i>
                               </label>
                           </div>

                           <div class="section">
                                           
                                           <div class="pull-left">
                                               <button type="submit" class="btn btn-bordered btn-warning">
                                                   Validasi
                                               </button>
                                           </div>
                                       </div>
                                       <!-- -------------- /section -------------- -->
                            <?php echo form_close();?>

                       </div>

                       <div class="col-md-4">
                       <?php echo form_open('kompetisi/sesudahs/'.$this->uri->segment('3'), 'target="BLANK"') ?>
                       <input type="hidden" name="kompetisi3" id="kompetisi3" value="<?=$this->uri->segment('3')?>">
                           <div class="section">
                               <label class="field select">
                                   <select id="kontingen3" name="kontingen3">
                                       <option value="">Kontingen</option>
                                       <?php foreach($kontingen as $k2): ?>
                                           <option value="<?=$k2->id_kontingen?>"><?=nama_kontingen($k2->id_kontingen)?></option>
                                       <?php endforeach; ?>
                                   </select>
                                   <i class="arrow double"></i>
                               </label>
                           </div>

                           <div class="section">
                               <label class="field select">
                                   <select id="cabor3"  name="cabor3">
                                       
                                   </select>
                                   <i class="arrow double"></i>
                               </label>
                           </div>

                           <div class="section">
                               <label class="field select">
                                  
                                   <select id="jk3"  name="jk3">
                                               
                                    <option value="Pa">Putra</option>
                                    <option value="Pi">Putri</option>
                                                   
                                                </select>
                                   </select>
                                   <i class="arrow double"></i>
                               </label>
                           </div>

                           <div class="section">
                                           
                                           <div class="pull-left">
                                               <button type="submit" class="btn btn-bordered btn-danger">
                                                   Sesudah
                                               </button>
                                           </div>
                                       </div>
                                       <!-- -------------- /section -------------- -->
                           <?php echo form_close();?>

                       </div>
                   </div>

                   


                   
                           </div>
                       </div>

                   </div>

                   </section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>

<script>
 $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
   // Kita sembunyikan dulu untuk loadingnya
   $("#loading").hide();
   
   $("#kontingen").change(function(){ // Ketika user mengganti atau memilih data provinsi
     $("#cabor").hide(); // Sembunyikan dulu combobox kota nya
     $("#loading").show(); // Tampilkan loadingnya
   
     $.ajax({
       type: "POST", // Method pengiriman data bisa dengan GET atau POST
       url: "<?php echo base_url("kompetisi/listCaborKontingenKompetisi"); ?>", // Isi dengan url/path file php yang dituju
       data: {kontingen : $("#kontingen").val(),
               kompetisi : $("#kompetisi").val()
           }, // data yang akan dikirim ke file yang dituju
       dataType: "json",
       beforeSend: function(e) {
         if(e && e.overrideMimeType) {
           e.overrideMimeType("application/json;charset=UTF-8");
         }
       },
       success: function(response){ // Ketika proses pengiriman berhasil
         $("#loading").hide(); // Sembunyikan loadingnya
         // set isi dari combobox kota
         // lalu munculkan kembali combobox kotanya
         $("#cabor").html(response.list_cabor).show();
       },
       error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
         alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
       }
     });
   });
 });
 </script>

<script>
 $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
   // Kita sembunyikan dulu untuk loadingnya
   $("#loading").hide();
   
   $("#kontingen2").change(function(){ // Ketika user mengganti atau memilih data provinsi
     $("#cabor2").hide(); // Sembunyikan dulu combobox kota nya
     $("#loading").show(); // Tampilkan loadingnya
   
     $.ajax({
       type: "POST", // Method pengiriman data bisa dengan GET atau POST
       url: "<?php echo base_url("kompetisi/listCaborKontingenKompetisi2"); ?>", // Isi dengan url/path file php yang dituju
       data: {kontingen2 : $("#kontingen2").val(),
               kompetisi2 : $("#kompetisi2").val()
           }, // data yang akan dikirim ke file yang dituju
       dataType: "json",
       beforeSend: function(e) {
         if(e && e.overrideMimeType) {
           e.overrideMimeType("application/json;charset=UTF-8");
         }
       },
       success: function(response){ // Ketika proses pengiriman berhasil
         $("#loading").hide(); // Sembunyikan loadingnya
         // set isi dari combobox kota
         // lalu munculkan kembali combobox kotanya
         $("#cabor2").html(response.list_cabor2).show();
       },
       error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
         alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
       }
     });
   });
 });
 </script>

<script>
 $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
   // Kita sembunyikan dulu untuk loadingnya
   $("#loading").hide();
   
   $("#kontingen3").change(function(){ // Ketika user mengganti atau memilih data provinsi
     $("#cabor3").hide(); // Sembunyikan dulu combobox kota nya
     $("#loading").show(); // Tampilkan loadingnya
   
     $.ajax({
       type: "POST", // Method pengiriman data bisa dengan GET atau POST
       url: "<?php echo base_url("kompetisi/listCaborKontingenKompetisi3"); ?>", // Isi dengan url/path file php yang dituju
       data: {kontingen3 : $("#kontingen3").val(),
               kompetisi3 : $("#kompetisi3").val()
           }, // data yang akan dikirim ke file yang dituju
       dataType: "json",
       beforeSend: function(e) {
         if(e && e.overrideMimeType) {
           e.overrideMimeType("application/json;charset=UTF-8");
         }
       },
       success: function(response){ // Ketika proses pengiriman berhasil
         $("#loading").hide(); // Sembunyikan loadingnya
         // set isi dari combobox kota
         // lalu munculkan kembali combobox kotanya
         $("#cabor3").html(response.list_cabor3).show();
       },
       error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
         alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
       }
     });
   });
 });
 </script>

