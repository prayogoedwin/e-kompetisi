 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <!-- <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li> -->
        </ul>
    </div>
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

  <!-- -------------- Column Center -------------- -->
  <div class="chute chute-center">

                       
                            <!-- -------------- Registration -------------- -->
                            <div class="allcp-form theme-primary tab-pane mw600" id="register" role="tabpanel">
                            <div class="panel">
                                <div class="panel-heading">
                                    <span class="panel-title">
                                      Form Tambah Kompetisi
                                    </span>
                                </div>
                                <!-- -------------- /Panel Heading -------------- -->

                                 <?php echo form_open_multipart('kompetisi/adds') ?>
                                    <div class="panel-body pn">

                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="text" name="nama_kompetisi" id="nama_kompetisi" class="gui-input"
                                                       placeholder="Nama kompetisi">
                                                <label for="nama" class="field-icon">
                                                    <i class="fa fa-cubes"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="file" name="icon" id="icon" class="gui-input"
                                                       placeholder="Icon kompetisi">
                                                <label for="icon" class="field-icon">
                                                    <i class="fa fa-file-image-o"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label for="comment" class="field prepend-icon">
                                            <textarea class="gui-textarea" id="deskripsi" name="diskripsi"
                                                        placeholder="Deskripsi"></textarea>
                                                <label for="comment" class="field-icon">
                                                    <i class="fa fa-align-justify"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="date" name="mulai" id="mulai" class="gui-input"
                                                       placeholder="Mulai">
                                                <label for="icon" class="field-icon">
                                                    <i class="fa fa-file-image-o"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="date" name="selesai" id="selesai" class="gui-input"
                                                       placeholder="Selesai">
                                                <label for="icon" class="field-icon">
                                                    <i class="fa fa-file-image-o"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                        <div class="section">
                                            <label for="email" class="field prepend-icon">
                                                <input type="text" name="lokasi" id="lokasi" class="gui-input"
                                                       placeholder="Lokasi">
                                                <label for="icon" class="field-icon">
                                                    <i class="fa fa-file-image-o"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- -------------- /section -------------- -->
                                            
                                        <div class="section">
                                            
                                            <div class="pull-right">
                                                <button type="submit" class="btn btn-bordered btn-primary">
                                                    Tambah kompetisi
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->

                                    </div>
                                    <!-- -------------- /Form -------------- -->
                                    <div class="panel-footer"></div>
                                <?php echo form_close();?>
                            </div>
                            <!-- -------------- /Panel -------------- -->
                        </div>
                        <!-- -------------- /Registration -------------- -->

                        

</div>
<!-- -------------- //End Column Center -------------- -->

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>



</body>
</html>


