<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <!-- <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li>
        </ul>
    </div> -->
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

        <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title "> Perolehan Medali <?=$rowdata->nama_kompetisi?></span>
                    </div>

                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <table border="0" style="width:100%" id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center">No</th>
                                    <th class="text-center">Cabor</th>
                                    <th class="text-center">Kelas</th>
                                    <th class="text-center">Medali</th>
                                    
                                   
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if($alldata == FALSE ){ ?>
                                    <tr class="bg-light">                                    
                                    <th class="text-center" colspan="9">Data Kosong</th>  
                                </tr>
                                <?php }else{ 
                                $no = 0;
                                foreach($alldata as $all): 
                                $no++;
                                ?>
                                <tr>
                                    <td class=""><?=$no?></td>
                                    <td class=""><?=nama_cabor($all->id_cabor)?></td>
                                    <td class=""><?=nama_kelas($all->id_kelas)?> <?=jenis_kelas($all->id_kelas)?></td>

                                    <td class="">

                                    <!-- Button trigger modal -->
                                

                                    <button type="button" class="btn btn-success btn-sm btn-add" data-placement="top" data-toggle="Tambah"  
                                        data-original-title="Tambah" 
                                        data-id_kompetisi="<?=$all->id_kompetisi?>"
                                        data-cabor="<?=nama_cabor($all->id_cabor)?>"
                                        data-id_cabor="<?=$all->id_cabor?>"
                                        data-kelas="<?=nama_kelas($all->id_kelas)?>"
                                        data-id_kelas="<?=$all->id_kelas?>"
                                    ><i class="fa fa-plus"></i></button>

                                    <br/>
                                    <?php
                                    $kelas = $this->db->query("SELECT a.* FROM e_kompetisi_medali a
                                    WHERE a.id_kompetisi = '$all->id_kompetisi'
                                    AND a.id_kelas = '$all->id_kelas'
                                    AND a.deleted_at IS NULL")->result();
                                    foreach($kelas AS $kls):
                                    ?>

                                    <p><?=nama_atlit($kls->id_atlit)?> (<?=nama_kontingen($kls->id_kontingen)?>) - <?=nama_medali($kls->id_medali)?>
                                    
                                    <a href="<?=base_url('kompetisi/hapus_by_number/'.$kls->id_kompetisi_medali)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin menghapus?')"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                     <span class="fa fa-trash pr5">
                                                    </button>
                                    </a>
                                    </p>

                                    <?php endforeach; ?>


                                   

                                    

                                    
                                    </td>
                                   
                   
                                    
                                </tr>
                                <?php endforeach;} ?>

                                </tbody>
                            </table>

                            <div class="topbar-right  mt5 mr35">
                            <a href="<?=base_url('kompetisi/cetak_medali_kontingen/'.$rowdata->id_kompetisi)?>"  onclick="return confirm('Cetak perolehan medali ?')" class="btn btn-success btn-sm ml10" title="Cetak Perolehan Medali"> 
                            <span class="fa fa-forward pr5"></span> Cetak Perolehan Medali</a>

                            <a href="<?=base_url('kompetisi/cetak_medali/'.$rowdata->id_kompetisi)?>"  onclick="return confirm('Cetak perolehan piagam ?')" class="btn btn-primary btn-sm ml10" title="Cetak Perolehan Piagam"> 
                            <span class="fa fa-forward pr5"></span> Cetak Perolehan Piagam</a>

                            <a href="<?=base_url('kompetisi/cetak_piagam/'.$rowdata->id_kompetisi)?>"  onclick="return confirm('Cetak piagam ?')" class="btn btn-warning btn-sm ml10" title="Cetak Piagam">
                            <span class="fa fa-forward pr5"></span> Cetak Piagam</a>
                        
                        </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>
<?php include(__DIR__ . "/../template/datatable.php"); ?>
<script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            // {
            //     extend: 'excelHtml5',
            //     messageTop: 'MEDAL - <?=$rowdata->nama_kompetisi?> - <?=nama_kontingen($kontingen)?>'
            // },
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "info": false,   
      "bLengthChange": false,
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "autoWidth": true,
      "scrollX": true,
      "autoWidth": true,
      "order": [[ 2, "asc" ]]
    });
});
</script>

<!-- Modal Edit -->
<?=form_open_multipart('kompetisi/adds_medali_by_kelas', 'method="POST" autocomplete="off" class="form-horizontal form-label-left" role="form" enctype="multipart/form-data"'); ?>
        <div class="modal fade" id="modal-add">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Tambah Medali
              </div>
              <div class="modal-body">
                        <input type="hidden" class="form-control" name="id" id="id">
                        <input type="hidden" class="form-control" name="id_kompetisi" id="id_kompetisi">
                        <input type="hidden" class="form-control" name="id_cabor" id="id_cabor">
                      
                        

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Cabor</label>
                                 <div class="col-sm-10">
                                 <input type="text" class="form-control" name="cabor" id="cabor" readonly>
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Kelas</label>
                                 <div class="col-sm-10">
                                 <input type="text" class="form-control" name="kelas" id="kelas" readonly>
                                </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Kontingen</label>
                            <div class="col-sm-10">                
                            <select required class="form-control select2" id="id_kontingen" name="id_kontingen" style="width: 100%;">
                            <option selected>Pilih Kontingen</option>
                            <?php foreach($kontingens as $ktns): ?>
                                <option value="<?=$ktns->id_kontingen?>"><?=nama_kontingen($ktns->id_kontingen)?></option>
                            <?php endforeach; ?>
                            
                            <select>
                            </div>
                        </div>

                        <input type="hidden" class="form-control" name="id_kelas" id="id_kelas">

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Atlit</label>
                            <div class="col-sm-10">                
                            <select required class="form-control select2" id="id_atlit" name="id_atlit" style="width: 100%;">
                            <option selected disabled>Pilih Atlit</option>
                            
                            <select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Medali</label>
                            <div class="col-sm-10">                
                            <select required class="form-control select2" id="medali" name="medali" style="width: 100%;">
                            <option value="1">Emas</option>
                            <option value="2">Perak</option>
                            <option value="3">Perunggu</option>
                           
                            
                            <select>
                            </div>
                        </div>


              </div>
              <div class="modal-footer">
                        <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah <i class="fa fa-send"></i></button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <?=form_close(); ?>
        <!-- End Modal Edit -->

    <script>
    $(document).ready(function(){
      $('#example1').on('click','.btn-add', function(ev){
      ev.preventDefault();
      $('#id').val($(this).data('id'));
      $('#id_kompetisi').val($(this).data('id_kompetisi'));
      $('#cabor').val($(this).data('cabor'));
      $('#id_cabor').val($(this).data('id_cabor'));
      $('#kelas').val($(this).data('kelas'));
      $('#id_kelas').val($(this).data('id_kelas'));
      $('#modal-add').modal();
    });

    });
  </script>

<script>
 $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
   // Kita sembunyikan dulu untuk loadingnya
   $("#loading").hide();
   
   $("#id_kontingen").change(function(){ // Ketika user mengganti atau memilih data provinsi
     $("#id_atlit").hide(); // Sembunyikan dulu combobox kota nya
     $("#loading").show(); // Tampilkan loadingnya
   
     $.ajax({
       type: "POST", // Method pengiriman data bisa dengan GET atau POST
       url: "<?php echo base_url("kompetisi/listAtlit"); ?>", // Isi dengan url/path file php yang dituju
       data: {
           id_kelas : $("#id_kelas").val(),
           id_kontingen : $("#id_kontingen").val()  
           }, // data yang akan dikirim ke file yang dituju
       dataType: "json",
       beforeSend: function(e) {
         if(e && e.overrideMimeType) {
           e.overrideMimeType("application/json;charset=UTF-8");
         }
       },
       success: function(response){ // Ketika proses pengiriman berhasil
         $("#loading").hide(); // Sembunyikan loadingnya
         // set isi dari combobox kota
         // lalu munculkan kembali combobox kotanya
         $("#id_atlit").html(response.list_atlit).show();
       },
       error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
         alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
       }
     });
   });
 });
 </script>



</body>
</html>


