<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    <!-- <div class="topbar-left">
        <ul class="nav nav-list nav-list-topbar pull-left">
            <li class="active">
                <a href="dashboard2.html">Overview</a>
            </li>
            <li>
                <a href="sales-stats-products.html">Products</a>
            </li>
            <li>
                <a href="sales-stats-purchases.html">Orders</a>
            </li>
            <li>
                <a href="sales-stats-clients.html">Clients</a>
            </li>
            <li>
                <a href="sales-stats-general-settings.html">Settings</a>
            </li>
        </ul>
    </div> -->
    <div class="topbar-right hidden-xs hidden-sm mt5 mr35">
       
        
    </div>
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">
<!-- -------------- Column Center -------------- -->
<div class="chute chute-center">

<div class="mw1000 center-block">

    <!-- -------------- Spec Form -------------- -->
    <div class="allcp-form">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">Form Cetak
                </div>
            </div>
            <div class="panel-body">
            
                  
                    

                    <!-- -------------- Multi Selects -------------- -->
                    <div class="row">
                        <div class="col-md-3">
                        <?php echo form_open('kompetisi/cetak_keikutsertaan/'.$this->uri->segment('3'), 'target="BLANK"') ?>
                        <input type="hidden" name="kompetisi" id="kompetisi" value="<?=$this->uri->segment('3')?>">
                            <div class="section">
                                <label class="field select">
                                    <select id="kontingen" name="kontingen">
                                        <option value="all">Kontingen</option>
                                        <?php foreach($kontingen as $k1): ?>
                                            <option value="<?=$k1->id_kontingen?>"><?=nama_kontingen($k1->id_kontingen)?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <i class="arrow"></i>
                                </label>
                            </div>

                            <div class="section">
                                <label class="field select">
                                    <select id="selectID"  disabled>
                                        
                                    </select>
                                    <i class="arrow double"></i>
                                </label>
                            </div>

                            <div class="section">
                                            
                                            <div class="pull-left">
                                                <button type="submit" class="btn btn-bordered btn-success">
                                                    Cetak Keikutsertaan
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->
                        <?php echo form_close();?>

                        </div>
                        <div class="col-md-3">
                        <?php echo form_open('kompetisi/cetak_by_number/'.$this->uri->segment('3'), 'target="BLANK"') ?>
                        <input type="hidden" name="id_kompetisi" id="id_kompetisi" value="<?=$this->uri->segment('3')?>">
                            <div class="section">
                                <label class="field select">
                                    <select id="id_kontingen" name="id_kontingen">
                                        <option value="">Kontingen</option>
                                        <?php foreach($kontingen as $k2): ?>
                                            <option value="<?=$k2->id_kontingen?>"><?=nama_kontingen($k2->id_kontingen)?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <i class="arrow double"></i>
                                </label>
                            </div>

                            <div class="section">
                                <label class="field select">
                                    <select id="selectID"  disabled>
                                        
                                    </select>
                                    <i class="arrow double"></i>
                                </label>
                            </div>

                            <div class="section">
                                            
                                            <div class="pull-left">
                                                <button type="submit" class="btn btn-bordered btn-warning">
                                                    Cetak By Number
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->
                                        <?php echo form_close();?>

                        </div>

                        <div class="col-md-3">
                        <?php echo form_open('kompetisi/cetak_by_name/'.$this->uri->segment('3'), 'target="BLANK"') ?>
                        <input type="hidden" name="kompetisi" id="kompetisi" value="<?=$this->uri->segment('3')?>">
                            <div class="section">
                                <label class="field select">
                                    <select id="id_kontingen" name="id_kontingen">
                                        <option value="%">Kontingen</option>
                                        <?php foreach($kontingen as $k3): ?>
                                            <option value="<?=$k3->id_kontingen?>"><?=nama_kontingen($k3->id_kontingen)?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <i class="arrow double"></i>
                                </label>
                            </div>

                            <div class="section">
                                <label class="field select">
                                    <select id="jenis" name="jenis">
                                       
                                        <option value="Atlit">Atlit</option>
                                        <option value="Pelatih">Pelatih</option>
                                    </select>
                                    <i class="arrow double"></i>
                                </label>
                            </div>

                            <div class="section">
                                            
                                            <div class="pull-left">
                                                <button type="submit" class="btn btn-bordered btn-danger">
                                                    Cetak By Name
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->
                            <?php echo form_close();?>

                        </div>

                        <div class="col-md-3">
                        <?php echo form_open('kompetisi/cetak_kartu_peserta/'.$this->uri->segment('3'), 'target="BLANK"') ?>
                        <input type="hidden" name="id_kompetisi" id="id_kompetisi" value="<?=$this->uri->segment('3')?>">
                            <div class="section">
                                <label class="field select">
                                    <select id="id_kontingen" name="id_kontingen">
                                        <option value="%">Kontingen</option>
                                        <?php foreach($kontingen as $k2): ?>
                                            <option value="<?=$k2->id_kontingen?>"><?=nama_kontingen($k2->id_kontingen)?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <i class="arrow double"></i>
                                </label>
                            </div>

                            <div class="section">
                                <label class="field select">
                                    <select id="jenis" name="jenis">
                                       
                                        <option value="Atlit">Atlit</option>
                                        <option value="Pelatih">Pelatih</option>
                                    </select>
                                    <i class="arrow double"></i>
                                </label>
                            </div>

                            <div class="section">
                                            
                                            <div class="pull-left">
                                                <button type="submit" class="btn btn-bordered btn-info">
                                                    Cetak Kartu Peserta
                                                </button>
                                            </div>
                                        </div>
                                        <!-- -------------- /section -------------- -->
                                        <?php echo form_close();?>

                        </div>

                    </div>
                </div>

            <div class="panel-body">
            <!-- -------------- Multi Selects -------------- -->
            <div class="row">
                <div class="col-md-3">
                <?php echo form_open('kompetisi/cetak_atlit_per_cabor/'.$this->uri->segment('3'), 'target="BLANK"') ?>
                <input type="hidden" name="kompetisi" id="kompetisi" value="<?=$this->uri->segment('3')?>">
                    <div class="section">
                        <label class="field select">
                            <select id="cabor" name="cabor">
                                
                                <?php foreach($cabor as $c): ?>
                                    <option value="<?=$c->id_cabor?>"><?=nama_cabor($c->id_cabor)?></option>
                                <?php endforeach; ?>
                            </select>
                            <i class="arrow"></i>
                        </label>
                    </div>

                    

                    <div class="section">
                                <label class="field select">
                                    <select id="jenis" name="jenis">
                                       
                                        <option value="Atlit">Atlit</option>
                                        <option value="Pelatih">Pelatih</option>
                                    </select>
                                    <i class="arrow double"></i>
                                </label>
                            </div>

                    <div class="section">
                                    
                                    <div class="pull-left">
                                        <button type="submit" class="btn btn-bordered btn-success">
                                            Cetak Per Cabor
                                        </button>
                                    </div>
                                </div>
                                <!-- -------------- /section -------------- -->
                <?php echo form_close();?>

                </div>
            

            </div>
            </div>

        </div>

        </section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>

<script>
  $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
    // Kita sembunyikan dulu untuk loadingnya
    $("#loading").hide();
    
    $("#kontingen").change(function(){ // Ketika user mengganti atau memilih data provinsi
      $("#cabor").hide(); // Sembunyikan dulu combobox kota nya
      $("#loading").show(); // Tampilkan loadingnya
    
      $.ajax({
        type: "POST", // Method pengiriman data bisa dengan GET atau POST
        url: "<?php echo base_url("kompetisi/listCaborKontingenKompetisi"); ?>", // Isi dengan url/path file php yang dituju
        data: {kontingen : $("#kontingen").val(),
                kompetisi : $("#kompetisi").val()
            }, // data yang akan dikirim ke file yang dituju
        dataType: "json",
        beforeSend: function(e) {
          if(e && e.overrideMimeType) {
            e.overrideMimeType("application/json;charset=UTF-8");
          }
        },
        success: function(response){ // Ketika proses pengiriman berhasil
          $("#loading").hide(); // Sembunyikan loadingnya
          // set isi dari combobox kota
          // lalu munculkan kembali combobox kotanya
          $("#cabor").html(response.list_cabor).show();
        },
        error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
          alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
        }
      });
    });
  });
  </script>

