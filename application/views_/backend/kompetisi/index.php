<?php include(__DIR__ . "/../template/head_datatable.php"); ?>

 <!-- -------------- Main Wrapper -------------- -->
 <section id="content_wrapper">



<!-- -------------- Topbar -------------- -->
<header id="topbar" class="ph10">
    
</header>
<!-- -------------- /Topbar -------------- -->

<!-- -------------- Content -------------- -->
<section id="content" class="table-layout animated fadeIn">

    <!-- -------------- Column Center -------------- -->
    <div class="chute chute-center">

        <!-- -------------- Products Status Table -------------- -->
        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title "> Daftar Kompetisi</span>
                    </div>
                    <div class="topbar-right  mt5 mr35">
                        <a href="<?=base_url('kompetisi/add')?>" class="btn btn-primary btn-sm ml10" title="New Order">
                            <span class="fa fa-plus pr5"></span><span class="fa fa-file-o pr5"></span>Tambah kompetisi</a>
                        
                    </div>

                    <div class="panel-body pn">
                        <div class="table-responsive">
                            <table border="0" style="width:100%" id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr class="bg-light">                                    
                                    <th class="text-center">No</th>
                                    <th class="text-center">Nama kompetisi</th>
                                    <th class="text-center">Tahap</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                if($alldata == FALSE ){ ?>
                                    <tr class="bg-light">                                    
                                    <th class="text-center" colspan="4">Data Kosong</th>
                                    
                                </tr>
                                <?php }else{ 
                                $no = 0;
                                foreach($alldata as $all): 
                                $no++
                                ?>
                                <tr>
                                    <td class=""><?=$no?></td>
                                    <td class=""><?=$all->nama_kompetisi?></td>
                                    <td class="">
                                    
                                    <?php if($all->tahap == 1){ ?>

                                    <a href="<?=base_url('kompetisi/keikutsertaan/'.$all->id_kompetisi)?>"><button type="button"
                                                class="btn btn-success br2 btn-xs fs12"
                                                aria-expanded="false">
                                                 Keikutsertaan
                                                </button>
                                    </a>


                                    <?php }elseif($all->tahap == 2){ ?>

                                    <a href="<?=base_url('kompetisi/keikutsertaan/'.$all->id_kompetisi)?>"><button type="button"
                                                class="btn btn-success br2 btn-xs fs12"
                                                aria-expanded="false">
                                                 Keikutsertaan
                                                </button>
                                    </a>

                                    <a href="<?=base_url('kompetisi/by_number/'.$all->id_kompetisi)?>"><button type="button"
                                                class="btn btn-warning br2 btn-xs fs12"
                                                aria-expanded="false">
                                                 By Number
                                                </button>
                                    </a>


                                    <?php }elseif($all->tahap == 3){ ?>

                                        <a href="<?=base_url('kompetisi/keikutsertaan/'.$all->id_kompetisi)?>"><button type="button"
                                                class="btn btn-success br2 btn-xs fs12"
                                                aria-expanded="false">
                                                 Keikutsertaan
                                                </button>
                                    </a>

                                    <a href="<?=base_url('kompetisi/by_number/'.$all->id_kompetisi)?>"><button type="button"
                                                class="btn btn-warning br2 btn-xs fs12"
                                                aria-expanded="false">
                                                 By Number
                                                </button>
                                    </a>

                                    <a href="<?=base_url('kompetisi/by_name/'.$all->id_kompetisi)?>"><button type="button"
                                                class="btn btn-danger br2 btn-xs fs12"
                                                aria-expanded="false">
                                                 By Name
                                                </button>
                                    </a>

                                    <?php }elseif($all->tahap == 4){ ?>

                                    <a href="<?=base_url('kompetisi/keikutsertaan/'.$all->id_kompetisi)?>"><button type="button"
                                            class="btn btn-success br2 btn-xs fs12"
                                            aria-expanded="false">
                                             Keikutsertaan
                                            </button>
                                    </a>

                                    <a href="<?=base_url('kompetisi/by_number/'.$all->id_kompetisi)?>"><button type="button"
                                            class="btn btn-warning br2 btn-xs fs12"
                                            aria-expanded="false">
                                             By Number
                                            </button>
                                    </a>

                                    <a href="<?=base_url('kompetisi/by_name/'.$all->id_kompetisi)?>"><button type="button"
                                            class="btn btn-danger br2 btn-xs fs12"
                                            aria-expanded="false">
                                             By Name
                                            </button>
                                    </a>

                                    <a href="<?=base_url('kompetisi/keabsahan/'.$all->id_kompetisi)?>"><button type="button"
                                            class="btn btn-system br2 btn-xs fs12"
                                            aria-expanded="false">
                                             Keabsahan
                                            </button>
                                    </a>

                                    <a href="<?=base_url('kompetisi/cetak/'.$all->id_kompetisi)?>"><button type="button"
                                            class="btn btn-alert br2 btn-xs fs12"
                                            aria-expanded="false">
                                             Cetak
                                            </button>
                                    </a>

                                    <a href="<?=base_url('kompetisi/medali/'.$all->id_kompetisi)?>"><button type="button"
                                            class="btn btn-dark br2 btn-xs fs12"
                                            aria-expanded="false">
                                             Medali
                                            </button>
                                    </a>

                                    <?php }else{ echo nama_tahap($all->tahap); }?>
                                    
                                    </td>
                                    <td>
                                    <?php if($all->status == 'ON'){?>
                                        <a href="<?=base_url('kompetisi/rubah/'.$all->id_kompetisi)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin menutup kompetisi ?')"
                                                    class="btn btn-primary br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                    ON <span class="fa fa-refresh pr5">
                                                    </button>
                                    </a>
                                    <?php }else{?>
                                        <a href="<?=base_url('kompetisi/rubah/'.$all->id_kompetisi)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin membuka kompetisi ?')"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                    OFF <span class="fa fa-refresh pr5">
                                                    </button>
                                    </a>
                                    
                                    <?php } ?>
                                    </td>
                                    <td class="text-center">
                                    
                                    

                                    <a href="<?=base_url('kompetisi/kontingen/'.$all->id_kompetisi)?>"><button type="button"
                                                    class="btn btn-success br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                      Kontingen
                                                    </button>
                                    </a>

                                    <a href="<?=base_url('kompetisi/cabor/'.$all->id_kompetisi)?>"><button type="button"
                                                    class="btn btn-alert br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                      Cabor
                                                    </button>
                                    </a>

                                    <!-- <a href="<?=base_url('kompetisi/edit')?>"><button type="button"
                                                    class="btn btn-info br2 btn-xs fs12"
                                                     aria-expanded="false">
                                                     <span class="fa fa-edit pr5">
                                                    </button>
                                    </a> -->

                                    <a href="<?=base_url('kompetisi/hapus/'.$all->id_kompetisi)?>"><button type="button"
                                                    onclick="return confirm('Anda yakin ingin menghapus?')"
                                                    class="btn btn-danger br2 btn-xs fs12"
                                                    aria-expanded="false">
                                                     <span class="fa fa-trash pr5">
                                                    </button>
                                    </a>

                                    
                                    </td>
                                    
                                </tr>
                                <?php endforeach;} ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      

</section>
<!-- -------------- /Content -------------- -->

</section>

<?php include(__DIR__ . "/../template/footer.php"); ?>
<?php include(__DIR__ . "/../template/datatable.php"); ?>
<script>
$(document).ready(function() {
    $('#example2').DataTable()
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            //'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            //'pdfHtml5'
        ],
      "info": false,   
      "bLengthChange": false,
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "autoWidth": true,
      "scrollX": true,
      "autoWidth": true
    });
});
</script>



</body>
</html>


