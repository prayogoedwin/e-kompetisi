<!DOCTYPE html>
<html>

<head>
    <!--   Meta and Title   -->
    <meta charset="utf-8">
    <title>Halaman Login e-Kompetisi</title>
    <!--   Fonts   -->
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic' rel='stylesheet'type='text/css'>

    <!--   CSS - theme   -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/all/assets/skin/default_skin/css/theme.css">

    <!--   CSS - allcp forms   -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/all/assets/allcp/forms/css/forms.css">

    <!--   Favicon   -->
    <link rel="shortcut icon" href="<?=base_url()?>assets/all/assets/img/favicon.ico">

    <!--   IE8 HTML5 support    -->
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="utility-page sb-l-c sb-r-c">

<!--   Body Wrap    -->
<div id="main" class="animated fadeIn">

    <!--   Main Wrapper   -->
    <section id="content_wrapper">

        <div id="canvas-wrapper">
            <canvas id="demo-canvas"></canvas>
        </div>

        <!--   Content   -->
        <section id="content">
            <!--   Login Form   -->
            <div class="allcp-form text-center theme-primary mw320" id="login">
                <div class="panel mw320">
                    <!-- <form method="post" action="/" id="form-login"> -->
                    <?php echo form_open('sys/act_login') ?>
                    <img  src="<?=base_url()?>assets/jateng.png" alt="avatar" width="100">
                    <h2 class="text-center">E-Kompetisi</h2>
                        <div class="panel-body pn mv10">

                            <div class="section">
                                <label for="username" class="field prepend-icon">
                                    <input type="text" name="username" id="username" class="gui-input"
                                           placeholder="Username">
                                    <label for="username" class="field-icon">
                                        <i class="fa fa-user"></i>
                                    </label>
                                </label>
                            </div>
                            <!--   /section   -->

                            <div class="section">
                                <label for="password" class="field prepend-icon">
                                    <input type="text" name="password" id="password" class="gui-input"
                                           placeholder="Password">
                                    <label for="password" class="field-icon">
                                        <i class="fa fa-lock"></i>
                                    </label>
                                </label>
                            </div>
                            <!--   /section   -->

                            <div class="section">
                            <div class="smart-widget sm-left sml-80">
                                <label for="verify" class="field prepend-icon">
                                    <input type="text" name="verify" id="verify" class="gui-input"
                                            placeholder="Random captcha">
                                    <input type="hidden" name="verify2" value="<?=$rand?>">
                                    <label for="verify" class="field-icon">
                                        <i class="fa fa-lock"></i>
                                    </label>
                                </label>
                                <label for="verify" class="button"><?=$rand?></label>
                            </div>
                            <!--   /Block Widget   -->
                        </div>
                        <!--   /section   -->

                            <div class="section">
                                <div class="bs-component pull-left pt5">
                                    <div class="radio-custom radio-primary mb5 lh25">

                                    </div>
                                </div>
                                <button type="submit" class="btn btn-bordered btn-primary pull-right">Log in</button>
                            </div>
                            <!--   /section   -->

                        </div>
                        <?php echo form_close() ?>
                        <!--   /Form   -->
                    </form>
                </div>
                <!--   /Panel   -->
            </div>
            <!--   /Spec Form   -->

        </section>
        <!--   /Content   -->

    </section>
    <!--   /Main Wrapper   -->

</div>
<!--   /Body Wrap    -->

<!--   Scripts   -->

<!--   jQuery   -->
<script src="<?=base_url()?>assets/all/assets/js/jquery/jquery-1.11.3.min.js"></script>
<script src="<?=base_url()?>assets/all/assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!--   CanvasBG JS   -->
<script src="<?=base_url()?>assets/all/assets/js/plugins/canvasbg/canvasbg.js"></script>

<!--   Theme Scripts   -->
<script src="<?=base_url()?>assets/all/assets/js/utility/utility.js"></script>
<script src="<?=base_url()?>assets/all/assets/js/demo/demo.js"></script>
<script src="<?=base_url()?>assets/all/assets/js/main.js"></script>

<!--   Page JS   -->
<script type="text/javascript">
    jQuery(document).ready(function () {

        "use strict";

        // Init Theme Core
        Core.init();

        // Init Demo JS
        Demo.init();

        // Init CanvasBG
        CanvasBG.init({
            Loc: {
                x: window.innerWidth / 5,
                y: window.innerHeight / 10
            }
        });

    });
</script>

<!--   /Scripts   -->

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<?php
$info= $this->session->flashdata('info');
$pesan= $this->session->flashdata('message');

if($info == 'success'){ ?>
  <script>
      swal({
          title: "Berhasil",
          text: "<?php echo $pesan; ?>",
          timer: 1500,
          showConfirmButton: false,
          type: 'success'
      });
  </script>
  <?php    }elseif($info == 'danger'){ ?>
  <script>
      swal({
          title: "Gagal",
          text: "<?php echo $pesan; ?>",
          timer: 1500,
          showConfirmButton: false,
          type: 'danger'
      });
  </script>
  <?php  }else{ } ?>

</body>
</html>
