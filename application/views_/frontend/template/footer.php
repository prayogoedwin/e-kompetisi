<footer id="footer-style-1">
    	<div class="container">

      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            	<div class="widget">
                	<div class="title">
                        <h3>Lokasi Kami</h3>
                    </div><!-- end title -->

                    <iframe width="500" height="300" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBRXvxArRmVoFykTXCXzym5B_ie4XHzSi0&q=Dinas+Kepemudaan+Olahraga+dan+Pariwisata,Jawa+Tengah" allowfullscreen></iframe>
                   
                </div><!-- end widget -->
            </div><!-- end columns -->
        	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            	<div class="widget">
              <div class="title">
                        <h3>Alamat Kami</h3>
                    </div><!-- end title -->
                	<p><strong>Dinas Pemuda, Olahraga dan Pariwisata Provinsi Jawa Tengah</strong><br>Jalan Ki Mangunsarkoro No.12, Semarang, Jawa Tengah 50241</p>
                    <div class="social-icons">
                        <span><a data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#"><i class="fa fa-facebook"></i></a></span>
                        <span><a data-toggle="tooltip" data-placement="bottom" title="Google Plus" href="#"><i class="fa fa-google-plus"></i></a></span>
                        <span><a data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#"><i class="fa fa-twitter"></i></a></span>
                        <span><a data-toggle="tooltip" data-placement="bottom" title="Youtube" href="#"><i class="fa fa-youtube"></i></a></span>
                        <span><a data-toggle="tooltip" data-placement="bottom" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a></span>
                        <span><a data-toggle="tooltip" data-placement="bottom" title="Dribbble" href="#"><i class="fa fa-dribbble"></i></a></span>
                    </div><!-- end social icons -->
                </div><!-- end widget -->
            </div><!-- end columns -->
        	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            	<div class="widget">
                	<div class="title">
                        <h3>Twitter Kami</h3>
                    </div><!-- end title -->
                    <ul class="twitter_feed">
					<a class="twitter-timeline" data-height="400" href="https://twitter.com/disporaparjtg">Tweets by disporaparjtg</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
					</ul><!-- end twiteer_feed --> 
                </div><!-- end widget -->
            </div><!-- end columns -->
        
        	
    	</div><!-- end container -->
    </footer><!-- end #footer-style-1 -->    

<div id="copyrights">
    	<div class="container">
			<div class="col-lg-5 col-md-6 col-sm-12">
            	<div class="copyright-text">
                    <p>Copyright © 2019 - Dinas Pemuda, Olahraga dan Pariwisata Provinsi Jawa Tengah</p>
                </div><!-- end copyright-text -->
			</div><!-- end widget -->
			<div class="col-lg-7 col-md-6 col-sm-12 clearfix">
				<div class="footer-menu">
                    <ul class="menu">
                        <li class="active"><a href="<?=base_url('sys/login');?>">Masuk</a></li>
                        
                    </ul>
                </div>
			</div><!-- end large-7 --> 
        </div><!-- end container -->
    </div><!-- end copyrights -->
    
	<div class="dmtop">Scroll to Top</div>
        
  <!-- Main Scripts-->
  <script src="<?php echo base_url() ?>assets/joll/js/jquery.js"></script>
  <script src="<?php echo base_url() ?>assets/joll/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url() ?>assets/joll/js/menu.js"></script>
  <script src="<?php echo base_url() ?>assets/joll/js/owl.carousel.min.js"></script>
  <script src="<?php echo base_url() ?>assets/joll/js/jquery.parallax-1.1.3.js"></script>
  <script src="<?php echo base_url() ?>assets/joll/js/jquery.simple-text-rotator.js"></script>
  <script src="<?php echo base_url() ?>assets/joll/js/wow.min.js"></script>
  <script src="<?php echo base_url() ?>assets/joll/js/custom.js"></script>
  
  <script src="<?php echo base_url() ?>assets/joll/js/jquery.isotope.min.js"></script>
  <script src="<?php echo base_url() ?>assets/joll/js/custom-portfolio.js"></script>

  <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
  <script type="text/javascript" src="<?php echo base_url() ?>assets/joll/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/joll/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
  <script type="text/javascript">
	var revapi;
	jQuery(document).ready(function() {
		revapi = jQuery('.tp-banner').revolution(
		{
			delay:9000,
			startwidth:1170,
			startheight:500,
			hideThumbs:10,
			fullWidth:"on",
			forceFullWidth:"on"
		});
	});	//ready
  </script>
      
  <!-- Royal Slider script files -->
  <script src="<?php echo base_url() ?>assets/joll/royalslider/jquery.easing-1.3.js"></script>
  <script src="<?php echo base_url() ?>assets/joll/royalslider/jquery.royalslider.min.js"></script>
  <script>
	jQuery(document).ready(function($) {
	  var rsi = $('#slider-in-laptop').royalSlider({
		autoHeight: false,
		arrowsNav: false,
		fadeinLoadedSlide: false,
		controlNavigationSpacing: 0,
		controlNavigation: 'bullets',
		imageScaleMode: 'fill',
		imageAlignCenter: true,
		loop: false,
		loopRewind: false,
		numImagesToPreload: 6,
		keyboardNavEnabled: true,
		autoScaleSlider: true,  
		autoScaleSliderWidth: 486,     
		autoScaleSliderHeight: 315,
	
		/* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
		imgWidth: 792,
		imgHeight: 479
	
	  }).data('royalSlider');
	  $('#slider-next').click(function() {
		rsi.next();
	  });
	  $('#slider-prev').click(function() {
		rsi.prev();
	  });
	});
  </script>
    
    
</body>
</html>