<section class="post-wrapper-top jt-shadow clearfix">
		<div class="container">
			<div class="col-lg-12">
				<h2>Galeri Foto</h2>
                <ul class="breadcrumb pull-right">
				    <li><a href="<?=base_url()?>">Home</a></li>
                    <li>Galeri Foto</li>
                </ul>
			</div>
		</div>
	</section>
	<section class="white-wrapper">
    	<div class="container">
        	<div class="general-title">
            	<h2>Galeri Foto</h2>
                <hr>
            </div>
		</div>
		<div class="text-center clearfix">
			
		</div>
		<div id="boxed-portfolio" class="portfolio_wrapper padding-top">
           
            <?php foreach ($gambar as $g) { ?>
                
                <div class="col-lg-3 col-sm-6 col-md-3 col-xs-12 logo">
                <div class="portfolio_item all">
                    <div class="entry">
                        <img src="<?=base_url($g->foto)?>" alt="<?=$g->judul?>" class="img-responsive">
                        <div class="magnifier">
                            <div class="buttons">
                            <a href="<?=base_url($g->foto)?>" rel="prettyPhoto[pp_gal]"><span class="fa fa-search"></span></a>
                                <h3><?=$g->judul?>'</h3>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            <?php }?>
            <!-- <a class="st btn btn-default" rel="bookmark" href="portfolio-single-sidebar.html">Lihat Detail</a> -->
		
        </div>
		<div class="clearfix"></div>
		<!-- <div class="buttons padding-top text-center">
			<a href="portfolio-masonry.html" class="btn btn-primary btn-lg" title="">Lihat Semua Galeri</a>
		</div> -->
    </section>

<?php include(__DIR__ . "/template/footer.php"); ?>

<link rel="stylesheet" href="<?=base_url()?>assets/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
<script src="<?=base_url()?>assets/prettyPhoto/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" charset="utf-8">
  $(document).ready(function(){
    $("a[rel^='prettyPhoto']").prettyPhoto();
  });
</script>