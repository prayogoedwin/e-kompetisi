<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adm_artikel extends CI_Controller{
    function __construct(){
        parent::__construct();
        if (!$this->session->sess_member['levels']==1) {
            redirect(base_url('auth'));
        }
        $this->load->model('admin_model');
        $this->content['data']['title_page'] = 'Berita';
    }

    function index()
    {
        $this->content['data']['title'] = "Tabel Berita";
        $this->content['artikel'] = $this->admin_model->get_artikel();
        $this->content['load'] = array("module/artikel-table");
        $this->load->view('contentadm',$this->content);
    }

    function input()
    {
        $this->content['data']['title'] = "Tambah Data";
        if ($this->input->post()) {
            $dataInput['judul']     = $this->input->post('judul');
            $dataInput['konten']    = $this->input->post('konten');
            $dataInput['tgl']       = $this->input->post('tgl');
            $dataInput['url']       = create_link($this->input->post('judul')).'-'.rand(0,9);
            $dataInput['penulis']   = $this->input->post('penulis');
            $dataInput['publish']   = 1;
            $proses = $this->admin_model->tambah_artikel($dataInput);

            if ($proses == TRUE) { ?>
                <script>alert('tambah data berhasil');window.location.href = "<?php echo base_url()?>adm_artikel"</script>
            <?php } else { ?>
                <script>alert('Error, maaf terjadi kesalahan proses tambah data!');window.location.href = "<?php echo base_url()?>adm_artikel"</script>
            <?php }
        }
        $this->content['load'] = array("module/artikel-form");
        $this->load->view('contentadm',$this->content);
    }

    function edit($id = null)
    {
        $this->content['data']['title'] = "Ubah Data";
        $this->content['artikel'] = $this->admin_model->get_artikel_by_id($id);
        if ($this->input->post()) {
            $dataInput['judul']     = $this->input->post('judul');
            $dataInput['konten']    = $this->input->post('konten');
            $dataInput['tgl']       = $this->input->post('tgl');
            $dataInput['url']       = create_link($this->input->post('judul')).'-'.rand(0,9);
            $dataInput['penulis']   = $this->input->post('penulis');
            $dataInput['publish']   = 1;
            $dataInput['update_at'] = date('Y-m-d H:i:s');
            $proses = $this->admin_model->update_artikel($dataInput,$this->input->post('id'));

            if ($proses == TRUE) { ?>
                <script>alert('ubah data berhasil');window.location.href = "<?php echo base_url()?>adm_artikel"</script>
            <?php } else { ?>
                <script>alert('Error, maaf terjadi kesalahan proses ubah data!');window.location.href = "<?php echo base_url()?>adm_artikel"</script>
            <?php }
        }
        $this->content['load'] = array("module/artikel-form");
        $this->load->view('contentadm',$this->content);
    }

    function detail($id)
    {
        $this->content['data']['title'] = "Detail Berita";
        $this->content['artikel'] = $this->admin_model->get_artikel_by_id($id);

        $this->content['load'] = array("module/artikel-detail");
        $this->load->view('contentadm',$this->content);
    }

    function delete($id = null)
    {
        // delete peserta
        $process = $this->admin_model->delete_artikel($id);
        // cek proses delete berhasil atau tidak
        if ($process) {
            // jika berhasil
            ?><script>alert('berhasil menghapus data');window.location.href = "<?php echo base_url()?>adm_artikel"</script><?php
        } else {
            // jika gagal
            ?><script>alert('maaf, terjadi kesalahan proses menghapus!');window.location.href = "<?php echo base_url()?>adm_artikel"</script><?php
        }
        
    }
}