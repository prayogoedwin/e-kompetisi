<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    /**
     * Code From AstamaTechnology.
     * Web Developer
     * @author      Abu Umar, S.Kom
     * @package     Formatter
     * @copyright   Copyright (c) 2018 AstamaTechnology
     * @version     1.0, 1 September 2018
     * Email        abuumarsg@gmail.com
     * Phone        (+62) 85725951044
     */

class Formatter {
    
    protected $CI;
    public function __construct()
    {
        $this->CI =& get_instance();
    }
	//count work time
    public function getDateNow() 
    {
        $date=gmdate("Y-m-d H:i:s", time() + 3600*(7));
        return $date;
    }
	//date format
    public function getMonth()
    {
        $month=[
            '01'=>'Januari',
            '02'=>'Februari',
            '03'=>'Maret',
            '04'=>'April',
            '05'=>'Mei',
            '06'=>'Juni',
            '07'=>'Juli',
            '08'=>'Agustus',
            '09'=>'September',
            '10'=>'Oktober',
            '11'=>'November',
            '12'=>'Desember',
        ];
        return $month;
    }
    public function getMonthID($key)
    {
        return $this->getVarFromArrayKey($key,$this->getMonth());
    }
    public function getYear()
    {
        $first=2018;
        $end=date('Y', strtotime(date('Y',strtotime($this->getDateNow()))));
        $date=range($first,$end);
        $year=[];
        foreach ($date as $d) {
            $year[$d]=$d;
        }
        return $year;
    }
    public function getYearAll()
    {
        $first=1990;
        $end=date('Y', strtotime(date('Y',strtotime($this->getDateNow())) . ' +2 year'));
        $date=range($first,$end);
        $year=[];
        foreach ($date as $d) {
            $year[$d]=$d;
        }
        return $year;
    }
	public function getDateFormatDb($date)
	{
        if(empty($date)) 
            return null;
		$date=explode('/', $date);
        // if ($date[0] > 12) {
            $new_date=$date[2].'-'.$date[1].'-'.$date[0];
        // }else{
            // $new_date=$date[2].'-'.$date[0].'-'.$date[1];
        // }		
		return $new_date;
	}
	public function getDateFormatUser($date)
	{
        if(empty($date)) 
            return null;
		$date=explode('-', date('Y-m-d',strtotime($date)));
		$new_date=$date[2].'/'.$date[1].'/'.$date[0];
		return $new_date;
	}
	public function getDateMonthFormatUser($date)
	{
        if(empty($date)) 
            return null;
		$date1=explode('-', date('Y-m-d',strtotime($date)));
		$new_date=$date1[2].' '.$this->getNameOfMonth($date1[1]).' '.$date1[0];
		return $new_date;
	}
	public function getDayDateFormatUserId($date)
	{
        if(empty($date)) 
            return null;
		$date1=explode('-', date('Y-m-d',strtotime($date)));
		$new_date=$this->getNameOfDay($date).', '.$date1[2].' '.$this->getNameOfMonth($date1[1]).' '.$date1[0];
		return $new_date;
	}
	public function getDateTimeFormatDb($datetime)
	{
        if(empty($datetime)) 
            return null;
		$datetime=explode(' ', $datetime);
		$date=explode('/', $datetime[0]);
		if (isset($datetime[1])) {
			$time=$datetime[1];
		}else{
			$time='00:00:00';
		}
		$new_datetime=$date[2].'-'.$date[1].'-'.$date[0].' '.$time;
		return $new_datetime;
	}
	public function getDateTimeFormatUser($datetime,$second=null)
	{
        if(empty($datetime)) 
            return null;
        if(empty($second)){
            $datetime=explode(' ', date('Y-m-d H:i:s',strtotime($datetime)));
        }else{
            $datetime=explode(' ', date('Y-m-d H:i',strtotime($datetime)));
        }
		$date=explode('-', $datetime[0]);
		if (isset($datetime[1])) {
			$time=$datetime[1];
		}else{
            if(empty($second)){
                $time='00:00:00';                
            }else{
                $time='00:00';
            }
		}
		$new_datetime=$date[2].'/'.$date[1].'/'.$date[0].' '.$time;
		return $new_datetime;
	}
	public function getDateTimeMonthFormatUser($datetime,$second=null)
	{
        if(empty($datetime)) 
            return null;
		// $datetime=explode(' ', date('Y-m-d H:i:s',strtotime($datetime)));
        if(empty($second)){
            $datetime=explode(' ', date('Y-m-d H:i:s',strtotime($datetime)));
        }else{
            $datetime=explode(' ', date('Y-m-d H:i',strtotime($datetime)));
        }
		$date=explode('-', $datetime[0]);
		if (isset($datetime[1])) {
			$time=$datetime[1];
		}else{
            if(empty($second)){
                $time='00:00:00';                
            }else{
                $time='00:00';
            }
		}
		$new_datetime=$date[2].' '.$this->getNameOfMonth($date[1]).' '.$date[0].' '.$time;
		return $new_datetime;
	}
	public function getNameOfDay($date){
        if(empty($date)) 
            return null;
        $name_day = date('l',strtotime($date));
        $day = '';
        $day = ($name_day=='Sunday')?'Minggu':$day;
        $day = ($name_day=='Monday')?'Senin':$day;
        $day = ($name_day=='Tuesday')?'Selasa':$day;
        $day = ($name_day=='Wednesday')?'Rabu':$day;
        $day = ($name_day=='Thursday')?'Kamis':$day;
        $day = ($name_day=='Friday')?'Jumat':$day;
        $day = ($name_day=='Saturday')?'Sabtu':$day;
        return $day;
    }
    public function getNameOfMonth($inputmonth)
    {
        if(empty($inputmonth)) 
            return null;
        $return = null;
        $month = strtolower(trim($inputmonth));
        switch($month){
            case '1' : $return = 'Januari'; break;
            case '01' : $return = 'Januari'; break;
            case 'januari' : $return = 'Januari'; break;
            case 'january' : $return = 'Januari'; break;
            case '2' : $return = 'Februari'; break;
            case '02' : $return = 'Februari'; break;
            case 'februari' : $return = 'Februari'; break;
            case 'february' : $return = 'Februari'; break;
            case '3' : $return = 'Maret'; break;
            case '03' : $return = 'Maret'; break;
            case 'maret' : $return = 'Maret'; break;
            case 'march' : $return = 'Maret'; break;
            case '4' : $return = 'April'; break;
            case '04' : $return = 'April'; break;
            case 'april' : $return = 'April'; break;
            case '5' : $return = 'Mei'; break;
            case '05' : $return = 'Mei'; break;
            case 'may' : $return = 'Mei'; break;
            case '6' : $return = 'Juni'; break;
            case '06' : $return = 'Juni'; break;
            case 'juni' : $return = 'Juni'; break;
            case 'june' : $return = 'Juni'; break;
            case '7' : $return = 'Juli'; break;
            case '07' : $return = 'Juli'; break;
            case 'juli' : $return = 'Juli'; break;
            case 'july' : $return = 'Juli'; break;
            case '8' : $return = 'Agustus'; break;
            case '08' : $return = 'Agustus'; break;
            case 'agt' : $return = 'Agustus'; break;
            case 'agu' : $return = 'Agustus'; break;
            case 'aug' : $return = 'Agustus'; break;
            case 'agustus' : $return = 'Agustus'; break;
            case 'august' : $return = 'Agustus'; break;
            case '9' : $return = 'September'; break;
            case '09' : $return = 'September'; break;
            case 'september' : $return = 'September'; break;
            case '10' : $return = 'Oktober'; break;
            case 'oct' : $return = 'Oktober'; break;
            case 'oktober' : $return = 'Oktober'; break;
            case 'october' : $return = 'Oktober'; break;
            case '11' : $return = 'November'; break;
            case 'nov' : $return = 'November'; break;
            case 'nopember' : $return = 'November'; break;
            case 'november' : $return = 'November'; break;
            case '12' : $return = 'Desember'; break;
            case 'dec' : $return = 'Desember'; break;
            case 'desember' : $return = 'Desember'; break;
            case 'december' : $return = 'Desember'; break;
            default : $return = $inputmonth; break;
        }
        return $return;
    }
    public function timeFormatUser($var)
    {
        if(empty($var))
            return $this->getMark();
        $var=substr($var,0, -3);
        return $var;
    }
    public function timeFormatDb($var)
    {
        if(empty($var))
            return null;
        $var=$var.':00';
        return $var;
    }
    public function limit_words($string, $word_limit=10)
    {
        $words = explode(" ",$string);
        return implode(" ",array_splice($words,0,$word_limit));
    }
    public function jam($time,$int,$op)
    {
        if ($op == '-') {
           $jam=strtotime($time) - (60*60*$int); 
        }else{
            $jam=strtotime($time) + (60*60*$int);
        }
        return date('H:i:s',$jam);
    }
    public function getMark($usage = null)
    {
        $return='<i class="fa fa-times-circle" style="color:red" data-toggle="tooltip" title="Unknown"></i> ';
        switch($usage){
            case 'danger' : $return = '<i class="fa fa-times-circle" style="color:red"></i> '; break;
        }
        return $return;
    }
    public function getVarFromArrayKey($key,$pack)
    {
        if (!isset($pack[$key])) 
            return $this->getMark('danger');
        return $pack[$key];
    }
    public function listProgress()
    {
        $pack=[
            '1'=>'TU Pimpinan upload file & kode surat',
            '2'=>'TU SUBBAG menerima & melengkapi surat',
            '3'=>'KASUBBAG menerima & memilih Konseptor untuk membuat naskah',
            '4'=>'Konseptor menerima tugas',
            '5'=>'Konseptor membuat naskah & KASUBBAG menerima naskah',
            '6'=>'KASUBBAG mengajukan ke KABAG & KABAG menerima naskah',
            '7'=>'KABAG mengirim naskah ke TU SUBBAG & TU SUBBAG menerima naskah',
            '8'=>'TU SUBBAG mengirim naskah ke Kabiro & Kabiro menerima naskah',
            '9'=>'Kabiro mengirim ke TU Pimpinan & TU Pimpinan Menerima naskah',
            '10'=>'TU Pimpinan telah mem-publish Naskah',
            '11'=>'TU Pimpinan telah membatalkan publish naskah',
        ];
        return $pack;
    }
    public function getProgress($key)
    {
        return $this->getVarFromArrayKey($key,$this->listProgress());
    }
    public function lingkupAsisten()
    {
        $pack=[
            '1'=>'Asisten I',
            '2'=>'Asisten II',
            '3'=>'Asisten III',
        ];
        return $pack;
    }
    public function getlingkupAsisten($key)
    {
        return $this->getVarFromArrayKey($key,$this->lingkupAsisten());
    }
    public function nama_kel($kel){
        return $this->CI->Auto_model->get_desa($kel);
    }
}
?>